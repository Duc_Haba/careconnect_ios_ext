//
//  DashboardTabbarController.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 24/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class DashboardTabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let tabItems = self.tabBar.items else { return }
        for item in tabItems {
            let unselectedItem = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
            let selectedItem = [NSAttributedString.Key.foregroundColor: Color.appPrimaryColor.uiColor]
            
            item.setTitleTextAttributes(unselectedItem, for: .normal)
            item.setTitleTextAttributes(selectedItem, for: .selected)
        }
    }

}
