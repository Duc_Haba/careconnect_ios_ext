//
//  SettingsTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 26/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SettingsTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var acessoryButton: UIButton!
    @IBOutlet weak var titileLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var `switch`: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
