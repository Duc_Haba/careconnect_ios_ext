//
//  WKWebviewVC.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 07/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import WebKit

protocol AuthorizeDelegate: class {
    func didDismiss(withAuthorization: Bool)
}
class WKWebviewVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    weak var shimerService: ShimmerService?
    weak var delegate: AuthorizeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
        if let url = URL(string: shimerService?.authenticationUrl ?? "") {
            let request = URLRequest(url: url)
            self.webView.load(request)
            webView.navigationDelegate = self
            self.webView.addObserver(self, forKeyPath: #keyPath(WKWebView.isLoading), options: .new, context: nil)
        }
    }
    
    @IBAction private func didClose(_ sender: Any) {
        dismiss(animated: true) {
            self.delegate?.didDismiss(withAuthorization: false)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "loading" {
            if webView.isLoading {
                activityIndicator.startAnimating()
                activityIndicator.isHidden = false
            } else {
                activityIndicator.stopAnimating()
            }
        }
    }
}

extension WKWebviewVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        
        let response = navigationResponse.response
        let string1 = response.url?.absoluteString
        let url = "https://careconnect-shimmer.ymedia.in/authorize/fitbit/callback"
        if string1?.hasPrefix(url) ?? false {
            self.dismiss(animated: true) {
                self.delegate?.didDismiss(withAuthorization: true)
            }
        }
        decisionHandler(.allow)
    }
    
}
