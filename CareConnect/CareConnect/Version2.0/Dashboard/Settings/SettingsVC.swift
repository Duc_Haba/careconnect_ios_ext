//
//  SettingsVC.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 26/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    
    private struct SettingsModel {
        var title: String?
        var subtitle: String?
        var image: UIImage?
        var isSwitch: Bool = false
    }
    
    @IBOutlet weak var loaderStackView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private var model: [SettingsModel] = []
    private let dashboardVM = DashboardVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loaderStackView.isHidden = true
        getUserDatafromRealm()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.tableHeaderView?.frame.size = CGSize(width: UIScreen.main.bounds.width, height: 325)
    }
    
    func getUserDatafromRealm() {
        dashboardVM.getUsersDataFromRealm()
        guard let headerView = SettingsHeaderView.instanceFromNib() as? SettingsHeaderView else {
            return
        }
        headerView.userData = dashboardVM.users
        headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 325)
        tableView.tableHeaderView = headerView
        
    }
    private func setupTable() {
        tableView.tableFooterView = UIView()
        
        tableView.register(SettingsTableCell.self)

        model = [SettingsModel(title: "Account", subtitle: "Account Settings & Login", image: #imageLiteral(resourceName: "Icon-Right"), isSwitch: false)]
        model.append(SettingsModel(title: "Connected Devices", subtitle: "Apple Health, Fitbit", image: #imageLiteral(resourceName: "Icon-Right"), isSwitch: false))
        model.append(SettingsModel(title: "Health Data", subtitle: "Manage your Health Data", image: #imageLiteral(resourceName: "Icon-Right"), isSwitch: false))
        model.append(SettingsModel(title: "Push-Notifications", subtitle: "Set up push notifications", image: #imageLiteral(resourceName: "Icon-Right"), isSwitch: true))
        model.append(SettingsModel(title: "Face ID", subtitle: "Face ID", image: #imageLiteral(resourceName: "Icon-Right"), isSwitch: false))
        model.append(SettingsModel(title: "Important Notices", subtitle: "Legal stuff goes here", image: #imageLiteral(resourceName: "Icon-Right"), isSwitch: false))
        model.append(SettingsModel(title: "Logout", subtitle: "Safe and secure", image: #imageLiteral(resourceName: "Icon-Logout"), isSwitch: false))
    }
    
    @IBAction private func didPressBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        cell = createSettingCell(indexPath: indexPath)
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if model[indexPath.row].title == "Logout" {
            self.loaderStackView.isHidden = false
            dashboardVM.logout { [weak self] _, error in
//                if error == nil {
                    DispatchQueue.main.async {
                        self?.loaderStackView.isHidden = true
                        
                        Defaults.shared.conncetedDeviceType = ShimType.none.rawValue
                        Defaults.shared.isDepedentUser = false
                        Defaults.shared.shouldUpdateDashboard = true
                        Defaults().isUserLoggedIn = false
                        Defaults().isFitBitLoggedIn = false
                        Defaults().refreshToken = ""
                        Defaults().selectedProfileID = 0
                        
                        let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
                        let initialViewController = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController")
                        self?.navigationController?.pushViewController(initialViewController, animated: false)
                        
//                        let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
//                        let initialViewController = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController")
//                        let nav = UINavigationController()
//                        nav.isNavigationBarHidden = true
//                        nav.viewControllers = [initialViewController]
//                        let appDelegateObj = UIApplication.shared.delegate as! AppDelegate
//                        appDelegateObj.window?.rootViewController = nav
//                        self?.navigationController?.popToViewController(initialViewController, animated: true)
//                        self?.navigationController?.pushViewController(initialViewController, animated: true)
//                        self.window?.rootViewController = initialViewController
                    }
//                }
            }
        } else if model[indexPath.row].title == "Connected Devices" {
            let connectedDevicesVC = ConnectedDevicesVC(nibName: ConnectedDevicesVC.className, bundle: nil)
            navigationController?.pushViewController(connectedDevicesVC, animated: true)
            return
        }
    }
    
    private func createSettingCell(indexPath: IndexPath) -> SettingsTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableCell.className, for: indexPath) as? SettingsTableCell else { return nil }
        cell.titileLabel.text = model[indexPath.row].title
        cell.subtitleLabel.text = model[indexPath.row].subtitle
        cell.acessoryButton.setImage(model[indexPath.row].image, for: .normal)
        cell.switch.isHidden = !model[indexPath.row].isSwitch
        cell.acessoryButton.isHidden = model[indexPath.row].isSwitch
        return cell
    }
}
