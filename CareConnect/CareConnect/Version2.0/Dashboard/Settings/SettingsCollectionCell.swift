//
//  SettingsCollectionCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 26/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SettingsCollectionCell: UICollectionViewCell, NibLoadableView {

    @IBOutlet weak var relationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var selectionImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    var isCurrentUser: Bool = false {
        didSet {
            selectionImageView.isHidden = !isCurrentUser
            profileImageView.layer.borderColor = UIColor.white.cgColor
            profileImageView.layer.borderWidth = 0.0
            if isCurrentUser {
                profileImageView.layer.borderWidth = 2.0
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileImageView.dropShadow(cornerRadius: 16)
    }

}
