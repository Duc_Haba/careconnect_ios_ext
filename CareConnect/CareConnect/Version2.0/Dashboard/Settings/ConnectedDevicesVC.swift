//
//  ConnectedDevicesVC.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 08/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import HealthKit

class ConnectedDevicesVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderStackView: UIView!
    
    var shimmerService = ShimmerService()
    var todayStep = Int()
    var stepDataSource: [[String: String]]? = []
    var todaysHeartRate = Int()
    var heartRateDataSource: [[String: String]]? = []
    var selectedShim: ShimType = .fitbit
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTable()
        shimmerService.updateModel()
        loaderStackView.isHidden = true
    }
    
    private func setupTable() {
        tableView.tableFooterView = UIView()
        tableView.register(SettingsTableCell.self)
    }
    
    @IBAction private func didPressBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}

extension ConnectedDevicesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shimmerService.model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        cell = createSettingCell(indexPath: indexPath)
        return cell ?? UITableViewCell()
    }
    
    private func createSettingCell(indexPath: IndexPath) -> SettingsTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableCell.className, for: indexPath) as? SettingsTableCell else { return nil }
        cell.switch.isHidden = false
        cell.switch.tag = indexPath.row
        cell.subtitleLabel.isHidden = true
        cell.switch.addTarget(self, action: #selector(switchChanged(_:)), for: .valueChanged)
        cell.acessoryButton.isHidden = true
        cell.switch.isOn = shimmerService.model[indexPath.row].isConnected
        if shimmerService.deviceIntegrated {
            cell.switch.isUserInteractionEnabled = shimmerService.model[indexPath.row].shimType == shimmerService.connectedShim
            cell.titileLabel.textColor = shimmerService.model[indexPath.row].shimType == shimmerService.connectedShim ? UIColor.darkText : UIColor.lightGray
        } else {
            cell.switch.isUserInteractionEnabled = true
            cell.titileLabel.textColor = UIColor.darkText
        }
        cell.titileLabel.text = shimmerService.model[indexPath.row].shimType.title
        return cell
    }
    
    @objc func switchChanged(_ sender: UISwitch) {
        guard sender.isOn else {
            shimmerService.clearHealthData()
            Defaults.shared.conncetedDeviceType = ShimType.none.rawValue
            shimmerService.deviceIntegrated = false
            shimmerService.updateModel()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deviceIntegrationUpdated"), object: self)
            tableView.reloadData()
            return
        }
        let node = shimmerService.model[sender.tag]
        selectedShim = node.shimType
        switch node.shimType {
        case .fitbit, .googleFit:
            authoriseShim(with: selectedShim)
        case .healthKit:
            authoriseHealthKit()
        case .none:
            break

        }
    }
    
    private func authoriseHealthKit() {
        loaderStackView.isHidden = false
        guard checkForIntegration() else {
            loaderStackView.isHidden = true
            return
        }
        HealthKitAssistant.shared.getHealthKitPermission { [weak self] response in
            guard let weakSelf = self else { return }
            weakSelf.shimmerService.model[weakSelf.selectedShim.rawValue].isConnected = true
            DispatchQueue.main.async {
                weakSelf.loaderStackView.isHidden = true
                weakSelf.shimmerService.connectedShim = weakSelf.selectedShim
                weakSelf.concurrentHealthKitCall()
            }
        }
    }
    
    private func authoriseShim(with shimType: ShimType) {
        loaderStackView.isHidden = false
        guard checkForIntegration() else {
            loaderStackView.isHidden = true
            return }
        shimmerService.authorizeService(with: shimType) { [weak self] isSuccess in
            DispatchQueue.main.async {
                guard let weakSelf = self else { return }
                weakSelf.loaderStackView.isHidden = true
                if isSuccess {
                    if weakSelf.shimmerService.model[shimType.rawValue].isConnected {
                        weakSelf.shimmerService.connectedShim = weakSelf.selectedShim
                        weakSelf.concurrentShimCall()
                    } else {
                        weakSelf.openWebView()
                    }
                }
            }
        }
    }
    
    private func openWebView() {
        guard shimmerService.authenticationUrl != ""else { return }
        let webviewVC = WKWebviewVC(nibName: WKWebviewVC.className, bundle: nil)
        webviewVC.shimerService = shimmerService
        webviewVC.delegate = self
        let webviewNC = UINavigationController(rootViewController: webviewVC)
        webviewNC.navigationBar.isHidden = true
        navigationController?.present(webviewNC, animated: true)
    }
    
    private func loadMostRecentSteps(_ completion: @escaping () -> Void) {
        guard let stepsdata = HKQuantityType.quantityType(forIdentifier: .stepCount) else { return }
        
        HealthKitAssistant.shared.getMostRecentStep(for: stepsdata) { steps, stepsData in
            self.todayStep = steps
            self.stepDataSource = stepsData
            completion()
        }
        
    }
    
    private func loadMostRecentHeartRates(_ completion: @escaping () -> Void) {
        guard let heatrRatesdata = HKQuantityType.quantityType(forIdentifier: .heartRate) else { return }
        
        HealthKitAssistant.shared.getRecentHeartRate(for: heatrRatesdata) { hearRates, ratesData in
            self.todaysHeartRate = hearRates
            self.heartRateDataSource = ratesData
            completion()
        }
    }
    
    private func checkForIntegration() -> Bool {
        if shimmerService.model[selectedShim.rawValue].isConnected {
            shimmerService.model[selectedShim.rawValue].isConnected = false
            shimmerService.deviceIntegrated = false
            tableView.reloadData()
            return false
        } else {
            return true
        }
    }
    
    private func concurrentHealthKitCall() {
        Defaults.shared.shouldUpdateDashboard = true
        let dipatchGroup = DispatchGroup()
        loaderStackView.isHidden = false
        shimmerService.model[selectedShim.rawValue].isConnected = true
        shimmerService.deviceIntegrated = true
        shimmerService.clearHealthData()

        dipatchGroup.enter()
        loadMostRecentSteps {
            dipatchGroup.leave()
        }
        
//        dipatchGroup.enter()
//        loadMostRecentHeartRates {
//            dipatchGroup.leave()
//        }
//
        dipatchGroup.notify(queue: .main) { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deviceIntegrationUpdated"), object: self)
                self?.loaderStackView.isHidden = true
            }
        }
    }
    
    private func concurrentShimCall() {
        Defaults.shared.shouldUpdateDashboard = true
        let dipatchGroup = DispatchGroup()
        loaderStackView.isHidden = false
        shimmerService.model[selectedShim.rawValue].isConnected = true
        shimmerService.deviceIntegrated = true
        Defaults.shared.conncetedDeviceType = selectedShim.rawValue
        shimmerService.clearHealthData()

        dipatchGroup.enter()
        let prevDay = Date().previousDay ?? Date()
        shimmerService.fetchSteps(with: selectedShim, fromDate: prevDay, toDate: Date()) { isSuccess in
            dipatchGroup.leave()
        }
        
        dipatchGroup.enter()
        shimmerService.fetchHearRateData(with: selectedShim, fromDate: prevDay, toDate: Date()) { isSuccess in
            dipatchGroup.leave()
        }
        
        dipatchGroup.enter()
        shimmerService.fetchSleepData(with: selectedShim, fromDate: prevDay, toDate: Date()) { isSuccess in
            dipatchGroup.leave()
        }
        
        dipatchGroup.notify(queue: .main) { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deviceIntegrationUpdated"), object: self)
                self?.loaderStackView.isHidden = true
            }
        }
    }
    
}

extension ConnectedDevicesVC: AuthorizeDelegate {
    func didDismiss(withAuthorization: Bool) {
        if withAuthorization {
            concurrentShimCall()
        } else {
            shimmerService.deviceIntegrated = false
            shimmerService.connectedShim = .none
            tableView.reloadData()
        }
    }

}
