//
//  SleepModel.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 12/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class SleepModel: Object, Codable {
    dynamic var dateOfSleep: String?
    dynamic var startTime: String?
    dynamic var endTime: String?
    dynamic var timeInBed: Int = 0
    dynamic var minutesAsleep: Int = 0
    var microModel = List<SleepMicroModel>()
    
    override static func primaryKey() -> String? {
        return "dateOfSleep"
    }
    
    private enum CodingKeys: String, CodingKey {
        case dateOfSleep
        case startTime
        case endTime
        case timeInBed
        case minutesAsleep
        case microModel = "data"
        case levels
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        dateOfSleep = try container.decodeIfPresent(String.self, forKey: .dateOfSleep)
        startTime = try container.decodeIfPresent(String.self, forKey: .startTime)
        endTime = try container.decodeIfPresent(String.self, forKey: .endTime)
        timeInBed = try container.decodeIfPresent(Int.self, forKey: .timeInBed) ?? 0
        minutesAsleep = try container.decodeIfPresent(Int.self, forKey: .minutesAsleep) ?? 0

        let levels = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .levels)

        if let microModel = try levels.decodeIfPresent(List<SleepMicroModel>.self, forKey: .microModel) {
            self.microModel = microModel
        }
    }
}

extension SleepModel {
    func encode(to encoder: Encoder) throws {
    }
}

@objcMembers
class SleepMicroModel: Object, Codable {
    dynamic var dateTime: String?
    dynamic var level: String?
    dynamic var value: Int?
    
    override static func primaryKey() -> String? {
        return "dateTime"
    }
    
    private enum CodingKeys: String, CodingKey {
        case dateTime
        case level
        case value
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        dateTime = try container.decodeIfPresent(String.self, forKey: .dateTime)
        level = try container.decodeIfPresent(String.self, forKey: .level)
        value = try container.decodeIfPresent(Int.self, forKey: .value)
    }
    
}

extension SleepModel {
    class func storeInDataBase(sleepData: [SleepModel]) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(sleepData, update: .all)
        }
    }
    
    func storeSleepDataToDB() {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(self, update: .all)
        }
    }
}
