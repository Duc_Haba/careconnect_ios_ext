//
//  HeartRateModel.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 13/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class HeartRateModel: Object, Codable {
    dynamic var dateTime: String?
    var heartRateZones = List<HeartRateZone>()
    
    override static func primaryKey() -> String? {
        return "dateTime"
    }
    
    private enum CodingKeys: String, CodingKey {
        case dateTime
        case heartRateZones = "heartRateZones"
        case value
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        dateTime = try container.decodeIfPresent(String.self, forKey: .dateTime)
        let value = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .value)
        
        if let heartRateZones = try value.decodeIfPresent(List<HeartRateZone>.self, forKey: .heartRateZones) {
            self.heartRateZones = heartRateZones
        }
    }
}

extension HeartRateModel {
    func encode(to encoder: Encoder) throws {
    }
}

@objcMembers
class HeartRateZone: Object, Codable {
    dynamic var name: String?
    dynamic var min: Int = 0
    dynamic var max: Int = 0
    
    override static func primaryKey() -> String? {
        return "name"
    }
    
    private enum CodingKeys: String, CodingKey {
        case name
        case max
        case min
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        name = try container.decodeIfPresent(String.self, forKey: .name)
        max = try container.decodeIfPresent(Int.self, forKey: .max) ?? 0
        min = try container.decodeIfPresent(Int.self, forKey: .min) ?? 0
    }
    
}

extension HeartRateModel {
    class func storeInDataBase(heartRateData: [HeartRateModel]) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(heartRateData, update: .all)
        }
    }
    
    func storeSleepDataToDB() {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(self, update: .all)
        }
    }
}
