//
//  StepsModel.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 12/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class StepsModel: Object, Codable {
    dynamic var dateTime: String?
    dynamic var value: String?
    
    override static func primaryKey() -> String? {
        return "dateTime"
    }
    
    private enum CodingKeys: String, CodingKey {
        case dateTime
        case value
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        dateTime = try container.decodeIfPresent(String.self, forKey: .dateTime)
        value = try container.decodeIfPresent(String.self, forKey: .value)
    }
}

extension StepsModel {
    class func storeInDataBase(stepData: [StepsModel]) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(stepData, update: .all)
        }
    }
    
    func storeStepsDataToDB() {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(self, update: .all)
        }
    }
}
