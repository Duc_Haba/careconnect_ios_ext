//
//  SettingsHeaderView.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 26/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import SDWebImage

class SettingsHeaderView: UIView {
    
    @IBOutlet weak var contianerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var userData = [Dependents]()
    
    class func instanceFromNib() -> UIView {
        guard let view = UINib(nibName: "SettingsHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView else {
            return UIView()
        }
        return view
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contianerView.setGradientBackground(cornerRadius: 0)
        registerCells()
        setUpLayout()
    }
    
    private func registerCells() {
        collectionView.register(SettingsCollectionCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func setUpLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
    }
}

extension SettingsHeaderView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell?
        cell = createProfileCell(indexPath: indexPath)
        return cell ?? UICollectionViewCell()
    }
    
    private func createProfileCell(indexPath: IndexPath) -> SettingsCollectionCell? {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SettingsCollectionCell.className, for: indexPath) as? SettingsCollectionCell else { return nil }
        cell.isCurrentUser = userData[indexPath.row].id == Defaults.shared.selectedProfileID
        cell.profileImageView.sd_setImage(with: URL(string: userData[indexPath.row].imageUrl ?? "" ), placeholderImage: UIImage(named: "Icon-Placeholder"))
        
        cell.nameLabel.text = userData[indexPath.row ].name
        let relation = userData[indexPath.row].relationship
        cell.relationLabel.text = relation == "self" ? "Me" : relation?.capitalized
        cell.profileImageView.layer.cornerRadius = 16
        cell.profileImageView.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 136, height: 204)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard userData[indexPath.row].id != Defaults.shared.selectedProfileID else { return }
        
        Defaults.shared.selectedProfileID = userData[indexPath.row].id
        Defaults.shared.isDepedentUser = userData[indexPath.row].relationship != "self"
        appDelegate?.switchToDashboard()
    }
}
