//
//  CareTeamCollectionCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 25/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class CareTeamCollectionCell: UICollectionViewCell, NibLoadableView {

    @IBOutlet weak var physicianImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

}
