//
//  AppointmentCheckInCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 23/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class AppointmentCheckInCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var checkinButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        checkinButton.setGradientBackground(cornerRadius: 15)
        containerView.dropShadow(cornerRadius: 20)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction private func checkInTapped(_ sender: Any) {
        
    }
    
}
