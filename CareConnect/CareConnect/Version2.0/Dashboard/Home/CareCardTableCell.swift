//
//  CareCardTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 24/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class CareCardTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitileLabel: UILabel!
    @IBOutlet weak var notificationImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.dropShadow(cornerRadius: 16)
    }
}
