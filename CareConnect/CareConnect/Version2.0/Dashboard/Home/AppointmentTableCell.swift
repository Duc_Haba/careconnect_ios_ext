//
//  AppointmentTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 24/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class AppointmentTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var containerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerTopConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.dropShadow(cornerRadius: 16)
    }
    
}
