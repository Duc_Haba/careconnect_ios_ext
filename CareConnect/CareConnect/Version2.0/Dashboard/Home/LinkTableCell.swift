//
//  LinkTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 25/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class LinkTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var linkImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.dropShadow(cornerRadius: 16)
    }
    
}
