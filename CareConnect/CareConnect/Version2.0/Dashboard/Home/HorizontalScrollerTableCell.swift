//
//  HorizontalScrollerTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 25/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class HorizontalScrollerTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var celltype: CellType = .careTeam {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var model: [CareTeamModel] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        registerCells()
        setUpLayout()
    }

    private func registerCells() {
        collectionView.register(CareTeamCollectionCell.self)
        collectionView.register(VitalsCollectionCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func setUpLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
    }
}

extension HorizontalScrollerTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return celltype == CellType.careTeam ? model.count : 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell?
        switch celltype {
        case .careTeam:
            cell = createCareTeamCell(indexPath: indexPath)
        default:
            cell = createVitalsCell(indexPath: indexPath)
        }
        return cell ?? UICollectionViewCell()
    }
    
    private func createVitalsCell(indexPath: IndexPath) -> VitalsCollectionCell? {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VitalsCollectionCell.className, for: indexPath) as? VitalsCollectionCell else { return nil }
        cell.graphImageView.image = #imageLiteral(resourceName: "HeartRate")
        cell.titleImage.image = #imageLiteral(resourceName: "heart")
        cell.titleLabel.text = "Heart"
        let date = Date.getCurrentDate(format: dateFormat)
        let hearRateModel = RealmManager.shared.getObjects(type: HeartRateZone.self, with: "Out of Range")
        cell.graphImageView.isHidden = false
        cell.progressBar.isHidden = true
        cell.goalLabel.isHidden = true
        cell.subtitleLabel.text = "\(hearRateModel?.max ?? 0) bpm"
        if indexPath.row == 0 {
            cell.progressBar.isHidden = false
            cell.graphImageView.isHidden = true
            cell.goalLabel.isHidden = false
            cell.titleImage.image = #imageLiteral(resourceName: "walking")
            cell.titleLabel.text = "Walking"
            let stepmodel = RealmManager.shared.getObjects(type: StepsModel.self, with: date)
            cell.goalLabel.text = "10,000 steps"
            guard let value = stepmodel?.value else {
                cell.subtitleLabel.text = "0 steps"
                cell.progressBar.angle = 0
                return cell
            }
            cell.subtitleLabel.text = value + " steps"
            cell.progressBar.progressColors = [Color.labTestValueNormalStartColor.uiColor, Color.labTestValueNormalStartColor.uiColor, Color.labTestValueNormalEndColor.uiColor]
            cell.progressBar.angle = getAngle(with: 4000, achievedValue: Double(value) ?? 0)
        } else if indexPath.row == 2 {
            cell.progressBar.isHidden = false
            cell.graphImageView.isHidden = true
            cell.goalLabel.isHidden = false
            cell.titleImage.image = #imageLiteral(resourceName: "Sleep")
            cell.titleLabel.text = "Sleep"
            cell.goalLabel.text = "8 hours"
            let sleepModel = RealmManager.shared.getObjects(type: SleepModel.self, with: date)
            guard let minutesAsleep = sleepModel?.minutesAsleep else {
                cell.subtitleLabel.text = "0 hours"
                cell.progressBar.angle = 0
                return cell }
            let (h, m) = Date.minutesToHoursMinutes(minutes: minutesAsleep)
            cell.subtitleLabel.text = "\(h) hours \(m) minutes"
            cell.progressBar.progressColors = [Color.gradientStartColor.uiColor, Color.gradientStartColor.uiColor, Color.gradientEndcolor.uiColor]
            let totalvalue: Double = Double(minutesAsleep) / 60
            cell.progressBar.angle = getAngle(with: 8, achievedValue: totalvalue)
        }
        return cell
    }
    
    private func getAngle(with goal: Double, achievedValue: Double) -> Double {
        return (360 * achievedValue) / goal
    }
    
    private func createCareTeamCell(indexPath: IndexPath) -> CareTeamCollectionCell? {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CareTeamCollectionCell.className, for: indexPath) as? CareTeamCollectionCell else { return nil }
        cell.nameLabel.text = model[indexPath.row].doctorName
        cell.roleLabel.text = model[indexPath.row].specialization
        cell.physicianImageView.contentMode = .scaleAspectFill
        cell.physicianImageView.sd_setImage(with: URL(string: model[indexPath.row].imageUrl ?? ""), placeholderImage: UIImage(named: "Icon-Placeholder"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: 173, height: 210)
        if celltype == .shim {
            size = CGSize(width: 173, height: 240)
        }
        return size
    }
    
}
