//
//  HomeTableHeader.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 24/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class HomeTableHeader: UIView, NibLoadableView {

    class func instanceFromNib() -> UIView {
        guard let view = UINib(nibName: "HomeTableHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView else {
            return UIView()
        }
        return view
    }

}
