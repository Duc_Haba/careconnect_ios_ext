//
//  HomeTableSectionHeader.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 25/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class HomeTableSectionHeader: UITableViewHeaderFooterView, NibLoadableView {
    @IBOutlet weak var titleLabel: UILabel!
    
}
