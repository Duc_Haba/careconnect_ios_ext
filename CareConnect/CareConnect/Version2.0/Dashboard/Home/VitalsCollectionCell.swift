//
//  VitalsCollectionCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 25/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class VitalsCollectionCell: UICollectionViewCell, NibLoadableView {

    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var graphImageView: UIImageView!
    @IBOutlet weak var progressBar: KDCircularProgress!
    @IBOutlet weak var goalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        progressBar.trackColor = Color.trackColor.uiColor
    }

}
