//
//  MedicationTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 24/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class MedicationTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var refillButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        refillButton.setGradientBackground(cornerRadius: refillButton.frame.height / 2)
        containerView.dropShadow(cornerRadius: 16)
    }
}
