//
//  FindDoctorTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 25/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class FindDoctorTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var titileLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.setGradientBackground(cornerRadius: 20)
        containerView.dropShadow(cornerRadius: 16)
    }
}
