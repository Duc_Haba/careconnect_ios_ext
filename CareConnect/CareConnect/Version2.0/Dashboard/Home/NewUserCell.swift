//
//  NewUserCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 20/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class NewUserCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.dropShadow(cornerRadius: 20)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
