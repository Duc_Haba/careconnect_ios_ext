//
//  HomeVC.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 24/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import SDWebImage

enum CellType: Int, CaseIterable {
    case appointment = 0
    case careCard
    case medication
    case link
    case shim
    case careTeam
}

class HomeVC: UIViewController {

    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var headerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderStackView: UIView!
    @IBOutlet weak var userName: UILabel!
    
    private let tableViewContentInset: CGFloat = 180

    let maxHeaderHeight: CGFloat = 275
    
    var dashBoardVM: DashboardVM = DashboardVM()
    var careCards: [ActivityModel] = []
    let imageView = UIImageView()
    var appointmentIsReadyToCheckIn = false

     override func viewDidLoad() {
        super.viewDidLoad()
        
        profileButton.imageView?.contentMode = .scaleAspectFill
        registerCells()
        loaderStackView.isHidden = true
        setUpHeader()
        tableView.contentInset.top = tableViewContentInset
        NotificationCenter.default.addObserver(self, selector: #selector(switchChanged), name: NSNotification.Name(rawValue: "deviceIntegrationUpdated"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchDetails(showSpinner: Defaults.shared.shouldUpdateDashboard)
    }
    
    private func setUpHeader() {
        let selectedProfileData = dashBoardVM.getUser(userID: Defaults.shared.selectedProfileID)
        SDWebImageManager.shared.loadImage(with: URL(string: selectedProfileData.imageUrl ?? "") as URL?, options: .continueInBackground, progress: { (recieved, expected, nil) in
        }, completed: { (downloadedImage, data, error, SDImageCacheType, true, imageUrlString) in
            DispatchQueue.main.async {
                if downloadedImage != nil {
                    self.profileButton.setImage(downloadedImage, for: .normal)
                }
            }
        })
        var title = Date().getGreetingsText() + " "
        if let name = selectedProfileData.name {
            title += name.components(separatedBy: " ")[0]
        }
        userName.text = title
        setSubtitleLabel()
    }
    
    private func setSubtitleLabel() {
        var subtitle = ""
        if !dashBoardVM.dashboardAppointments.isEmpty {
            let suffix = dashBoardVM.dashboardAppointments.count == 1 ? "appointment" : "appointments"
            subtitle = "You have \(dashBoardVM.dashboardAppointments.count) upcomming " + suffix
        }
        subtitleLabel.text = subtitle
    }
    
    private func fetchDetails(showSpinner: Bool = true) {
        dashBoardVM.getUsersDataFromRealm()
        loaderStackView.isHidden = !showSpinner
        dashBoardVM.fetchDashboardDetails { [weak self] isSuccess in
            DispatchQueue.main.async {
                self?.loaderStackView.isHidden = true
                if isSuccess {
                    Defaults.shared.shouldUpdateDashboard = false
                    self?.dashBoardVM.setUpDataSource()
                    self?.setSubtitleLabel()
                    self?.tableView.reloadData()
                } else {
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    @objc func switchChanged() {
        dashBoardVM.setUpDataSource()
        tableView.reloadData()
    }
    
    private func registerCells() {
        profileButton.layer.cornerRadius = profileButton.frame.width / 2
        profileButton.layer.masksToBounds = true
        tableView.register(AppointmentTableCell.self)
        tableView.register(CareCardTableCell.self)
        tableView.register(CareDetailTableCell.self)
        tableView.register(LinkTableCell.self)
        tableView.register(FindDoctorTableCell.self)
        tableView.register(NewUserCell.self)
        tableView.register(HorizontalScrollerTableCell.self)
        tableView.register(AppointmentCheckInCell.self)
        tableView.registerReusableHeaderFooterView(HomeTableSectionHeader.self)
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100.0
    }
    
    @IBAction private func didClickProfileButton(_ sender: Any) {
        let settingsVC = SettingsVC(nibName: SettingsVC.className, bundle: nil)
        navigationController?.pushViewController(settingsVC, animated: true)
    }
    
    private func updateMedication(date: Date, patientMedicationId: Int, statusCode: Int) {
        loaderStackView.isHidden = false
        self.view.isUserInteractionEnabled = false
        let dateInIntervels = Int(date.timeIntervalSince1970 * 1000)
        dashBoardVM.updateMedication(date: dateInIntervels, statusCode: statusCode, patientMedicationID: patientMedicationId) { [weak self] _, error in
            if error == nil {
                DispatchQueue.main.async {
                    self?.loaderStackView.isHidden = true
                    self?.view.isUserInteractionEnabled = true
                }
            } else {
                self?.loaderStackView.isHidden = true
                self?.view.isUserInteractionEnabled = true
            }
        }
    }

}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if Defaults.shared.isDepedentUser {
            return 1
        } 
        return dashBoardVM.model.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Defaults.shared.isDepedentUser {
            return 1
        }
        return dashBoardVM.model[section].numOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if Defaults.shared.isDepedentUser {
           let cell = tableView.dequeueReusableCell(for: indexPath) as NewUserCell
           cell.selectionStyle = .none
           return cell
        }
        var cell: UITableViewCell?
        let celltype = dashBoardVM.model[indexPath.section].cellDetails?[indexPath.row].cellType ?? .appointment
        switch celltype {
        case .appointment:
            cell = Date().minutesBetweenDates(Date(timeIntervalSince1970: self.dashBoardVM.dashboardAppointments[indexPath.row].date)) < 15 ? crateAppointmentCheckInCelll(indexPath: indexPath) : createAppointmentCell(indexPath: indexPath)
        case .careCard :
            cell = createCareCardCell(indexPath: indexPath)
        case .medication:
            cell = createMedicationCell(indexPath: indexPath)
        case .link:
            cell = createLinkCell(indexPath: indexPath)
        case .shim, .careTeam:
            cell = createHorizontalCell(indexPath: indexPath)
        }
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
    
    private func crateAppointmentCheckInCelll(indexPath: IndexPath) -> AppointmentCheckInCell? {
        appointmentIsReadyToCheckIn = true
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AppointmentCheckInCell.className, for: indexPath) as? AppointmentCheckInCell else { return nil }
        
        let model = self.dashBoardVM.dashboardAppointments[indexPath.row]
        cell.location.text = model.address
        var title = "Checkup - "
        if let name = model.name {
            title += name
        }
        cell.name.text = title
        cell.date.text = Date.getDateStringFromTimeStamp(model.date, format: prescriptionDateFormat)
        return cell
    }
    
    private func createAppointmentCell(indexPath: IndexPath) -> AppointmentTableCell? {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AppointmentTableCell.className, for: indexPath) as? AppointmentTableCell else { return nil }
        let model = self.dashBoardVM.dashboardAppointments[indexPath.row]
        cell.addressLabel.text = model.address
        var title = "Checkup - "
        if let name = model.name {
            title += name
        }
        cell.nameLabel.text = title
        cell.dateLabel.text = Date.getDateStringFromTimeStamp(model.date, format: prescriptionDateFormat)
        return cell
    }
    
    private func createHorizontalCell(indexPath: IndexPath) -> HorizontalScrollerTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HorizontalScrollerTableCell.className, for: indexPath) as? HorizontalScrollerTableCell else { return nil }
        cell.model = dashBoardVM.careTeam
        cell.celltype = dashBoardVM.model[indexPath.section].cellDetails?[indexPath.row].cellType ?? .careTeam
        return cell
    }
    
    private func createCareCardCell(indexPath: IndexPath) -> CareCardTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CareCardTableCell.className, for: indexPath) as? CareCardTableCell else { return nil }
        careCards = RealmManager.shared.getObjects(type: ActivityModel.self)
        let title = careCards[indexPath.row].name
        var subTitile = "No activities due"
        let list = careCards[indexPath.row].activitiesList
        if !list.isEmpty {
            subTitile = "\(list.count) activities due"
        }
        cell.titleLabel.text = title
        cell.subtitileLabel.text = subTitile
        return cell
    }
    
    private func createMedicationCell(indexPath: IndexPath) -> CareDetailTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CareDetailTableCell.className, for: indexPath) as? CareDetailTableCell else { return nil }
        cell.titileLabel.text = dashBoardVM.dashboardMedications[indexPath.row].name
        cell.subTitileLabel.text = dashBoardVM.dashboardMedications[indexPath.row].dosage
        let image = dashBoardVM.dashboardMedications[indexPath.row].consumptionStatus == 2 ? #imageLiteral(resourceName: "Icon-Selected") : #imageLiteral(resourceName: "Icon-UnSelected")
        cell.checkButton.setImage(image, for: .normal)
        cell.selectionStyle = .none
        return cell
    }
    
    private func createLinkCell(indexPath: IndexPath) -> LinkTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LinkTableCell.className, for: indexPath) as? LinkTableCell else { return nil }
        return cell
    }
    
    private func createFindDoctorCell(indexPath: IndexPath) -> FindDoctorTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FindDoctorTableCell.className, for: indexPath) as? FindDoctorTableCell else { return nil }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var header: UIView?
        guard !Defaults.shared.isDepedentUser else { return header }
        let type: SectionType = dashBoardVM.model[section].sectionType ?? .dynamic
        if type == .careTeam || type == .connectedDevices {
            let sectionHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: HomeTableSectionHeader.className) as? HomeTableSectionHeader
            sectionHeader?.titleLabel.text = dashBoardVM.model[section].title ?? ""
            header = sectionHeader
        }
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard !Defaults.shared.isDepedentUser else { return 0 }
        let type: SectionType = dashBoardVM.model[section].sectionType ?? .dynamic
        var height: CGFloat = 0
        if type == .careTeam || type == .connectedDevices {
            height = 60
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard !Defaults.shared.isDepedentUser else { return }
        guard let cellType = dashBoardVM.model[indexPath.section].cellDetails?[indexPath.row].cellType else { return }
        switch cellType {
        case .appointment:
            appointmentIsReadyToCheckIn == true ? pushToCheckInAppointmentVC(indexPath: indexPath) : pushToAppointmentDetails(indexPath: indexPath)

        case .careCard :
            presentCareCard(indexPath: indexPath)
        case .medication:
            markMedication(indexPath: indexPath)
        case .link:
            pushToLink()
        case .shim, .careTeam:
            break
        }
    }
    
    private func pushToAppointmentDetails(indexPath: IndexPath) {
        let appointmentDetailVC = AppointmentDetailVC(nibName: AppointmentDetailVC.className, bundle: nil)
        appointmentDetailVC.selectedAppointment = dashBoardVM.dashboardAppointments[indexPath.row]
        appointmentDetailVC.selectedAppointmentType = .upcomming
        appointmentDetailVC.refreshBlock = { [weak self] in
            DispatchQueue.main.async {
                self?.fetchDetails(showSpinner: Defaults.shared.shouldUpdateDashboard)
            }
        }
        navigationController?.pushViewController(appointmentDetailVC, animated: true)
    }
    
    private func pushToCheckInAppointmentVC(indexPath: IndexPath) {
        DispatchQueue.main.async {
            let checkInVC = CheckInAppointmentVC(nibName: CheckInAppointmentVC.className, bundle: nil)
            checkInVC.selectedAppointment = self.dashBoardVM.dashboardAppointments[indexPath.row]
            self.navigationController?.present(checkInVC, animated: true, completion: nil)
        }
    }
    
    private func markMedication(indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CareDetailTableCell else {
            return
        }
        cell.isChecked = cell.isChecked ? false : true
        let image = cell.isChecked ? #imageLiteral(resourceName: "Icon-Selected") : #imageLiteral(resourceName: "Icon-UnSelected")
        cell.checkButton.setImage(image, for: .normal)
        guard let medicationId = dashBoardVM.dashboardMedications[indexPath.row].patientMedicationId, let consumptionStatus = dashBoardVM.dashboardMedications[indexPath.row].consumptionStatus else {
            return
        }
        dashBoardVM.dashboardMedications[indexPath.row].consumptionStatus = consumptionStatus == 1 ? 2 : 1
        updateMedication(date: Date(), patientMedicationId: medicationId, statusCode: 2)
    }
    
    private func presentCareCard(indexPath: IndexPath) {
        DispatchQueue.main.async {
            let activityDetailsVC = ActivityDetailsVC(nibName: ActivityDetailsVC.className, bundle: nil)
            activityDetailsVC.activityVM.selectedCareCard = self.careCards[indexPath.row]
            let activityDetailsNC = UINavigationController(rootViewController: activityDetailsVC)
            activityDetailsNC.isNavigationBarHidden = true
            self.navigationController?.present(activityDetailsNC, animated: true)
        }
    }
    
    private func pushToLink() {
        DispatchQueue.main.async {
            let linkDetailVC = LinkDetailsVC(nibName: LinkDetailsVC.className, bundle: nil)
            linkDetailVC.shouldHideButton = true
            self.navigationController?.pushViewController(linkDetailVC, animated: true)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.headerTopConstraint.constant = min(0, -(self.tableView.contentInset.top + self.tableView.contentOffset.y))
        self.profileButton.isHidden = self.headerTopConstraint.constant != 0
        self.searchButton.isHidden = self.headerTopConstraint.constant != 0
        self.headerHeightConstraint.constant = maxHeaderHeight - min(0, (self.tableView.contentInset.top + self.tableView.contentOffset.y))

    }
}
