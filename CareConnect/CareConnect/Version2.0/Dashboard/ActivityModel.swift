//
//  ActivityModel.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 19/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ActivityModel: Object, Codable {
    dynamic var id: Int = 0
    dynamic var name: String?
    dynamic var activitySectionName: String?
    var activitiesList = List<ActivitiesListModel>()
    dynamic var surveySectionName: String?
    var surveysList = List<SurveysListModel>()
    dynamic var createdOn: Double = 0
    dynamic var startDate: Double = 0
    dynamic var targetBy: Double = 0
    dynamic var patientId: Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case activitySectionName
        case activitiesList = "activities"
        case surveySectionName
        case surveysList = "surveys"
        case createdOn
        case startDate
        case targetBy
        case patientId
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try container.decodeIfPresent(String.self, forKey: .name)
        activitySectionName = try container.decodeIfPresent(String.self, forKey: .activitySectionName)
        if let activitiesList = try container.decodeIfPresent(List<ActivitiesListModel>.self, forKey: .activitiesList) {
            self.activitiesList = activitiesList
        }
        surveySectionName = try container.decodeIfPresent(String.self, forKey: .surveySectionName)
        if let surveysList = try container.decodeIfPresent(List<SurveysListModel>.self, forKey: .surveysList) {
            self.surveysList = surveysList
        }
        createdOn = try container.decodeIfPresent(Double.self, forKey: .createdOn) ?? 0.0
        startDate = try container.decodeIfPresent(Double.self, forKey: .startDate) ?? 0.0
        targetBy = try container.decodeIfPresent(Double.self, forKey: .targetBy) ?? 0.0
        patientId = try container.decodeIfPresent(Int.self, forKey: .patientId) ?? 0
    }
}

extension ActivityModel {
    func encode(to encoder: Encoder) throws {
    }
}

extension ActivityModel {
    class func storeInDataBase(teamData: [ActivityModel]) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(teamData, update: .all)
        }
    }
    
    func storeDataToDB() {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(self, update: .all)
        }
    }
}

@objcMembers
class ActivitiesListModel: Object, Codable {
    dynamic var id: Int = 0
    dynamic var name: String?
    dynamic var duration: String?
    dynamic var createdOn: Double = 0.0
    var valuesList = List<ActivityValuesModel>()

    override static func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case duration
        case createdOn
        case valuesList = "values"
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try container.decodeIfPresent(String.self, forKey: .name)
        duration = try container.decodeIfPresent(String.self, forKey: .duration)
        createdOn = try container.decodeIfPresent(Double.self, forKey: .createdOn) ?? 0.0
        if let valuesList = try container.decodeIfPresent(List<ActivityValuesModel>.self, forKey: .valuesList) {
            self.valuesList = valuesList
        }
    }
}

extension ActivitiesListModel {
    func encode(to encoder: Encoder) throws {
    }
}

extension ActivitiesListModel {
    class func storeInDataBase(values: [ActivitiesListModel]) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(values, update: .all)
        }
    }
    
    func storeDataToDB() {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(self, update: .all)
        }
    }
    
    func addValueList(value: ActivityValuesModel) {
        let realm = try? Realm()
        try? realm?.write {
            valuesList.append(value)
        }
    }
}

@objcMembers
class SurveysListModel: Object, Codable {
    dynamic var id: Int = 0
    dynamic var name: String?
    dynamic var type: String?
    dynamic var frequency: String?
    dynamic var units: String?
    var valuesList = List<ActivityValuesModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case type
        case frequency
        case units
        case valuesList = "values"
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try container.decodeIfPresent(String.self, forKey: .name)
        type = try container.decodeIfPresent(String.self, forKey: .type)
        frequency = try container.decodeIfPresent(String.self, forKey: .frequency)
        units = try container.decodeIfPresent(String.self, forKey: .units)
        
        if let valuesList = try container.decodeIfPresent(List<ActivityValuesModel>.self, forKey: .valuesList) {
            self.valuesList = valuesList
        }
    }
}

extension SurveysListModel {
    func encode(to encoder: Encoder) throws {
    }
}

extension SurveysListModel {
    class func storeInDataBase(values: [ActivitiesListModel]) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(values, update: .all)
        }
    }
    
    func storeDataToDB() {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(self, update: .all)
        }
    }
    
    func addValueList(value: ActivityValuesModel) {
        let realm = try? Realm()
        try? realm?.write {
            valuesList.append(value)
        }
    }
}

@objcMembers
class ActivityValuesModel: Object, Codable {
    dynamic var value: String?
    dynamic var createdOn: Double = 0.0
    dynamic var isCompleted: Bool = false
    
    private enum CodingKeys: String, CodingKey {
        case value
        case createdOn
        case isCompleted
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        value = try container.decodeIfPresent(String.self, forKey: .value)
        createdOn = try container.decodeIfPresent(Double.self, forKey: .createdOn) ?? 0.0
        isCompleted = try container.decodeIfPresent(Bool.self, forKey: .isCompleted) ?? false
    }
}

extension ActivityValuesModel {
    func encode(to encoder: Encoder) throws {
    }
}

extension ActivityValuesModel {
    class func storeInDataBase(values: [ActivityValuesModel]) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(values, update: .all)
        }
    }
    
    func storeDataToDB() {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(self, update: .all)
        }
    }
}
