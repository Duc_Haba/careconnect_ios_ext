//
//  ChatListViewController.swift
//  CareConnect
//
//  Created by Venugopal S A on 25/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import TwilioChatClient

class ChatListViewController: UIViewController {
    
    @IBOutlet weak var chatListTableView: UITableView!
    @IBOutlet weak var loader: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        chatListTableView.register(ChatListUserCell.self)
//        
//        guard let userEmail = Defaults.shared.email as? String else {
//            return
//        }
//        registerUser(userName: userEmail )
    }
    
    @IBAction private func addNewTapped(_ sender: Any) {
        
        let username = "dreames@yml.com"
        
        guard username != TwilioHelper.sharedInstance.username else {
            //self.alert(withTitle: "You need to communicate with other people :)")
            return
        }
        
        if (CoreDataHelper.sharedInstance.getChannels().contains {
            ($0 as? Channel)?.name == username
        }) {
           // self.alert(withTitle: "You already have this channel")
            return
        } else {
            VirgilHelper.sharedInstance.getExportedCard(identity: username) { exportedCard, error in
                guard let exportedCard = exportedCard, error == nil else {
                    Log.error("Getting card failed")
                    return
                }
                TwilioHelper.sharedInstance.createSingleChannel(with: username) { error in
                    if error == nil {
                        _ = CoreDataHelper.sharedInstance.createChannel(type: .single,
                                                                        name: username,
                                                                        cards: [exportedCard])
                    } else {
                        //HUD.flash(.error)
                    }
                }
            }
        }
    }
    
    @IBAction private func searchTapped(_ sender: Any) {
    }
    
    func registerUser(userName: String) {
        
        VirgilHelper.sharedInstance.signUp(identity: userName) {[weak self] exportedCard, error in
            guard error == nil, let exportedCard = exportedCard else {
                if let err = error as? VirgilHelper.UserFriendlyError {
                    self?.signIn(userName: userName)
                }
                
                return
            }
            CoreDataHelper.sharedInstance.createAccount(withIdentity: userName, exportedCard: exportedCard)
            UserDefaults.standard.set(userName, forKey: "last_username")
        }
    }
    
    func signIn(userName: String) {
        guard CoreDataHelper.sharedInstance.loadAccount(withIdentity: userName) else {
            return
        }
        let exportedCard = CoreDataHelper.sharedInstance.getAccountCard()
        
        VirgilHelper.sharedInstance.signIn(identity: userName, card: exportedCard) { error in
            guard error == nil else {
                print("successfull")
                return
            }
            self.configure {
                self.navigationItem.titleView = nil
                self.title = "Chats"
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    private func configure(completion: @escaping () -> Void) {
        
        loader.isHidden = false
        let channels = TwilioHelper.sharedInstance.channels.subscribedChannels()
        let group = DispatchGroup()
        
        for i in 0..<channels!.count {
            let channel = channels![i]
            guard let channelName = TwilioHelper.sharedInstance.getName(of: channel) else {
                continue
            }
            if let coreChannel = CoreDataHelper.sharedInstance.getChannel(withName: channelName) {
                while channel.messages == nil { sleep(1) }
                
                group.enter()
                self.setLastMessages(twilioChannel: channel, coreChannel: coreChannel) {
                    group.leave()
                }
                
                group.enter()
                self.updateGroupChannelMembers(twilioChannel: channel, coreChannel: coreChannel) {
                    group.leave()
                }
            } else {
                Log.error("Get Channel failed")
                self.loader.isHidden = true

            }
        }
        
        group.notify(queue: .main) {
            self.loader.isHidden = true
            self.chatListTableView.reloadData()
            completion()
        }
    }
    private func updateGroupChannelMembers(twilioChannel: TCHChannel, coreChannel: Channel, completion: @escaping () -> Void) {
        if coreChannel.type == ChannelType.group.rawValue {
            TwilioHelper.sharedInstance.updateMembers(of: twilioChannel, coreChannel: coreChannel) {
                completion()
            }
        } else {
            completion()
        }
    }
    private func setLastMessages(twilioChannel: TCHChannel, coreChannel: Channel, completion: @escaping () -> Void) {
        if let messages = twilioChannel.messages {
            CoreDataHelper.sharedInstance.setLastMessage(for: coreChannel)
            TwilioHelper.sharedInstance.setLastMessage(of: messages, channel: coreChannel) {
                completion()
            }
        } else {
            Log.error("Get Messages failed")
            completion()
        }
    }
    
}

extension ChatListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let account = CoreDataHelper.sharedInstance.currentAccount,
            let channels = account.channel else {
                Log.error("Can't form row: Core Data account or channels corrupted")
                return 0
        }
        return channels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(for: indexPath) as ChatListUserCell
        
        let channels = CoreDataHelper.sharedInstance.getChannels()
        
        cell.tag = indexPath.row
        
        guard let channel = channels[indexPath.row] as? Channel else {
            Log.error("Can't form row: Core Data channel wrong index")
            return cell
        }
        cell.message.text = channel.lastMessagesBody
        cell.name.text = channel.name
        
        guard let date = channel.lastMessagesDate else {
            return cell
        }
        cell.time.text = channel.lastMessagesDate != nil ? calcDateString(messageDate: date) : ""
        cell.selectionStyle = .none
        return cell
    }
    
    private func calcDateString(messageDate: Date) -> String {
        let dateFormatter = DateFormatter()
        if messageDate.minutes(from: Date()) < 2 {
            return "now"
        } else if messageDate.days(from: Date()) < 1 {
            dateFormatter.dateStyle = DateFormatter.Style.none
            dateFormatter.timeStyle = DateFormatter.Style.short
        } else {
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.none
        }
        
        let messageDateString = dateFormatter.string(from: messageDate)
        
        return messageDateString
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let chatVC = UIStoryboard(storyboard: .chat).instantiateViewController(withIdentifier: String(describing: ChatVC.self)) as? ChatVC else {
            return
        }
        
        let channels = CoreDataHelper.sharedInstance.getChannels()
        
        guard let channelData = channels[indexPath.row] as? Channel else {
            Log.error("Can't form row: Core Data channel wrong index")
            return
        }
        chatVC.selectedUser = channelData.name ?? ""
        TwilioHelper.sharedInstance.setChannel(withName: channelData.name ?? "")
        
        guard CoreDataHelper.sharedInstance.loadChannel(withName: channelData.name ?? ""),
            let channel = CoreDataHelper.sharedInstance.currentChannel else {
                Log.error("Channel do not exist in Core Data")
                return
        }
        
        VirgilHelper.sharedInstance.setChannelKeys(channel.cards)
        
//        TwilioHelper.sharedInstance.currentChannel.getMessagesCount { result, count in
//            guard result.isSuccessful() else {
//                Log.error("Can't get Twilio messages count")
//                return
//            }
//            //self.currentChannelMessegesCount = Int(count)
//        }
//
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
}
