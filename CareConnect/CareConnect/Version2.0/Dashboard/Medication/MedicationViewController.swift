//
//  MedicationViewController.swift
//  CareConnect
//
//  Created by Venugopal S A on 31/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import VACalendar

class MedicationViewController: UIViewController {
    
    enum TabSelected {
        case schedule
        case refill
    }
    
    @IBOutlet weak var noMedicatioinView: UIView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var weekDaysView: VAWeekDaysView! {
        didSet {
            let appereance = VAWeekDaysViewAppearance(symbolsType: .veryShort, weekDayTextColor: UIColor(red: 199 / 255, green: 199 / 255, blue: 212 / 255, alpha: 1.0), separatorBackgroundColor: .clear, calendar: defaultCalendar)
            
            weekDaysView.appearance = appereance
        }
    }
    @IBOutlet weak var medicationTableView: UITableView!
    @IBOutlet weak var calenderView: UIView!
    @IBOutlet weak var scheduleButtonView: UIView!
    @IBOutlet weak var refillButtonView: UIView!
    @IBOutlet weak var refillButton: UIButton!
    @IBOutlet weak var scheduleButton: UIButton!
    
    var selectedTab = TabSelected.schedule
    var selectedDate = Date()
 
    var vacalendarView: VACalendarView?
    
    let defaultCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        if let zone = TimeZone(secondsFromGMT: 0) {
            calendar.timeZone = zone
        }
        return calendar
    }()
    
    let dashboardVM: DashboardVM = {
        return DashboardVM()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        medicationTableView.register(CareDetailTableCell.self)
        scheduleButton.setTitleColor(Color.appPrimaryColor.uiColor, for: .normal)
        setupCalendar()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchMedications(date: selectedDate)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if vacalendarView?.frame == .zero {
            vacalendarView?.frame = CGRect(
                x: 0,
                y: weekDaysView.frame.maxY,
                width: view.frame.width * 0.97,
                height: calenderView.frame.height * 3
            )
            vacalendarView?.setup()
        }
    }
    
    private func setupCalendar() {
        let calendar = VACalendar(selectedDate: Date(), calendar: defaultCalendar)

        vacalendarView = VACalendarView(frame: .zero, calendar: calendar)
        vacalendarView?.showDaysOut = true
        vacalendarView?.selectionStyle = .single
        vacalendarView?.dayViewAppearanceDelegate = self
        vacalendarView?.calendarDelegate = self
        vacalendarView?.scrollDirection = .horizontal
        vacalendarView?.changeViewType()
        calenderView.addSubview(vacalendarView ?? UIView())
    }
    
    @IBAction private func scheduleButton(_ sender: Any) {
        selectedTab = .schedule
        refillButton.setTitleColor(Color.appBlackTextColor.uiColor, for: .normal)
        scheduleButton.setTitleColor(Color.appPrimaryColor.uiColor, for: .normal)
        medicationTableView.reloadData()
    }
    
    @IBAction private func refillButtonTapped(_ sender: Any) {
        selectedTab = .refill
        refillButton.setTitleColor(Color.appPrimaryColor.uiColor, for: .normal)
        scheduleButton.setTitleColor(Color.appBlackTextColor.uiColor, for: .normal)
        medicationTableView.reloadData()

    }
    
    func fetchMedications(date: Date) {
        loaderView.isHidden = false
        self.view.isUserInteractionEnabled = false
        let dateInIntervels = Int(date.timeIntervalSince1970 * 1000)
        dashboardVM.fetchMedications(date: dateInIntervels, patientId: Defaults.shared.selectedProfileID) { [weak self] _, error  in
            if error == nil {
                DispatchQueue.main.async {
                    if self?.dashboardVM.medications.isEmpty ?? false {
                         self?.updateSpinner()
                    } else {
                         self?.updateSpinner(isHidden: true)
                         self?.medicationTableView.reloadData()
                    }
                }
            } else {
                self?.updateSpinner()
            }
        }
    }
    
    private func updateSpinner(isHidden: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            self?.noMedicatioinView.isHidden = isHidden
            self?.loaderView.isHidden = true
            self?.view.isUserInteractionEnabled = true
        }
    }
    
    func updateMedication(date: Date, patientMedicationId: Int, statusCode: Int) {
        loaderView.isHidden = false
        self.view.isUserInteractionEnabled = false
        let dateInIntervels = Int(date.timeIntervalSince1970 * 1000)
        dashboardVM.updateMedication(date: dateInIntervels, statusCode: statusCode, patientMedicationID: patientMedicationId) { [weak self] _, error in
            if error == nil {
                DispatchQueue.main.async {
                    Defaults.shared.shouldUpdateDashboard = true
                    self?.loaderView.isHidden = true
                    self?.view.isUserInteractionEnabled = true
                }
            } else {
                self?.loaderView.isHidden = true
                self?.view.isUserInteractionEnabled = true
            }
        }
    }
    
}

extension MedicationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dashboardVM.medications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch selectedTab {
        
        case .schedule:
            let cell = tableView.dequeueReusableCell(for: indexPath) as CareDetailTableCell
            scheduleButtonView.backgroundColor = Color.appPrimaryColor.uiColor
            refillButtonView.backgroundColor = .white
            cell.titileLabel.text = dashboardVM.medications[indexPath.row].name
            cell.subTitileLabel.text = dashboardVM.medications[indexPath.row].dosage
            let image = dashboardVM.medications[indexPath.row].consumptionStatus == 2 ? #imageLiteral(resourceName: "Icon-Selected") : #imageLiteral(resourceName: "Icon-UnSelected")
            cell.checkButton.setImage(image, for: .normal)
            cell.selectionStyle = .none
            cell.isUserInteractionEnabled = true
            cell.titileLabel.textColor = .black
            if !selectedDate.isSameDate(Date()) {
                cell.isUserInteractionEnabled = false
                cell.titileLabel.textColor = .lightGray
                cell.subTitileLabel.textColor = .lightGray
            }
            return cell
        case .refill:
            let cell = tableView.dequeueReusableCell(for: indexPath) as RefillCell
            scheduleButtonView.backgroundColor = .white
            refillButtonView.backgroundColor = Color.appPrimaryColor.uiColor
            cell.selectionStyle = .none
            return cell
        }
    
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CareDetailTableCell else {
            return
        }
        cell.isChecked = cell.isChecked ? false : true
        let image = cell.isChecked ? #imageLiteral(resourceName: "Icon-Selected") : #imageLiteral(resourceName: "Icon-UnSelected")
        cell.checkButton.setImage(image, for: .normal)
        guard let medicationId = dashboardVM.medications[indexPath.row].patientMedicationId, let consumptionStatus = dashboardVM.medications[indexPath.row].consumptionStatus else {
            return
        }
        dashboardVM.medications[indexPath.row].consumptionStatus = consumptionStatus == 1 ? 2 : 1
        updateMedication(date: selectedDate, patientMedicationId: medicationId, statusCode: 2)
       // tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
}

extension MedicationViewController: VADayViewAppearanceDelegate {
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return UIColor(red: 199 / 255, green: 199 / 255, blue: 212 / 255, alpha: 1.0)
        case .selected:
            return .white
        case .unavailable:
            return .lightGray
        default:
            return UIColor(red: 76 / 255, green: 75 / 255, blue: 94 / 255, alpha: 1.0)
        }
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            return Color.appPrimaryColor.uiColor
        default:
            return .clear
        }
    }
    
    func font(for state: VADayState) -> UIFont {
        return UIFont.systemFont(ofSize: 12)
    }
    
    func shape() -> VADayShape {
        return .circle
    }
}

extension MedicationViewController: VACalendarViewDelegate {
    
    func selectedDate(_ date: Date) {
        selectedDate = date
        vacalendarView?.startDate = date
        fetchMedications(date: date)
    }

}
