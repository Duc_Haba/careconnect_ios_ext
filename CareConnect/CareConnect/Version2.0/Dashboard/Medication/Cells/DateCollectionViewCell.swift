//
//  DateCollectionViewCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 31/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class DateCollectionViewCell: UICollectionViewCell, NibLoadableView {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
