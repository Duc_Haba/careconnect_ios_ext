//
//  RefillCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 31/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class RefillCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var remainingLabel: UILabel!
    @IBOutlet weak var lastFilledDateLabe: UILabel!
    @IBOutlet weak var medicationName: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.dropShadow(cornerRadius: 20)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction private func refillButtonTapped(_ sender: Any) {
    }
    
}
