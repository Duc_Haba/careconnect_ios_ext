//
//  Medication.swift
//  CareConnect
//
//  Created by Venugopal S A on 02/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation

class Medication {
    var name: String?
    var consumed: Bool?
    var dosage: String?
    var asNeeded: Bool?
    var consumptionStatus: Int?
    var patientMedicationId: Int?
}
