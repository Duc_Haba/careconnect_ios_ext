//
//  AppointmentsModel.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 16/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class AppointmentsModel: Object, Codable {
    dynamic var reason: String?
    dynamic var doctorId: Int = 0
    dynamic var date: Double = 0.0
    dynamic var startTime: Double = 0.0
    dynamic var endTime: Double = 0.0
    dynamic var slotId: Int = 0
    dynamic var appointmentStatus: Int = 0
    dynamic var notes: String?
    dynamic var latitude: Double = 0.0
    dynamic var longitude: Double = 0.0
    dynamic var appointmentId: Int = 0
    dynamic var address: String?
    dynamic var name: String?
    dynamic var patientId: Int = 0
    dynamic var imageUrl: String?

    override static func primaryKey() -> String? {
        return "appointmentId"
    }
    
    private enum CodingKeys: String, CodingKey {
        case reason
        case date
        case startTime
        case endTime
        case slotId
        case appointmentStatus
        case notes
        case latitude
        case longitude
        case appointmentId
        case address
        case name
        case patientId
        case imageUrl
        case doctorId
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        reason = try container.decodeIfPresent(String.self, forKey: .reason)
        date = try container.decodeIfPresent(Double.self, forKey: .date) ?? 0.0
        startTime = try container.decodeIfPresent(Double.self, forKey: .startTime) ?? 0.0
        endTime = try container.decodeIfPresent(Double.self, forKey: .endTime) ?? 0.0
        slotId = try container.decodeIfPresent(Int.self, forKey: .slotId) ?? 0
        appointmentStatus = try container.decodeIfPresent(Int.self, forKey: .appointmentStatus) ?? 0
        notes = try container.decodeIfPresent(String.self, forKey: .notes)
        latitude = try container.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        longitude = try container.decodeIfPresent(Double.self, forKey: .longitude) ?? 0.0
        appointmentId = try container.decodeIfPresent(Int.self, forKey: .appointmentId) ?? 0
        address = try container.decodeIfPresent(String.self, forKey: .address)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl)
        patientId = try container.decodeIfPresent(Int.self, forKey: .patientId) ?? 0
        doctorId = try container.decodeIfPresent(Int.self, forKey: .doctorId) ?? 0

    }
}

extension AppointmentsModel {
    func encode(to encoder: Encoder) throws {
    }
}

extension AppointmentsModel {
    class func storeInDataBase(appiontmentData: [AppointmentsModel]) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(appiontmentData, update: .all)
        }
    }
    
    func storeTeamDataToDB() {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(self, update: .all)
        }
    }
}
