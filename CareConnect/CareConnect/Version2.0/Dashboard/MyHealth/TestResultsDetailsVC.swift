//
//  TestResultsDetailsVC.swift
//  CareConnect
//
//  Created by Venugopal S A on 05/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class TestResultsDetailsVC: UIViewController, ScrollableGraphViewDataSource {
    
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var orederedByName: UILabel!
    @IBOutlet weak var resultName: UILabel!
    @IBOutlet weak var orderedDate: UILabel!
    @IBOutlet weak var testsTableView: UITableView!
    
    var graph: ScrollableGraphView?
    
    var labResultId: String?
    var labResultsVM = LabResultsVM()
    var selectedItemDate: Double?
    var testCode: String?
    var graphViewTag = 100
    var displayList: [DateLabWiseLabResults]?
    var graphList = [DateLabWiseLabResults]()
    
    var graphDisplayDataIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        testsTableView.register(TestDetailsCell.self)
        loaderView.isHidden = true
        getTestDetails()
        resultName.text = testCode ?? ""
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "dataLoaded"), object: nil)
    }
    
    @IBAction private func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getTestDetails() {
        loaderView.isHidden = false
        labResultsVM.fetchLabTestDetails(labResults: labResultId ?? "1") { [weak self] isSuccess in
            if isSuccess {
                self?.displayList = [DateLabWiseLabResults]()
                DispatchQueue.main.async {
                    self?.loaderView.isHidden = true
                    self?.displayList = (self?.labResultsVM.labTestDetails.filter{$0.date == self?.selectedItemDate})
                    self?.testsTableView.reloadData()
                    self?.loadGraph(itemIndex: 0)
                }
            }
        }
    }
    
    func value(forPlot plot: Plot, atIndex pointIndex: Int) -> Double {
        switch plot.identifier {
        case "darkLine", "darkLineDot":
            guard let value = Double(graphList[pointIndex].labResultsDetails?.first?.result ?? "1") else {
                return 0
            }
            return value
        default:
            return 0
        }
    }
    
    func label(atIndex pointIndex: Int) -> String {
        return "\(Date.getDateStringFromTimeStamp(graphList[pointIndex].date ?? 0.0, format: graphDateFormate))"
    }
    
    func numberOfPoints() -> Int {
        return graphList.count
    }
    
    func createMultiPlotGraphOne(_ frame: CGRect) -> ScrollableGraphView {
        let graphView = ScrollableGraphView(frame: frame, dataSource: self)
        
        // Setup the line plot.
        let linePlot = LinePlot(identifier: "darkLine")
        
        linePlot.lineWidth = 1
        linePlot.lineColor = .white
        linePlot.lineStyle = ScrollableGraphViewLineStyle.smooth
        
        linePlot.shouldFill = true
        linePlot.fillType = ScrollableGraphViewFillType.gradient
        linePlot.fillGradientType = ScrollableGraphViewGradientType.linear
        linePlot.fillGradientStartColor = Color.graphgradientStartColor.uiColor
        linePlot.fillGradientEndColor = Color.graphGradientEndColor.uiColor
        linePlot.adaptAnimationType = ScrollableGraphViewAnimationType.elastic
        
        let dotPlot = DotPlot(identifier: "darkLineDot", dataSource: graphList) // Add dots as well.
        dotPlot.dataPointSize = 3
        dotPlot.dataPointFillColor = UIColor.white
        
        dotPlot.adaptAnimationType = ScrollableGraphViewAnimationType.elastic
        
        // Setup the reference lines.
        let referenceLines = ReferenceLines()
        
        referenceLines.referenceLineLabelFont = UIFont.boldSystemFont(ofSize: 8)
        referenceLines.referenceLineColor = UIColor.white.withAlphaComponent(0.2)
        referenceLines.referenceLineLabelColor = UIColor.white
        referenceLines.dataPointLabelColor = UIColor.white.withAlphaComponent(1)
        referenceLines.referenceLineNumberOfDecimalPlaces = 1
        
        // Setup the graph
        graphView.backgroundFillColor = Color.appPrimaryColor.uiColor
        graphView.dataPointSpacing = 80
        graphView.leftmostPointPadding = 100
        graphView.shouldAnimateOnStartup = true
        //graphView.shouldAdaptRange = true
        guard let minValue = Double(graphList.first?.labResultsDetails?.first?.lowerLimit ?? "1"), let maxValue = Double(graphList.first?.labResultsDetails?.first?.higherLimit ?? "1"), let unit = graphList.first?.labResultsDetails?[0].units  else {
            return ScrollableGraphView()
        }
        referenceLines.referenceLineUnits = unit
        let padding = getPadding(min: minValue, max: maxValue)
        graphView.rangeMax = maxValue + padding
        graphView.rangeMin = minValue - padding
        // Add everything to the graph.
        graphView.addReferenceLines(referenceLines: referenceLines)
        graphView.addPlot(plot: linePlot)
        graphView.addPlot(plot: dotPlot)
        
        return graphView
    }
    
    private func getPadding(min: Double, max: Double) -> Double {
        return max - min / 2
    }
    
    func loadGraph(itemIndex: Int) {
        graphList = []
        if let code = displayList?.first?.labResultsDetails?[itemIndex].code {
            for detail in labResultsVM.labTestDetails {
                let filteredGraphList = DateLabWiseLabResults()
                filteredGraphList.labResultsDetails = []
                filteredGraphList.date = detail.date
                if let filteredList = detail.labResultsDetails?.filter({ $0.code == code }) {
                    filteredGraphList.labResultsDetails?.append(contentsOf: filteredList)
                }
                if !(filteredGraphList.labResultsDetails?.isEmpty ?? true) {
                    graphList.append(filteredGraphList)
                }
            }
            
            if let graphView = graph?.viewWithTag(graphViewTag) {
                graphView.removeFromSuperview()
                graph = createMultiPlotGraphOne(self.graphView.frame)
                graph?.tag = graphViewTag
                self.view.addSubview(graph ?? UIView())
            } else {
                graph = createMultiPlotGraphOne(self.graphView.frame)
                graph?.tag = graphViewTag
                self.view.addSubview(graph ?? UIView())
            }
        }
        let minLineImage = UIImageView(frame: CGRect(x: graphView.frame.origin.x + 40, y: graphView.frame.maxY - 40 - graphView.frame.height / 6, width: UIScreen.main.bounds.width, height: 2))
        minLineImage.image = UIImage(named: "line")
        let maxLineImage = UIImageView(frame: CGRect(x: graphView.frame.origin.x + 40, y: graphView.frame.minY + 20 + graphView.frame.height / 6, width: UIScreen.main.bounds.width, height: 2))
        maxLineImage.image = UIImage(named: "line")
        self.view.addSubview(minLineImage)
        self.view.addSubview(maxLineImage)
    }
    
}

extension TestResultsDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let labResultcount = displayList?.first?.labResultsDetails?.count else {
            return 0
        }
        return labResultcount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(for: indexPath) as TestDetailsCell
        cell.name.text = displayList?.first?.labResultsDetails?[indexPath.row].resultName
        guard let result = Double(displayList?.first?.labResultsDetails?[indexPath.row].result ?? "1"), let minValue = Double(displayList?.first?.labResultsDetails?.first?.lowerLimit ?? "1")  else {
            return UITableViewCell()
        }
        cell.testDate.text = Date.getDateStringFromTimeStamp(displayList?.first?.labResultsDetails?[indexPath.row].issuedDate ?? 0, format: labResultsDateFormate)
        cell.value.text = "\(result) " + (displayList?.first?.labResultsDetails?[indexPath.row].units ?? "")
        cell.selectionStyle = .none
        if result > minValue {
            cell.valueView.setGradientBackground(cornerRadius: 10, startColor: Color.labTestValueLowColor.cgColor, endColor: Color.labTestValueLowColor.cgColor)
            cell.testStatus.textColor = Color.labTestValueLowColor.uiColor
            cell.testStatus.text = "Low"
            cell.arrowImageView.image = UIImage(named: "down")
        } else {
            cell.testStatus.text = "Normal"
            cell.arrowImageView.image = UIImage(named: "up")
            cell.testStatus.textColor = Color.labTestValueNormalStartColor.uiColor
            cell.valueView.setGradientBackground(cornerRadius: 10, startColor: Color.labTestValueNormalStartColor.cgColor, endColor: Color.labTestValueNormalEndColor.cgColor)
        }
        
        if graphDisplayDataIndex == indexPath.row {
            cell.containerView.layer.borderColor = Color.appPrimaryColor.cgColor
            cell.containerView.layer.borderWidth = 2
        } else {
            cell.containerView.layer.borderColor = UIColor.clear.cgColor
            cell.containerView.layer.borderWidth = 0
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        graphDisplayDataIndex = indexPath.row
        self.testsTableView.reloadData()
        self.loadGraph(itemIndex: indexPath.row)
    }
}
