//
//  CustomSegmentControll.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 20/06/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

protocol SegmentViewDelegate: class {
    func segmentView(_ segmentView: SegmentView, didSelectItemAt index: Int)
}

class SegmentView: UICollectionView {
    
    enum Segment: String {
        case scrollToTop
        case indexItem
    }
    
    var selectedIndex: Int = 0 {
        didSet {
            reloadContent()
        }
    }
    weak var segmentViewDelegate: SegmentViewDelegate?
    var items: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        delegate = self
        dataSource = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentInset = .zero
    }
    
    private func reloadContent() {
        reloadData()
        scrollToItem(at: IndexPath(item: selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
    }
    
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout

extension SegmentView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard selectedIndex != indexPath.item else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Segment.scrollToTop.rawValue), object: nil, userInfo: [Segment.indexItem.rawValue: indexPath.item])
            return
        }
        
        selectedIndex = indexPath.item
        segmentViewDelegate?.segmentView(self, didSelectItemAt: selectedIndex)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyHealthSegmentViewCell", for: indexPath ) as? MyHealthSegmentViewCell else {
            return UICollectionViewCell()
        }
        cell.name.text = items[indexPath.row]
        cell.isSelected = indexPath.item == selectedIndex
        cell.currentIndex = indexPath.row
        cell.scrollIndicator.backgroundColor = .white
        if indexPath.item == selectedIndex {
            cell.scrollIndicator.backgroundColor = Color.appPrimaryColor.uiColor
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = items[indexPath.row].size(font: .systemFont(ofSize: 12))
        return CGSize(width: size.width + 30, height: collectionView.bounds.size.height)
    }
}
