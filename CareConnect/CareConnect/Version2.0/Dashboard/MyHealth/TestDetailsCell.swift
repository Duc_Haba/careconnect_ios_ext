//
//  TestDetailsCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 05/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class TestDetailsCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var testDate: UILabel!
    @IBOutlet weak var testStatus: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var valueView: UIView!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.dropShadow(cornerRadius: 20)
        testStatus.textColor = Color.labTestValueNormalStartColor.uiColor
        valueView.setGradientBackground(cornerRadius: 10, startColor: Color.labTestValueNormalStartColor.cgColor, endColor: Color.labTestValueNormalEndColor.cgColor)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
