//
//  Lab.swift
//  CareConnect
//
//  Created by Venugopal S A on 09/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation

class LabResultsDM {
    
    var status: String?
    var effectiveDate: Double?
    var issuedData: Double?
    var resultCode: String?
    var performerName: String?
    var displayresults: String?
    var id: String?
    
}
