//
//  MyHealthVC.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 26/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

enum HealthType: Int, CaseIterable {
    case allergy
    case immunisation
    case condition
    case labresults
    
    var title: String {
        var text = ""
        switch self {
        case .allergy:
            text = "Allergies"
        case .immunisation:
            text = "Immunisations"
        case .condition:
            text = "Health Conditions"
        case .labresults:
            text = "Test Results"
        }
        return text
    }
}

class MyHealthVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentCollectionView: SegmentView!
    @IBOutlet weak var loaderStackView: UIView!
    
    var allergiesVM = AllergyVM()
    var labResultsVM = LabResultsVM()
    var conditionsVM = ConditionsVM()
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentCollectionView.items = [HealthType.allergy.title, HealthType.immunisation.title, HealthType.condition.title, HealthType.labresults.title]
        segmentCollectionView.segmentViewDelegate = self
        tableView.register(MyHealthTableCell.self)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        reloadData()
    }
    
    private func reloadData() {
        loaderStackView.isHidden = false
        allergiesVM.allergies = []
        labResultsVM.labResults = []
        switch selectedIndex {
        case HealthType.allergy.rawValue:
            allergiesVM.fetchAllergies(patientId: Defaults.shared.selectedProfileID) {[weak self] success, error in
                DispatchQueue.main.async {
                    self?.loaderStackView.isHidden = true
                    if success {
                        self?.tableView.reloadData()
                    }
                }
            }
        case HealthType.immunisation.rawValue:
            allergiesVM.fetchImmunozationData(patientId: Defaults.shared.selectedProfileID) {[weak self] success, error in
                DispatchQueue.main.async {
                    self?.loaderStackView.isHidden = true
                    if success {
                        self?.tableView.reloadData()
                    }
                }
            }
        case HealthType.labresults.rawValue:
            labResultsVM.fetchLabResults(patientId: Defaults.shared.selectedProfileID) {[weak self] success in
                DispatchQueue.main.async {
                    self?.loaderStackView.isHidden = true
                    if success {
                        self?.tableView.reloadData()
                    }
                }
            }
        case HealthType.condition.rawValue:
            conditionsVM.getConditions(patientId: Defaults.shared.selectedProfileID) {[weak self] success in
                DispatchQueue.main.async {
                    self?.loaderStackView.isHidden = true
                    if success {
                        self?.tableView.reloadData()
                    }
                }
            }
        default:
            break
        }
        
    }

}

extension MyHealthVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        switch selectedIndex {
        case HealthType.immunisation.rawValue, HealthType.allergy.rawValue:
            count = allergiesVM.allergies.count
        case HealthType.labresults.rawValue:
            count = labResultsVM.labResults.count
        case HealthType.condition.rawValue:
            count = conditionsVM.conditions.count
        default:
            break
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MyHealthTableCell.className, for: indexPath) as? MyHealthTableCell else { return UITableViewCell() }
        switch selectedIndex {
        case HealthType.immunisation.rawValue, HealthType.allergy.rawValue:
            let date = allergiesVM.allergies[indexPath.row].onsetDateTime
            cell.firstLabel.text = Date.getStringFromDateString(date ?? "", format: prescriptionDateFormat)
            cell.titileLabel.text = allergiesVM.allergies[indexPath.row].displayName
            cell.lastLabel.text = allergiesVM.allergies[indexPath.row].allergen
        case HealthType.labresults.rawValue:
            let date = labResultsVM.labResults[indexPath.row].issuedData
            cell.firstLabel.text = Date.getDateStringFromTimeStamp(date ?? 0, format: labResultsDateFormate)
            cell.titileLabel.text = labResultsVM.labResults[indexPath.row].resultCode
            cell.lastLabel.text = labResultsVM.labResults[indexPath.row].displayresults
        case HealthType.condition.rawValue:
            let date = conditionsVM.conditions[indexPath.row].onsetDateTime
            cell.firstLabel.text = Date.getStringFromDateString(date ?? "", format: prescriptionDateFormat)
            cell.titileLabel.text = conditionsVM.conditions[indexPath.row].display
            cell.lastLabel.text = ""
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch selectedIndex {
        case HealthType.immunisation.rawValue, HealthType.allergy.rawValue:
           break
        case HealthType.labresults.rawValue:
            
            guard let testsResultsVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "TestResultsDetailsVC") as? TestResultsDetailsVC else {
                return
            }
            testsResultsVC.labResultId = labResultsVM.labResults[indexPath.row].id
            testsResultsVC.selectedItemDate = labResultsVM.labResults[indexPath.row].effectiveDate
            testsResultsVC.testCode = labResultsVM.labResults[indexPath.row].resultCode
            self.navigationController?.pushViewController(testsResultsVC, animated: true)
        case HealthType.condition.rawValue:
            break
            
        default:
            break
        }
    }
    
}

extension MyHealthVC: SegmentViewDelegate {
    func segmentView(_ segmentView: SegmentView, didSelectItemAt index: Int) {
        selectedIndex = index
        reloadData()
    }
    
}
