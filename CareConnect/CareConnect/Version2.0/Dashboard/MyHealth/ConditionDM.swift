//
//  ConditionDM.swift
//  CareConnect
//
//  Created by Venugopal S A on 26/06/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation

class ConditionDM {
    
    var system: String?
    var code: String?
    var display: String?
    var dateRecorded: String?
    var onsetDateTime: String?
    
}
