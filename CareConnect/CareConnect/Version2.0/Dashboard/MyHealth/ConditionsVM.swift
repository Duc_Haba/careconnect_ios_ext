//
//  RecordsVM.swift
//  CareConnect
//
//  Created by Venugopal S A on 26/06/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import Rudder

typealias CompletionBlock = (_ isSccuss: Bool) -> Void
class ConditionsVM {
    
    var conditions = [ConditionDM]()
    var conditionInfo: String?
    
    func getConditions(patientId: Int, completion: @escaping CompletionBlock) {
        
        let headers = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let path = Apiconstants.baseUrl + CareConnectURLs.conditions.rawValue + "?patientId=" + "\(patientId)"
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        
        _ = NetworkManager.request(request) { json, error, _ in
            
            if error == nil {
                
                guard let response = json?.dictionaryObject, let entries = response["entry"] as? [[String: Any]] else {
                    return
                }
                for entry in entries {
                    
                    guard let resourse = entry["resource"] as? [String: Any], let setDate = resourse["onsetDateTime"] as? String, let code = resourse["code"]  as? [String: Any], let text = code["text"] as? String, let coding = code["coding"] as? [[String: Any]] else {
                        return
                    }
                    guard let conditionCode = coding[0]["code"] as? String else {
                        return
                    }
                    self.conditions.removeAll()
                    
                    let condition = ConditionDM()
                    condition.code = conditionCode
                    condition.display = text
                    //condition.system = system
                    //condition.dateRecorded = recordedDate
                    condition.onsetDateTime = setDate
                    
                    self.conditions.append(condition)
                }
                completion(true)
                
            } else {
                completion(false)
            }
        }
        
    }
    
    func fetchConditionInfo(condition: ConditionDM, completion: @escaping CompletionBlock) {
        
        guard let code = condition.code, let system = condition.system else {
            return
        }
        let systemCode = system.replacingOccurrences(of: "urn:oid:", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        let url = "https://connect.medlineplus.gov/service?mainSearchCriteria.v.c=\(code)&mainSearchCriteria.v.cs=\(systemCode)&mainSearchCriteria.v.dn=&informationRecipient.languageCode.c=en&knowledgeResponseType=application%2Fjson"
        let headers = HeaderConfiguration.epicHeader.header
        
        guard let request = NetworkManager.requestForURL(url, method: .Get, params: [:], headers: headers) else {
            return
        }
        
        _ = NetworkManager.request(request) { json, error, _ in
            
            if error == nil {
                
                guard let response = json?.dictionaryObject, let feed = response["feed"] as? [String: Any], let entries = feed["entry"] as? [[String: Any]] else {
                    return
                }
                guard let summary = entries[0]["summary"] as? [String: Any], let info = summary["_value"] as? String else {
                    return
                }
                self.conditionInfo = info
                completion(true)
                
            } else {
                completion(false)
            }
        }
    }
    
}
