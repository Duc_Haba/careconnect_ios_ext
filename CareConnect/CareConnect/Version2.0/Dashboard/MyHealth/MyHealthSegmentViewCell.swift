//
//  MyHealthSegmentViewCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 05/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class MyHealthSegmentViewCell: UICollectionViewCell, NibLoadableView {
    
    @IBOutlet weak var scrollIndicator: UIView!
    @IBOutlet weak var name: UILabel!
    var currentIndex: Int?
    
}
