//
//  LabResultsVM.swift
//  CareConnect
//
//  Created by Venugopal S A on 09/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import Rudder

class LabResultsVM {
    
    var labResults = [LabResultsDM]()
    var labTestDetails = [DateLabWiseLabResults]()
    
    func fetchLabResults(patientId: Int, completion: @escaping CompletionBlock) {
        let headers = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        
        let path = Apiconstants.baseUrl + CareConnectURLs.getLabResults.rawValue + "?patientId=\(patientId)"
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        
        _ = NetworkManager.request(request) { json, error, _ in
            
            if error == nil {
                
                guard let response = json?.dictionaryObject, let entries = response["entry"] as? [[String: Any]] else {
                    return
                }
                for entry in entries {
                    let labResult = LabResultsDM()
                    
                    guard let resourse = entry["resource"] as? [String: Any], let effectiveDate = resourse["effectiveDateTime"] as? Double, let issuedDate = resourse["issued"] as? Double, let id = resourse["id"] as? String else {
                        return
                    }
                    labResult.effectiveDate = effectiveDate
                    labResult.issuedData = issuedDate
                    labResult.id = id
                    if let code = resourse["code"]  as? [String: Any], let text = code["text"] as? String {
                        labResult.resultCode = text
                    }
                    
                    if let results = resourse["result"] as? [[String: Any]] {
                        var testResults: [String] = []
                        for result in results {
                            guard let displayName = result["display"] as? String else {
                                return
                            }
                            testResults.append(displayName)
                        }
                        labResult.displayresults = "\(testResults)"
                        labResult.displayresults = labResult.displayresults?.replacingOccurrences(of: "[\\[\\]^+<>\"]", with: "", options: .regularExpression, range: nil)
                    }
                    self.labResults.append(labResult)
                }
                completion(true)
                
            } else {
                completion(false)
            }
        }
    }
    
    func fetchLabTestDetails(labResults: String, completion: @escaping CompletionBlock) {
        let headers = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        
        let path = Apiconstants.baseUrl + CareConnectURLs.getLabResultDetaiils.rawValue + "?labResultId=\(labResults)"
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        
        _ = NetworkManager.request(request) { json, error, _ in
            
            if error == nil {
                
                guard let response = json?.dictionaryObject, let dataObjects = response["data"] as? [[String: Any]] else {
                    return
                }
                for dataObject in dataObjects {
                    let dateWiseDetails = DateLabWiseLabResults()
                    dateWiseDetails.labResultsDetails = [LabTestDetails]()
                    if let date = dataObject["date"] as? Double {
                      dateWiseDetails.date = date
                    }
                    guard let labTestResults = dataObject["labResults"] as? [[String: Any]] else {
                        return
                    }
                    for labTestResult in labTestResults {
                        let labResultDetails = LabTestDetails()
                        if let id = labTestResult["id"] as? Int, let resultName = labTestResult["resultName"] as? String, let resultValue = labTestResult["result"] as? String, let interpretation = labTestResult["interpretation"] as? String, let lowerLimit = labTestResult["lowerLimit"] as? String, let higherLimit = labTestResult["higherLimit"] as? String, let units = labTestResult["units"] as? String, let code = labTestResult["code"] as? String, let issuedDate = labTestResult["issuedDate"] as? Double {
                            labResultDetails.id = id
                            labResultDetails.resultName = resultName
                            labResultDetails.lowerLimit = lowerLimit
                            labResultDetails.higherLimit = higherLimit
                            labResultDetails.units = units
                            labResultDetails.result = resultValue
                            labResultDetails.interpretation = interpretation
                            labResultDetails.code = code
                            labResultDetails.issuedDate = issuedDate
                           // self.labTestDetails.append(labResultDetails)
                            dateWiseDetails.labResultsDetails?.append(labResultDetails)
                        }

                    }
                    self.labTestDetails.append(dateWiseDetails)
                }
                completion(true)
                
            } else {
                completion(false)
            }
        }
    }
}
