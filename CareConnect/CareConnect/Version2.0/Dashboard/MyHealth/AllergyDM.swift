//
//  AllergyDM.swift
//  CareConnect
//
//  Created by Venugopal S A on 04/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation

class AllergyDM {
    var criticality: String?
    var code: String?
    var displayName: String?
    var noteText: String?
    var dateRecorded: String?
    var onsetDateTime: String?
    var allergen: String?
}
