//
//  MyHealthTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 26/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class MyHealthTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var titileLabel: UILabel!
    @IBOutlet weak var lastLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.dropShadow(cornerRadius: 10)
    }

}
