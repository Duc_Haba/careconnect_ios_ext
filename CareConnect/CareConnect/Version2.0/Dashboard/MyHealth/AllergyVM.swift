//
//  AllergyVM.swift
//  CareConnect
//
//  Created by Venugopal S A on 04/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import Rudder

typealias AllergyCompletionBlock = (_ isSccuss: Bool, _ error: Error?) -> Void

class AllergyVM {
    
    var allergies = [AllergyDM]()
    
    func fetchAllergies(patientId: Int, completion: @escaping AllergyCompletionBlock) {
        
        allergies = []
        let headers = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let path = Apiconstants.baseUrl + CareConnectURLs.allergies.rawValue + "?patientId=" + "\(patientId)"
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        
        _ = NetworkManager.request(request) { json, error, _ in
            
            if error == nil {
                
                guard let response = json?.dictionaryObject, let entries = response["entry"] as? [[String: Any]] else {
                    return
                }
                for entry in entries {
                    
                    let allergy = AllergyDM()

                    guard let resourse = entry["resource"] as? [String: Any] else {
                        return
                    }
                    if let criticality = resourse["criticality"] as? String {
                        allergy.criticality = criticality
                    }
                    if let reaction = resourse["reaction"] as? [[String: Any]], let manifestation = reaction[0]["manifestation"] as? [String: Any], let allergyName = manifestation["text"] as? String {
                        allergy.displayName = allergyName
                    }
                    
                    if let recordedDate = resourse["recordedDate"] as? String {
                        allergy.dateRecorded = recordedDate
                    }
                    if let setDate = resourse["onset"] as? String {
                        allergy.onsetDateTime = setDate
                    }
                    
                    if let substance = resourse["substance"]  as? [String: Any], let text = substance["text"] as? String, let coding = substance["coding"] as? [[String: Any]], let conditionCode = coding[0]["code"] as? String {
                        
                        allergy.code = conditionCode
                        allergy.allergen = text
                    }
                    
                    var text = "--"
                    if  let note = resourse["note"] as? [String: Any], let noteText = note["text"] as? String {
                        text = noteText
                    }
                    allergy.noteText = text
                  
                    self.allergies.append(allergy)
                }
                completion(true, nil)
                
            } else {
                completion(false, error)
            }
        }
    }
    
    func fetchImmunozationData(patientId: Int, completion: @escaping AllergyCompletionBlock) {
        allergies = []
        let headers = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let path = Apiconstants.baseUrl + CareConnectURLs.immunizations.rawValue + "?patientId=" + "\(patientId)"
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        
        _ = NetworkManager.request(request) { json, error, _ in
            
            if error == nil {
                
                guard let response = json?.dictionaryObject, let entries = response["entry"] as? [[String: Any]] else {
                    return
                }
                for entry in entries {
                    
                    let allergy = AllergyDM()
                    
                    guard let resourse = entry["resource"] as? [String: Any] else {
                        return
                    }
                   
                    if let vaccineCode = resourse["vaccineCode"] as? [String: Any], let displayName = vaccineCode["text"] as? String, let coding = vaccineCode["coding"] as? [[String: Any]], let conditionCode = coding[0]["code"] as? String {
                        allergy.displayName = displayName
                        allergy.code = conditionCode
                    }
                    
                    if let recordedDate = resourse["date"] as? String {
                        allergy.onsetDateTime = recordedDate
                    }
                    
                    self.allergies.append(allergy)
                }
                completion(true, nil)
                
            } else {
                completion(false, error)
            }
        }
    }
}
