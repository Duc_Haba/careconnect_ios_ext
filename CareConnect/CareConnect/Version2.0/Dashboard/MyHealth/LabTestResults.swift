//
//  LabTestResults.swift
//  CareConnect
//
//  Created by Venugopal S A on 07/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation

class LabTestDetails {
    var id: Int?
    var panelName: String?
    var resultName: String?
    var code: String?
    var codeFamily: String?
    var result: String?
    var units: String?
    var interpretation: String?
    var referenceRange: String?
    var lowerLimit: String?
    var higherLimit: String?
    var effectiveDate: Double?
    var issuedDate: Double?
}

class DateLabWiseLabResults {
    var date: Double?
    var labResultsDetails: [LabTestDetails]?
}
