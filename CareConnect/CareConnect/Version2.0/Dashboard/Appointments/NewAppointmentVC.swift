//
//  NewAppointmentVC.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 29/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

protocol CreateAppointmentDelegate: class {
    func didCreateAppointment(date: Double?)
}

enum AppointmentStatusCode: Int {
    case create = 0
    case reschedule
    case cancel
}
typealias DismissBlock = (_ date: Double) -> Void

class NewAppointmentVC: UIViewController {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var loaderStackView: UIView!
    
    var appointmentsVM = AppointmentsVM()
    weak var createAppointmentDelegate: CreateAppointmentDelegate?
    var isPresented: Bool = true
    var footerView = UIView()
    var shouldShowResults: Bool = true
    var selectedDate = Date()
    var progressCount = AppointmentsVM.VCType.patient.rawValue
    weak var selectedAppointment: AppointmentsModel?
    var appointmentStatus = AppointmentStatusCode.create
    var dismissBlock: DismissBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        tableView.beginUpdates()
        //        tableView.endUpdates()
    }
    
    private func setupViews() {
        loaderStackView.isHidden = true
        nextButton.setGradientBackground(cornerRadius: 8)
        appointmentsVM.getAllData()
        appointmentsVM.progressCount = progressCount
        let image = isPresented ? #imageLiteral(resourceName: "Icon-Cancel") : #imageLiteral(resourceName: "Icon-Back")
        closeButton.setImage(image, for: .normal)
        nextButton.setTitle("Next", for: .normal)
        titleLabel.text = "New Appointment"
        tableView.backgroundColor = Color.grayBackgroundColor.uiColor
        if appointmentsVM.progressCount == AppointmentsVM.VCType.review.rawValue {
            tableView.backgroundColor = .white
            nextButton.setTitle("Confirm Appointment", for: .normal)
            titleLabel.text = "Review"
            footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        }
        if appointmentStatus == .reschedule {
            titleLabel.text = "Reschedule"
            appointmentsVM.selectedReason = selectedAppointment?.reason
        }
        
        nextButton.isHidden = appointmentsVM.progressCount != 3 && appointmentsVM.progressCount != 6
        let count = CGFloat(integerLiteral: appointmentsVM.progressCount)
        progressWidthConstraint.constant = UIScreen.main.bounds.width / (6.0 / count)
        progressView.layoutIfNeeded()
        regisetCells()
        switch appointmentsVM.progressCount {
        case AppointmentsVM.VCType.doctor.rawValue:
            fetchDoctors()
        case AppointmentsVM.VCType.symptom.rawValue:
            fetchSymptoms()
        case AppointmentsVM.VCType.availability.rawValue:
            fetchSlots()
        default:
            break
        }
    }
    
    private func regisetCells() {
        tableView.register(PatientTableCell.self)
        tableView.register(ReasonsTableCell.self)
        tableView.register(AppointmentDetailsTableCell.self)
        tableView.register(AppointmentPersonaTableCell.self)
        tableView.register(SymptomSearchTableCell.self)
        tableView.register(SearchResultTableCell.self)
        tableView.register(SymptomsTableCell.self)
        tableView.register(CalendarTableCell.self)
        tableView.register(AvailabilityTableCell.self)
        tableView.register(SlotsTableCell.self)
        tableView.registerReusableHeaderFooterView(HomeTableSectionHeader.self)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60
        tableView.tableFooterView = footerView
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 60
    }
    
    private func fetchDoctors() {
        loaderStackView.isHidden = false
        appointmentsVM.doctors = []
        appointmentsVM.fetchDoctors(completion: {[weak self] isSuccess in
            DispatchQueue.main.async {
                self?.loaderStackView.isHidden = true
                if isSuccess {
                    self?.tableView.reloadData()
                }
            }
        })
    }
    
    private func fetchSymptoms() {
        appointmentsVM.symptomResults = []
        loaderStackView.isHidden = false
        appointmentsVM.fetchSymptoms(completion: {[weak self] isSuccess in
            DispatchQueue.main.async {
                self?.loaderStackView.isHidden = true
                if isSuccess {
                    self?.tableView.reloadData()
                }
            }
        })
    }
    
    private func createAppoinment(appointmentStatus: AppointmentStatusCode) {
        loaderStackView.isHidden = false
        appointmentsVM.createAppoinment(appointmentStatus: appointmentStatus, patientID: selectedAppointment?.patientId ?? 0, appointmentID: selectedAppointment?.appointmentId ?? 0, completion: {[weak self] isSuccess in
            DispatchQueue.main.async {
                Defaults.shared.shouldUpdateDashboard = true
                self?.loaderStackView.isHidden = true
                self?.appointmentsVM.progressCount = 1
                let totalDate = (self?.appointmentsVM.selectedSlot?.startDate ?? 0.0) + (self?.appointmentsVM.selectedSlot?.date ?? 0.0)
                self?.dismissBlock?(totalDate)
                self?.appointmentsVM.resetData()
                self?.navigationController?.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    func fetchSlots() {
        loaderStackView.isHidden = false
        var id = appointmentsVM.selectedDoctor?.id
        if appointmentStatus == .reschedule {
            id = selectedAppointment?.doctorId
            let appointmentCurrentDate = Date.getDateFromTimeInterval(selectedAppointment?.date ?? 0)
            appointmentsVM.fromDate = appointmentCurrentDate
            appointmentsVM.todate = appointmentCurrentDate.endOfMonth()
            selectedDate = appointmentCurrentDate
        }
        appointmentsVM.fetchSlots(doctorId: id, completion: { [weak self] isSuccess in
            DispatchQueue.main.async {
                self?.loaderStackView.isHidden = true
                if isSuccess {
                    self?.tableView.reloadData()
                }
            }
        })
    }
    
    func pushSelfController() {
        guard appointmentsVM.progressCount != AppointmentsVM.VCType.review.rawValue else {
            createAppoinment(appointmentStatus: appointmentStatus)
            return
        }
        
        appointmentsVM.progressCount += 1
        let newAppointmentVC = NewAppointmentVC(nibName: NewAppointmentVC.className, bundle: nil)
        newAppointmentVC.appointmentsVM = appointmentsVM
        newAppointmentVC.progressCount = appointmentsVM.progressCount
        newAppointmentVC.selectedAppointment = selectedAppointment
        newAppointmentVC.appointmentStatus = appointmentStatus
        newAppointmentVC.dismissBlock = {[weak self] date in
            self?.dismissBlock?(date)
        }
        newAppointmentVC.isPresented = false
        navigationController?.pushViewController(newAppointmentVC, animated: true)
    }
    
    @IBAction private func didPressClose(_ sender: Any) {
        if isPresented {
            appointmentsVM.progressCount = 1
            dismiss(animated: true)
        } else {
            appointmentsVM.progressCount -= 1
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction private func didPressNext(_ sender: Any) {
        pushSelfController()
    }
}
