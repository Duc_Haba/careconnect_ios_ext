//
//  AppointmentDetailsTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 29/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class AppointmentDetailsTableCell: UITableViewCell, NibLoadableView {
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var acessoryButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var switchButton: UISwitch!
    
    var hideAddButton: Bool = false {
        didSet {
            addButton.isHidden = hideAddButton
            bottomConstraint.constant = hideAddButton ? 0 : 30
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
