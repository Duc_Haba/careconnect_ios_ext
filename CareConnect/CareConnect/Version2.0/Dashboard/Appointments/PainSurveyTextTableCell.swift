//
//  PainSurveyTextTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 28/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class PainSurveyTextTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
