//
//  SlotsTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 31/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

protocol CalendarSlotSelectedDelegate: class {
    func didSelectSlot(index: IndexPath)
}

class SlotsTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var collectionView: UICollectionView!

    weak var delegate: CalendarSlotSelectedDelegate?
    var availability: Availability = Availability() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        registerCells()
        setUpLayout()

    }
    
    private func registerCells() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(SlotsCollectionCell.self)
    }

    private func setUpLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 8
        layout.minimumLineSpacing = 8
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
    }

}

extension SlotsTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return availability.slots?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell?
        guard let symcell = collectionView.dequeueReusableCell(withReuseIdentifier: SlotsCollectionCell.className, for: indexPath) as? SlotsCollectionCell else { return UICollectionViewCell() }
        symcell.containerView.setGradientBackground(cornerRadius: 12, startColor: UIColor.gray.cgColor, endColor: UIColor.gray.cgColor)
        let time = (availability.slots?[indexPath.row].date ?? 0) + (availability.slots?[indexPath.row].startDate ?? 0)
        let thisDate = Date.getDateFromTimeInterval(time)
        if availability.slots?[indexPath.row].available ?? false && Date().isLessThan(thisDate) {
            symcell.containerView.setGradientBackground(cornerRadius: 12)
        }
        guard let day = availability.date, let startTime = availability.slots?[indexPath.row].startDate else {
            return UICollectionViewCell()
        }
        let date = startTime + day
        symcell.titileLabel.text = Date.getDateStringFromTimeStamp(date, format: genericTimeFormat)
        cell = symcell
        return cell ?? UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 84, height: 24)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let time = (availability.slots?[indexPath.row].date ?? 0) + (availability.slots?[indexPath.row].startDate ?? 0)
        let thisDate = Date.getDateFromTimeInterval(time)
        guard Date().isLessThan(thisDate) else { return }
        delegate?.didSelectSlot(index: indexPath)
    }
}
