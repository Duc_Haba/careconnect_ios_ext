//
//  CalendarTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 31/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import VACalendar

protocol CalendarDateSelectedDelegate: class {
    func didSelectDate(date: Date)
    func didSelectNextMonth()
    func didSelectPrevMonth()
}

class CalendarTableCell: UITableViewCell, NibLoadableView {
    
    @IBOutlet weak var monthHeaderView: VAMonthHeaderView! {
        didSet {
            let appereance = VAMonthHeaderViewAppearance(monthFont: .systemFont(ofSize: 16), monthTextColor: .lightGray, monthTextWidth: 200, previousButtonImage: #imageLiteral(resourceName: "Icon-left-black"), nextButtonImage: #imageLiteral(resourceName: "Icon-Right-Black")
            )
            monthHeaderView.delegate = self
            monthHeaderView.appearance = appereance
        }
    }
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var calendarContainerView: UIView!
    @IBOutlet weak var weekDaysView: VAWeekDaysView! {
        didSet {
            let appereance = VAWeekDaysViewAppearance(symbolsType: .veryShort, weekDayTextColor: UIColor(red: 199 / 255, green: 199 / 255, blue: 212 / 255, alpha: 1.0), separatorBackgroundColor: .clear, calendar: defaultCalendar)

            weekDaysView.appearance = appereance
        }
    }
    
    var datesRange = [Date]()
    weak var appointmentsVM: AppointmentsVM?
    var calendarView: VACalendarView?
    weak var delegate: CalendarDateSelectedDelegate?
    let defaultCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar
    }()
    
    var startMonthDate: Date?
    var endMonthDate: Date?
    var selectedDate: Date = Date() {
        didSet {
            setupCalendar()
        }

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    private func setupCalendar() {
        if let calView = calendarView {
            calView.removeFromSuperview()
        }
        let calendar = VACalendar(selectedDate: selectedDate, calendar: defaultCalendar)

        calendarView = VACalendarView(frame: .zero, calendar: calendar)
//        calendarView?.selectDates([selectedDate])
        calendarView?.showDaysOut = true
        calendarView?.selectionStyle = .single
        calendarView?.monthDelegate = monthHeaderView
        calendarView?.dayViewAppearanceDelegate = self
        calendarView?.calendarDelegate = self
        calendarView?.scrollDirection = .horizontal
        datesRange = Date.dates(from: Date(), to: appointmentsVM?.todate ?? Date())
        let availableDays = DaysAvailability.some(datesRange)
        calendarView?.setAvailableDates(availableDays)
        containerView?.addSubview(calendarView ?? UIView())
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if calendarView?.frame == .zero {
            calendarView?.frame = CGRect(
                x: calendarContainerView.frame.origin.x,
                y: calendarContainerView.frame.origin.y,
                width: calendarContainerView.frame.width,
                height: calendarContainerView.frame.height
            )
            calendarView?.setup()
        }
    }
}

extension CalendarTableCell: VACalendarViewDelegate, VAMonthHeaderViewDelegate {
    
    func didTapNextMonth() {
        calendarView?.nextMonth()
        delegate?.didSelectNextMonth()
    }
    
    func didTapPreviousMonth() {
        calendarView?.previousMonth()
        delegate?.didSelectPrevMonth()
    }
    
    func selectedDate(_ date: Date) {
        delegate?.didSelectDate(date: date)
    }

}

extension CalendarTableCell: VADayViewAppearanceDelegate {
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return UIColor(red: 199 / 255, green: 199 / 255, blue: 212 / 255, alpha: 1.0)
        case .selected:
            return .white
        case .unavailable:
            return .lightGray
        default:
            return UIColor(red: 76 / 255, green: 75 / 255, blue: 94 / 255, alpha: 1.0)
        }
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            return Color.appPrimaryColor.uiColor
        default:
            return .clear
        }
    }
    
    func font(for state: VADayState) -> UIFont {
        return UIFont.systemFont(ofSize: 12)
    }
    
    func shape() -> VADayShape {
        return .circle
    }
}
