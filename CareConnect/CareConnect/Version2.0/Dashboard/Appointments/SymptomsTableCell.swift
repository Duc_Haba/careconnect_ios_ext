//
//  SymptomsTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 31/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SymptomsTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var tagView: TagView!
    @IBOutlet weak var tagViewHeightConstraint: NSLayoutConstraint!
    
    private let spacing: CGFloat = 16.0
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
}
