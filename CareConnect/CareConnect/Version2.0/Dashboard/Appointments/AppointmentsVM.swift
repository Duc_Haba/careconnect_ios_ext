//
//  AppointmentsVM.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 29/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import Rudder

struct Appointment {
    var title: String?
    var location: String?
    var date: String?
    var isPastAppointment: Bool = true
    var id: Int?
    var isPhonecall: Bool = true
    var doctor: Doctor?
    var patient: Patient?
    var reason: String?
    var note: String?
}

struct Doctor {
    var name: String?
    var role: String?
    var id: Int?
    var address: String?
    var imageUrl: String?
}

struct Patient {
    var name: String?
    var relation: String?
    var id: Int?
    var imageUrl: String?
}

struct Availability {
    var date: Double?
    var slots: [Slots]?
}

struct Slots {
    var date: Double?
    var slotId: Int?
    var startDate: Double?
    var endDate: Double?
    var available: Bool = false
}

enum AppointmentType: String {
    case upcomming = "Upcoming"
    case past = "Past"
}

class AppointmentsVM {
    
    struct AppoinmentsModel {
        var type: AppointmentType?
        var numberOfRows: Int?
        var appointments: [AppointmentsModel]?
    }
    
    struct ListModel {
        var type: AppointmentType?
        var numberOfRows: Int?
        var appointments: [Appointment]?
    }
    
    enum VCType: Int {
        case patient = 1
        case reason
        case symptom
        case doctor
        case availability
        case review
    }
    
    var upcoming: [AppointmentsModel] = []
    var past: [AppointmentsModel] = []

    var model: [ListModel]?
    var allPatients: [Patient]?
    var allReasons: [String] = []
    var progressCount: Int = VCType.patient.rawValue
    var sectionTitles: [String] = []
    var allDoctors: [Doctor] = []
    var createdAppointment: Appointment?
    var selectedPatient: Patient?
    var selectedReason: String?
    var selectedDoctor: Doctor?
    var symptomResults: [String] = []
    var selectedSymptoms: [String] = []
    var showSlots: Bool = false
    var allAvailabilities: [Availability] = []
    var selectedSlot: Slots? = Slots()
    var fromDate = Date().startOfMonth()
    var todate = Date().endOfMonth()

    var appoinmentsModel: [AppoinmentsModel] = []
    var doctors: [Doctor] = []
    var symptoms = [String]()
    var slotsForAGivenDate = Availability()
    var firstAvailableSlot: Slots?
    
    func formModel() {
        appoinmentsModel = []
        if !upcoming.isEmpty {
            appoinmentsModel.append(AppoinmentsModel(type: .upcomming, numberOfRows: upcoming.count, appointments: upcoming))
        }
        if !past.isEmpty {
            appoinmentsModel.append(AppoinmentsModel(type: .past, numberOfRows: past.count, appointments: past))
        }
    }
    
    func getAllData() {
        let realmUserData = RealmManager.shared.getObjects(type: UserModel.self, with: Defaults.shared.selectedProfileID)
        let userData = realmUserData ?? UserModel()
        var patients = [Patient]()
        for each in userData.dependents {
            patients.append(Patient(name: each.name, relation: each.relationship, id: each.id, imageUrl: each.imageUrl))
        }
        allPatients = patients
        
        allReasons = ["I want to see a doctor to diagnose my symptoms", "Routine checkup or physical with my physician or nurse practitioner", "For lab work ordered by my provider", "I need a routine blood pressure check", "Optometry services", "Something else"]
        
        sectionTitles = ["Who is this appointment for?", "What’s the reason for your appointment?", "What symptoms are you experiencing?", "Based on your symptoms, we recommend the following doctors:", "When would you like to be seen?", ""]
    }
    
    func fetchSymptoms(completion: @escaping CompletionBlock) {
        let headers = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let path = Apiconstants.baseUrl + CareConnectURLs.getSymptoms.rawValue
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        _ = NetworkManager.request(request) { json, error, _ in
            
            if error == nil {
                
                guard let response = json?.dictionaryObject, let entries = response["data"] as? [String] else {
                    return
                }
                self.symptoms = []
                self.selectedSymptoms = []
                for each in entries {
                    self.symptoms.append(each.capitalized)
                }
                completion(true)
                
            } else {
                completion(false)
            }
        }
    }
    
    func fetchAppointments(completion: @escaping CompletionBlock) {
        let headers = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let path = Apiconstants.baseUrl + CareConnectURLs.getAppointments.rawValue + "?patientId=1"
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        _ = NetworkManager.request(request) { json, error, _ in
            DispatchQueue.main.async { [weak self] in
                if error == nil, let json = json {
                    if let response = try? JSONDecoder().decode([AppointmentsModel].self, from: json["data"]["upcomingAppointments"].rawData()) {
                        self?.upcoming = response
                        AppointmentsModel.storeInDataBase(appiontmentData: response)
                    }
                    if let response = try? JSONDecoder().decode([AppointmentsModel].self, from: json["data"]["pastAppointments"].rawData()) {
                        self?.past = response
                        AppointmentsModel.storeInDataBase(appiontmentData: response)
                    }
                    self?.formModel()
                    completion(true)
                    } else {
                        completion(false)
                    }
                }
            }
    }
    
    func fetchDoctors(completion: @escaping CompletionBlock) {
        let headers = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let path = Apiconstants.baseUrl + CareConnectURLs.getDoctors.rawValue + "?symptoms=\(selectedSymptoms)&addtionalInfo=\("")"
//        let params = ["symptoms": selectedSymptoms,
//                      "addtionalInfo": ""] as [String: Any]
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        _ = NetworkManager.request(request) { json, error, _ in
            if error == nil {
                guard let response = json?.dictionaryObject, let entries = response["data"] as? [[String: Any]] else {
                    return
                }
                for each in entries {
                    var doc = Doctor()
                    if let name = each[ApiKeys.name] as? String {
                        doc.name = name
                    }
                    if let id = each[ApiKeys.doctorId] as? Int {
                        doc.id = id
                    }
                    if let address = each[ApiKeys.address] as? String {
                        doc.address = address
                    }
                    if let role = each[ApiKeys.specialisation] as? String {
                        doc.role = role
                    }
                    if let imageUrl = each[ApiKeys.imageUrl] as? String {
                        doc.imageUrl = imageUrl
                    }
                    self.doctors.append(doc)
                }
                completion(true)
                
            } else {
                completion(false)
            }
        }
    }
    
    func fetchSlots(doctorId: Int?, completion: @escaping CompletionBlock) {
        let startDate = fromDate.timeIntervalSince1970 * 1000
        let endDate = todate.timeIntervalSince1970 * 1000
        let headers = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        guard let doctorId = doctorId else {
            return
        }
        let appendText = "?doctorId=\(doctorId)" + "&fromDate=\(Int(startDate))" + "&toDate=\(Int(endDate))"
        let path = Apiconstants.baseUrl + CareConnectURLs.getSlots.rawValue + appendText
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        _ = NetworkManager.request(request) { json, error, _ in
            
            if error == nil {
                
                guard let response = json?.dictionaryObject, let entries = response["data"] as? [[String: Any]] else {
                    return
                }
                self.allAvailabilities = []
                for each in entries {
                    if let date = each["date"] as? Double, let slots = each["slots"] as? [[String: Any]] {
                        var availability = Availability()
                        availability.date = date
                        var allSlots = [Slots]()
                        for slot in slots {
                            var cuurentSlot = Slots()
                            guard let slotId = slot["slotId"] as? Int else { return }
                            cuurentSlot.slotId = slotId
                            cuurentSlot.available = slot["available"] as? Bool ?? false
                            cuurentSlot.endDate = slot["endDate"] as? Double ?? 0
                            cuurentSlot.startDate = slot["startDate"] as? Double ?? 0
                            cuurentSlot.date = date
                            allSlots.append(cuurentSlot)
                        }
                        availability.slots = allSlots
                        self.allAvailabilities.append(availability)
                    }
                }
                completion(true)
                
            } else {
                completion(false)
            }
        }
    }
    
    func fetchSlotsForDate( date: Double) {
        let available = allAvailabilities.filter {
            let eachDate = Date.getDateFromTimeInterval($0.date ?? 0)
            let currentDate = Date.getDateFromTimeInterval(date)
            return eachDate.isSameDate(currentDate)
            }.first
        guard let selectedDate = available else { return }
        slotsForAGivenDate = selectedDate
    }
    
    func fetchFirstAvailableSlotfor(slots: [Slots], date: Date) -> Slots? {
        return slots.filter {
            let time = ($0.date ?? 0) + ($0.startDate ?? 0)
            let thisDate = Date.getDateFromTimeInterval(time)
            var check = Date().isLessThan(date)
            if Date().isLessThan(date) {
                check = date.isLessThan(thisDate)
            }
            return $0.available && check
            }.first
    }
    
    func createAppoinment(appointmentStatus: AppointmentStatusCode, patientID: Int, appointmentID: Int, completion: @escaping CompletionBlock) {
        
        let headers = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        
        var path = ""
        var patientId = patientID
        var params = [String: Any]()
        guard let slot = selectedSlot?.slotId, let reason = selectedReason else { return }
        if appointmentStatus == .create {
            path = Apiconstants.baseUrl + CareConnectURLs.createAppointment.rawValue
            patientId = selectedPatient?.id ?? 0
            params = ["slotId": slot, "patientId": patientId, "reason": reason]
        } else {
           path = Apiconstants.baseUrl + CareConnectURLs.createAppointment.rawValue + "/\(appointmentID)"
           params = ["slotId": slot, "reason": reason, "appointmentStatus": appointmentStatus.rawValue] as [String: Any]
        }
       
        guard let request = NetworkManager.requestForURL(path, method: appointmentStatus == .create ? .Post : .Put, params: params, headers: headers) else {
            return
        }
        _ = NetworkManager.request(request) { json, error, _ in
            if error == nil {
                completion(true)
                
            } else {
                completion(false)
            }
        }
    }
    
    func resetData() {
        selectedReason = ""
        selectedSymptoms = []
        selectedDoctor = nil
        selectedPatient = nil
        selectedSlot = nil
    }
}
