//
//  AvailabilityTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 31/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class AvailabilityTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.dropShadow(cornerRadius: 16)
    }
}
