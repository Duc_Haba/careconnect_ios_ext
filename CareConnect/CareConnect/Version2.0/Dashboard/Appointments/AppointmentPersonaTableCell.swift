//
//  AppointmentPersonaTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 29/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class AppointmentPersonaTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var titileLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileButton.layer.cornerRadius = profileButton.frame.width / 2
        profileButton.layer.masksToBounds = true
    }
    
}
