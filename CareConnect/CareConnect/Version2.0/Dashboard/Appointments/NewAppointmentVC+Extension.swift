//
//  NewAppointmentVC+Extention.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 30/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import SDWebImage

extension NewAppointmentVC: UITableViewDelegate, UITableViewDataSource {
        
    func numberOfSections(in tableView: UITableView) -> Int {
        if appointmentsVM.progressCount == AppointmentsVM.VCType.review.rawValue {
            return 2
        } else if appointmentsVM.progressCount == AppointmentsVM.VCType.symptom.rawValue {
            return 2
        } else if appointmentsVM.progressCount == AppointmentsVM.VCType.availability.rawValue {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        switch appointmentsVM.progressCount {
        case AppointmentsVM.VCType.patient.rawValue:
            count = appointmentsVM.allPatients?.count ?? 0
        case AppointmentsVM.VCType.reason.rawValue:
            count = appointmentsVM.allReasons.count
        case AppointmentsVM.VCType.availability.rawValue:
            count = section == 0 ? 1 : appointmentsVM.showSlots ? 2 : 1
        case AppointmentsVM.VCType.review.rawValue:
            count = section == 0 ? 1 : 4
        case AppointmentsVM.VCType.symptom.rawValue:
            count = 1
            if section == 1 {
                if shouldShowResults {
                    count = appointmentsVM.symptomResults.count
                } else {
                    count = 1
                }
            }
        case AppointmentsVM.VCType.doctor.rawValue:
            count = appointmentsVM.doctors.count
        default:
            count = 3
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        switch appointmentsVM.progressCount {
        case AppointmentsVM.VCType.patient.rawValue:
            cell = createPatientCell(indexPath: indexPath)
        case AppointmentsVM.VCType.reason.rawValue:
            cell = createReasonCell(indexPath: indexPath)
        case AppointmentsVM.VCType.availability.rawValue:
            if indexPath.section == 0 {
                cell = createAvailabilityCell(indexPath: indexPath)
            } else {
                if indexPath.row == 0 {
                    cell = createCalendarCell(indexPath: indexPath)
                } else {
                    cell = createSlotsCell(indexPath: indexPath)
                }
            }
        case AppointmentsVM.VCType.doctor.rawValue:
            cell = createPatientCell(indexPath: indexPath)
        case AppointmentsVM.VCType.review.rawValue:
            if indexPath.section == 0 {
                cell = createPersonaCell(indexPath: indexPath)
            } else {
                cell = createDetailCell(indexPath: indexPath)
            }
        case AppointmentsVM.VCType.symptom.rawValue:
            if indexPath.section == 0 {
                cell = createSearchCell(indexPath: indexPath)
            } else {
                if shouldShowResults {
                    cell = createResultCell(indexPath: indexPath)
                } else {
                    cell = createSymptomCell(indexPath: indexPath)
                }
            }
        default:
            cell = createPatientCell(indexPath: indexPath)
        }
        return cell ?? UITableViewCell()
    }
    
    private func createSlotsCell(indexPath: IndexPath) -> SlotsTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SlotsTableCell.className, for: indexPath) as? SlotsTableCell else { return nil }
        cell.delegate = self
        cell.availability = appointmentsVM.slotsForAGivenDate
        return cell
    }
    
    private func createAvailabilityCell(indexPath: IndexPath) -> AvailabilityTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AvailabilityTableCell.className, for: indexPath) as? AvailabilityTableCell else { return nil }
        appointmentsVM.fetchSlotsForDate(date: selectedDate.timeIntervalSince1970 * 1000)
        var dateString = "No slots available for " + Date.getDateStringFromTimeStamp(selectedDate.timeIntervalSince1970 * 1000, format: prescriptionDateFormat)
        if let day = appointmentsVM.slotsForAGivenDate.date, let slots = appointmentsVM.slotsForAGivenDate.slots, let firstSlot = appointmentsVM.fetchFirstAvailableSlotfor(slots: slots, date: selectedDate), let time = firstSlot.startDate {
            appointmentsVM.firstAvailableSlot = firstSlot
            dateString = Date.getDateStringFromTimeStamp(day + time, format: prescriptionDateFormat)

        }
        cell.valueLabel.text = dateString
        return cell
    }
    
    private func createCalendarCell(indexPath: IndexPath) -> CalendarTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CalendarTableCell.className, for: indexPath) as? CalendarTableCell else { return nil }
        cell.delegate = self
        cell.appointmentsVM = appointmentsVM
        cell.selectedDate = selectedDate
        return cell
    }
    
    private func createSymptomCell(indexPath: IndexPath) -> SymptomsTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SymptomsTableCell.className, for: indexPath) as? SymptomsTableCell else { return nil }
        cell.tagView.delegate = self
        cell.tagView.dataSource = self
        cell.tagView.numberOfTagsToShow = appointmentsVM.selectedSymptoms.count
        cell.tagView.isOpaqueState = false
        cell.tagView.setuptags()
        return cell
    }
    
    private func createDetailCell(indexPath: IndexPath) -> AppointmentDetailsTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AppointmentDetailsTableCell.className, for: indexPath) as? AppointmentDetailsTableCell else { return nil }
        cell.hideAddButton = true
        cell.switchButton.isHidden = indexPath.row != 3
        
        let details = formDetailModel()
        cell.acessoryButton.setImage(details[indexPath.row].image, for: .normal)
        cell.titleLabel.text = details[indexPath.row].title
        cell.nameLabel.text = details[indexPath.row].value
        cell.bottomBar.isHidden = false
        return cell
    }
    
    private func createResultCell(indexPath: IndexPath) -> SearchResultTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultTableCell.className, for: indexPath) as? SearchResultTableCell else { return nil }
        cell.titileLabel.text = appointmentsVM.symptomResults[indexPath.row]
        return cell
    }
    
    private func createSearchCell(indexPath: IndexPath) -> SymptomSearchTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SymptomSearchTableCell.className, for: indexPath) as? SymptomSearchTableCell else { return nil }
        cell.searchTextField.delegate = self
        return cell
    }
    
    private func formDetailModel() -> [DetailModel] {
        let totalDate = (appointmentsVM.selectedSlot?.startDate ?? 0.0) + (appointmentsVM.selectedSlot?.date ?? 0.0)
        let date = Date.getDateStringFromTimeStamp(totalDate, format: slotDateFormat)
        var detail = [DetailModel(title: "Date", image: nil, value: date)]
        detail.append(DetailModel(title: "Location", image: nil, value: appointmentStatus == .create ?  appointmentsVM.selectedDoctor?.address : selectedAppointment?.address))
        detail.append(DetailModel(title: "Reason", image: nil, value: appointmentsVM.selectedReason))
        detail.append(DetailModel(title: "", image: nil, value: "Send my medical records to \(appointmentStatus == .create ?  appointmentsVM.selectedDoctor?.name ?? "" : selectedAppointment?.name ?? "")"))
        return detail
    }
    
    private func createPersonaCell(indexPath: IndexPath) -> AppointmentPersonaTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AppointmentPersonaTableCell.className, for: indexPath) as? AppointmentPersonaTableCell else { return nil }
        cell.titileLabel.text = "Appointment With"
        SDWebImageManager.shared.loadImage(with: URL(string: appointmentStatus == .create ?  appointmentsVM.selectedDoctor?.imageUrl ?? "" : selectedAppointment?.imageUrl ?? "") as URL?, options: .continueInBackground, progress: { (recieved, expected, nil) in
            print(recieved, expected)
        }, completed: { (downloadedImage, data, error, SDImageCacheType, true, imageUrlString) in
            DispatchQueue.main.async {
                if downloadedImage != nil {
                    cell.profileButton.setImage(downloadedImage, for: .normal)
                }
            }
        })
        cell.nameLabel.text = appointmentStatus == .create ?  appointmentsVM.selectedDoctor?.name ?? "" : selectedAppointment?.name ?? ""
        
        return cell
    }
    
    private func createPatientCell(indexPath: IndexPath) -> PatientTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PatientTableCell.className, for: indexPath) as? PatientTableCell else { return nil }
        if appointmentsVM.progressCount == AppointmentsVM.VCType.doctor.rawValue {
            cell.nameLabel.text = appointmentsVM.doctors[indexPath.row].name
            cell.relationLabel.text = appointmentsVM.doctors[indexPath.row].role
            cell.addressLabel.text = appointmentsVM.doctors[indexPath.row].address
            cell.profileImageView.sd_setImage(with: URL(string: appointmentsVM.doctors[indexPath.row].imageUrl ?? ""), placeholderImage: UIImage(named: "Icon-Placeholder"))
        } else if appointmentsVM.progressCount == AppointmentsVM.VCType.patient.rawValue {
            cell.nameLabel.text = appointmentsVM.allPatients?[indexPath.row].name
            let relation = appointmentsVM.allPatients?[indexPath.row].relation
            cell.relationLabel.text = relation == "self" ? "Me" : relation?.capitalized
            cell.addressLabel.text = ""
            cell.profileImageView.sd_setImage(with: URL(string: appointmentsVM.allPatients?[indexPath.row].imageUrl ?? ""), placeholderImage: UIImage(named: "Icon-Placeholder"))
        }
        return cell
    }
    
    private func createReasonCell(indexPath: IndexPath) -> ReasonsTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReasonsTableCell.className, for: indexPath) as? ReasonsTableCell else { return nil }
        cell.titleLabel.text = appointmentsVM.allReasons[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: HomeTableSectionHeader.className) as? HomeTableSectionHeader
        let title = appointmentsVM.sectionTitles[(appointmentsVM.progressCount) - 1]
        header?.titleLabel.text = title
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard appointmentsVM.progressCount != 6 else { return 0 }
        if appointmentsVM.progressCount == AppointmentsVM.VCType.symptom.rawValue {
            if section != 0 {
                return 0
            }
        } else if appointmentsVM.progressCount == AppointmentsVM.VCType.availability.rawValue {
            if section != 0 {
                return 0
            }
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard appointmentsVM.progressCount != AppointmentsVM.VCType.review.rawValue else { return }
        if appointmentsVM.progressCount == AppointmentsVM.VCType.symptom.rawValue && shouldShowResults {
            if indexPath.section == 1 {
                shouldShowResults = false
                appointmentsVM.selectedSymptoms.append(appointmentsVM.symptomResults[indexPath.row])
                tableView.reloadData()
            } else {
                return
            }
        } else {
            switch appointmentsVM.progressCount {
            case AppointmentsVM.VCType.patient.rawValue:
                appointmentsVM.selectedPatient = appointmentsVM.allPatients?[indexPath.row]
            case AppointmentsVM.VCType.reason.rawValue:
                appointmentsVM.selectedReason = appointmentsVM.allReasons[indexPath.row]
            case AppointmentsVM.VCType.doctor.rawValue:
                appointmentsVM.selectedDoctor = appointmentsVM.doctors[indexPath.row]
            case AppointmentsVM.VCType.availability.rawValue:
                if indexPath.section == 0 {
                    appointmentsVM.selectedSlot = appointmentsVM.firstAvailableSlot
                } else {
                    appointmentsVM.selectedSlot = appointmentsVM.slotsForAGivenDate.slots?[indexPath.row]
                }

            default:
                break
            }
            if appointmentsVM.progressCount == AppointmentsVM.VCType.availability.rawValue {
                guard indexPath.section == 0 && appointmentsVM.selectedSlot?.available ?? false  else { return }
                self.pushSelfController()
            } else {
                self.pushSelfController()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
}

extension NewAppointmentVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        shouldShowResults = true
        appointmentsVM.symptomResults = []
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = string
        appointmentsVM.symptomResults = []
        appointmentsVM.symptomResults = appointmentsVM.symptoms.all(where: { $0.contains(text) })
        tableView.reloadSections([1], with: .none)
        return true
    }
}

extension NewAppointmentVC: CalendarDateSelectedDelegate {
    func didSelectNextMonth() {
        let date = appointmentsVM.todate.nextDay ?? Date()
        appointmentsVM.fromDate = date
        appointmentsVM.todate = date.endOfMonth()
        selectedDate = appointmentsVM.fromDate
        fetchSlots()
    }
    
    func didSelectPrevMonth() {
        let date = appointmentsVM.fromDate.previousDay ?? Date()
        appointmentsVM.fromDate = date.startOfMonth()
        appointmentsVM.todate = date
        selectedDate = appointmentsVM.todate
        fetchSlots()
    }
    
    func didSelectDate(date: Date) {
        appointmentsVM.showSlots = true
        selectedDate = date
        appointmentsVM.fetchSlotsForDate(date: date.timeIntervalSince1970 * 1000)
        tableView.reloadData()
    }
}

extension NewAppointmentVC: CalendarSlotSelectedDelegate {
    func didSelectSlot(index: IndexPath) {
        appointmentsVM.selectedSlot = appointmentsVM.slotsForAGivenDate.slots?[index.row]
        guard appointmentsVM.selectedSlot?.available ?? false else { return }
        pushSelfController()
    }
    
}

extension Array where Element: Equatable {
    func all(where predicate: (Element) -> Bool) -> [Element] {
        return self.compactMap { predicate($0) ? $0 : nil }
    }
}

extension NewAppointmentVC: TagViewDelegate, TagViewDataSource {
    func tagView(_ tagView: TagView, didRemoveTagAt index: Int) {
        appointmentsVM.selectedSymptoms.remove(at: index)
    }
    
    func tagView(_ tagView: TagView, didUpdateTagHeight height: CGFloat) {
        let cell = tableView.cellForRow(at: IndexPath(item: 1, section: 1)) as? SymptomsTableCell
        cell?.tagViewHeightConstraint.constant = height
//        tableView.reloadSections([1], with: .none)
    }
    
    func tagView(_ tagView: TagView, titleTextAt index: Int) -> String {
        return appointmentsVM.selectedSymptoms[index]
    }
}
