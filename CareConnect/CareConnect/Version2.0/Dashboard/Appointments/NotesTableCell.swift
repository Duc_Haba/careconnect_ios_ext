//
//  NotesTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 30/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class NotesTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var titileLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
