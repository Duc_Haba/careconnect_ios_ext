//
//  SymptomCollectionCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 31/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SymptomCollectionCell: UICollectionViewCell, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var accessoryButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.setGradientBackground(cornerRadius: 20)
    }

}
