//
//  AppointmentDetailVC.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 29/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import SDWebImage
import MapKit
import EventKit

struct DetailModel {
    var title: String?
    var image: UIImage?
    var value: String?
}

typealias RefreshBlock = () -> Void

class AppointmentDetailVC: UIViewController {
    
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var rescheduleButton: UIButton!
    
    var selectedAppointment: AppointmentsModel?
    var selectedAppointmentType: AppointmentType?
    var detailModel: [DetailModel]?
    private var userData = Dependents()
    private var appointmentVM = AppointmentsVM()
    var selectedDate: Double?
    var refreshBlock: RefreshBlock?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        cancelButton.dropShadow(cornerRadius: 28)
        rescheduleButton.dropShadow(cornerRadius: 28)
        rescheduleButton.setGradientBackground(cornerRadius: rescheduleButton.frame.height / 2)
        tableView.register(AppointmentPersonaTableCell.self)
        tableView.register(AppointmentDetailsTableCell.self)
        tableView.register(NotesTableCell.self)
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        selectedDate = (selectedAppointment?.date ?? 0) + (selectedAppointment?.startTime ?? 0)
        setDetailModel()
        fetchUserData()
        cancelButton.isHidden = selectedAppointmentType == .past
        rescheduleButton.isHidden = selectedAppointmentType == .past
        loaderView.isHidden = true
        
    }
    
    func fetchUserData() {
        guard let patientID = selectedAppointment?.patientId else { return }
        let realmUserData = RealmManager.shared.getObjects(type: Dependents.self, with: patientID)
        userData = realmUserData ?? Dependents()
        appointmentVM.selectedReason = selectedAppointment?.reason ?? ""
        appointmentVM.selectedSlot?.slotId = selectedAppointment?.slotId ?? 0
    }
    
    private func setDetailModel() {
        let date = Date.getDateStringFromTimeStamp(selectedDate ?? 0, format: slotDateFormat)
        
        detailModel = [DetailModel(title: "Date", image: #imageLiteral(resourceName: "Icon-Calendar-appointment"), value: date)]
        detailModel?.append(DetailModel(title: "Location", image: #imageLiteral(resourceName: "Icon-Location"), value: selectedAppointment?.address))
        detailModel?.append(DetailModel(title: "Reason", image: #imageLiteral(resourceName: "Icon-Location"), value: selectedAppointment?.reason))
        if selectedAppointmentType == .past {
            if let notes = selectedAppointment?.notes {
                detailModel?.append(DetailModel(title: "Notes", image: #imageLiteral(resourceName: "Icon-Location"), value: notes))
            }
        } else {
            detailModel?.append(DetailModel(title: "Reminder", image: #imageLiteral(resourceName: "Icon-Location"), value: "Email"))
        }
    }
    
    @IBAction private func didPressBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func didPressReschedule(_ sender: Any) {
        
        presentNewAppointmentVC()
    }
    
    func presentNewAppointmentVC() {
        let appointmentVC = NewAppointmentVC(nibName: NewAppointmentVC.className, bundle: nil)
        appointmentVC.progressCount = AppointmentsVM.VCType.availability.rawValue
        appointmentVC.appointmentStatus = .reschedule
        appointmentVC.selectedAppointment = selectedAppointment
        appointmentVC.dismissBlock = { [weak self] date in
            self?.selectedDate = date
            DispatchQueue.main.async {
                self?.setDetailModel()
                self?.tableView.reloadData()
            }
        }
        
        let newAppointmentNC = UINavigationController(rootViewController: appointmentVC)
        newAppointmentNC.navigationBar.isHidden = true
        newAppointmentNC.modalPresentationStyle = .currentContext
        self.navigationController?.present(newAppointmentNC, animated: true, completion: nil)
    }
    
    @IBAction private func didPressCancel(_ sender: Any) {
        
        guard let doctorName = selectedAppointment?.name else {
            return
        }
        let date = Date.getDateStringFromTimeStamp(selectedDate ?? 0, format: slotDateFormat)
        let customAlert = AlertViewController(nibName: AlertViewController.className, bundle: nil)
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.title = "Cancel Confirmation"
        customAlert.message = "Are you sure that you want to cancel your appoinment with \(doctorName) on \(date)? Or would you like to reschedule to a different date and time."
        self.present(customAlert, animated: true, completion: nil)
    }
}

extension AppointmentDetailVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if section == 0 {
            count = 2
        } else {
            count = detailModel?.count ?? 0
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        if indexPath.section == 0 {
            cell = createPersonaCell(indexPath: indexPath)
        } else {
            if selectedAppointmentType == .past && indexPath.row == (detailModel?.count ?? 0) - 1 {
                cell = createNoteCell(indexPath: indexPath)
                
            } else {
                cell = createDetailCell(indexPath: indexPath)
            }
        }
        return cell ?? UITableViewCell()
    }
    
    private func createNoteCell(indexPath: IndexPath) -> NotesTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NotesTableCell.className, for: indexPath) as? NotesTableCell else { return nil }
        cell.valueLabel.text = selectedAppointment?.notes
        return cell
    }
    
    private func createPersonaCell(indexPath: IndexPath) -> AppointmentPersonaTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AppointmentPersonaTableCell.className, for: indexPath) as? AppointmentPersonaTableCell else { return nil }
        var title = "With"
        var name = selectedAppointment?.name
        var url = selectedAppointment?.imageUrl
        if indexPath.row == 0 {
            title = "Appointment for"
            name = userData.name ?? ""
            url = userData.imageUrl ?? ""
        }
        cell.profileButton.imageView?.contentMode = .scaleAspectFill
        SDWebImageManager.shared.loadImage(with: URL(string: url ?? "") as URL?, options: .continueInBackground, progress: { (recieved, expected, nil) in
            print(recieved, expected)
        }, completed: { (downloadedImage, data, error, SDImageCacheType, true, imageUrlString) in
            DispatchQueue.main.async {
                if downloadedImage != nil {
                    cell.profileButton.setImage(downloadedImage, for: .normal) 
                }
            }
        })
        cell.titileLabel.text = title
        cell.nameLabel.text = name
        return cell
    }
    
    private func createDetailCell(indexPath: IndexPath) -> AppointmentDetailsTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AppointmentDetailsTableCell.className, for: indexPath) as? AppointmentDetailsTableCell else { return nil }
        cell.hideAddButton = true
        if indexPath.row == 0 && selectedAppointmentType != .past {
            cell.hideAddButton = false
        }
        cell.acessoryButton.isHidden = indexPath.row >= 2
        let details = detailModel?[indexPath.row]
        cell.acessoryButton.tag = indexPath.row
        cell.acessoryButton.setImage(details?.image, for: .normal)
        cell.titleLabel.text = details?.title
        cell.nameLabel.text = details?.value
        cell.bottomBar.isHidden = true
        cell.switchButton.isHidden = true
        cell.acessoryButton.addTarget(self, action: #selector(accessoryButtonClicked(_:)), for: .touchUpInside)
        cell.addButton.addTarget(self, action: #selector(addButtonClicked(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func accessoryButtonClicked(_ sender: UIButton) {
        let details = detailModel?[sender.tag]
        if details?.title == "Location" {
            openMaps()
        }
    }
    
    @objc func addButtonClicked(_ sender: UIButton) {
        addToCalendar { isAdded, error in
            if isAdded {
                /// Show popup
                print("Added")
            }
        }
    }
    
    private func openMaps() {
        let latitude: CLLocationDegrees = selectedAppointment?.latitude ?? 0.0
        let longitude: CLLocationDegrees = selectedAppointment?.longitude ?? 0.0
        
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: nil))
        if let name = selectedAppointment?.name {
            mapItem.name = "Appointment With " + name
        }
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
    
    private func addToCalendar(completion: ((_ success: Bool, _ error: Error?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { [weak self] granted, error in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                if let name = self?.selectedAppointment?.name {
                    event.title = "Appointment With " + name
                }
                event.startDate = Date.getDateFromTimeInterval(self?.selectedAppointment?.date ?? 0)
                event.endDate = Date.getDateFromTimeInterval(self?.selectedAppointment?.date ?? 0)
                event.notes = event.title
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as Error?)
            }
        })
    }
}

extension AppointmentDetailVC: CustomAlertViewDelegate {
    
    func rescheduleButtonTapped() {
        presentNewAppointmentVC()
    }
    
    func cancelButtonTapped() {
        loaderView.isHidden = false
        appointmentVM.createAppoinment(appointmentStatus: .cancel, patientID: 0, appointmentID: selectedAppointment?.appointmentId ?? 0) {[weak self] isSuccess in
            DispatchQueue.main.async {
                self?.loaderView.isHidden = true
                if isSuccess {
                    Defaults.shared.shouldUpdateDashboard = true
                    self?.refreshBlock?()
                    self?.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
}
