//
//  SymptomSearchTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 30/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SymptomSearchTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var searchTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
