//
//  ListAppointmentsVC.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 29/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class ListAppointmentsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderStackView: UIView!

    var appointmentsVM = AppointmentsVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(AppointmentTableCell.self)
        tableView.registerReusableHeaderFooterView(HomeTableSectionHeader.self)
        fetchAllApointments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchAllApointments()
    }
    
    private func fetchAllApointments() {
        loaderStackView.isHidden = false
        appointmentsVM.fetchAppointments {[weak self] isSuccess in
            DispatchQueue.main.async {
                self?.loaderStackView.isHidden = true
                if isSuccess {
                    self?.tableView.reloadData()
                }
            }
        }
    }

    @IBAction private func didPressAdd(_ sender: Any) {
        let newAppointmentVC = NewAppointmentVC(nibName: NewAppointmentVC.className, bundle: nil)
        newAppointmentVC.createAppointmentDelegate = self
        let newAppointmentNC = UINavigationController(rootViewController: newAppointmentVC)
        newAppointmentNC.navigationBar.isHidden = true
        navigationController?.present(newAppointmentNC, animated: true)
    }
    
}

extension ListAppointmentsVC: CreateAppointmentDelegate {
    func didCreateAppointment(date: Double?) {
        fetchAllApointments()
    }
}

extension ListAppointmentsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return appointmentsVM.appoinmentsModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointmentsVM.appoinmentsModel[section].numberOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        cell = createAppointmentCell(indexPath: indexPath)
        return cell ?? UITableViewCell()
    }
    
    private func createAppointmentCell(indexPath: IndexPath) -> AppointmentTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AppointmentTableCell.className, for: indexPath) as? AppointmentTableCell else { return nil }
        cell.containerTopConstraint.constant = 12
        cell.containerBottomConstraint.constant = 12
        var date = "--"
        var title = "--"
        var subtitle = "--"
        var image = #imageLiteral(resourceName: "Icon-Document")
        let model = appointmentsVM.appoinmentsModel[indexPath.section].appointments?[indexPath.row]

        if appointmentsVM.appoinmentsModel[indexPath.section].type == AppointmentType.upcomming {
            image = #imageLiteral(resourceName: "Icon-Map")
        } else {
            image = #imageLiteral(resourceName: "Icon-Document")
        }
        date = Date.getDateStringFromTimeStamp(model?.date ?? 0.0, format: prescriptionDateFormat)
        if let name = model?.name {
            title = "Checkup - " + name
        }
        subtitle = model?.address ?? ""

        cell.dateLabel.text = date
        cell.nameLabel.text = title
        cell.addressLabel.text = subtitle
        cell.mapButton.setImage(image, for: .normal)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: HomeTableSectionHeader.className) as? HomeTableSectionHeader
        var title = AppointmentType.past.rawValue
        if appointmentsVM.appoinmentsModel[section].type == AppointmentType.upcomming {
            title = AppointmentType.upcomming.rawValue
        }
        header?.titleLabel.text = title
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var height: CGFloat = 0
        if appointmentsVM.appoinmentsModel[section].numberOfRows ?? 0 > 0 {
            height = 60
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let appointmentDetailVC = AppointmentDetailVC(nibName: AppointmentDetailVC.className, bundle: nil)
        appointmentDetailVC.selectedAppointment = appointmentsVM.appoinmentsModel[indexPath.section].appointments?[indexPath.row]
        appointmentDetailVC.selectedAppointmentType = appointmentsVM.appoinmentsModel[indexPath.section].type
        navigationController?.pushViewController(appointmentDetailVC, animated: true)
    }
}
