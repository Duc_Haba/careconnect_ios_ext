//
//  PatientTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 30/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class PatientTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var relationLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
