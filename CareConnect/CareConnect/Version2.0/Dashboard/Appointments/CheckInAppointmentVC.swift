//
//  CheckInAppointmentVC.swift
//  CareConnect
//
//  Created by Venugopal S A on 23/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class CheckInAppointmentVC: UIViewController {

    enum AppointmentInfo: Int, CaseIterable {
        case appointmentWith
        case date
        case reason
        case copay
        
        var title: String {
            switch self {
          
            case .appointmentWith:
                return "Appointment with"
            case .date:
                return "Date"
            case .reason:
                return "Reason"
            case .copay:
                return "Copay"
            }
        }
    }
    
    @IBOutlet weak var checkInButton: UIButton!
    @IBOutlet weak var appointmentInfoTableView: UITableView!
    @IBOutlet weak var hospitalAddress: UILabel!
    @IBOutlet weak var hospitalName: UILabel!
    
    var selectedAppointment: AppointmentsModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        appointmentInfoTableView.register(AppointmentPersonaTableCell.self)
        appointmentInfoTableView.register(AppointmentDetailsTableCell.self)
        hospitalAddress.text = selectedAppointment?.address ?? ""
    }
    
    @IBAction private func crossButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func checkInTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension CheckInAppointmentVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return AppointmentInfo.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        guard let cellInfo = AppointmentInfo(rawValue: indexPath.row) else {
            return UITableViewCell()
        }
        
        guard let cell = appointmentInfoTableView.dequeueReusableCell(withIdentifier: AppointmentDetailsTableCell.className, for: indexPath) as? AppointmentDetailsTableCell else { return UITableViewCell() }
        cell.addButton.isHidden = true
        cell.switchButton.isHidden = true
        cell.acessoryButton.isHidden = true
        
        switch cellInfo {
        
        case .appointmentWith:
            guard let cell = appointmentInfoTableView.dequeueReusableCell(withIdentifier: AppointmentPersonaTableCell.className, for: indexPath) as? AppointmentPersonaTableCell else { return UITableViewCell() }
            cell.backgroundColor = .clear
            cell.titileLabel.text = cellInfo.title
            cell.nameLabel.text = selectedAppointment?.name
            return cell
        case .date:
            cell.nameLabel.text = Date.getDateStringFromTimeStamp(selectedAppointment?.date ?? Date().timeIntervalSince1970, format: slotDateFormat)
            cell.titleLabel.text = cellInfo.title
        case .reason:
            cell.nameLabel.text = selectedAppointment?.reason
            cell.titleLabel.text = cellInfo.title
        case .copay:
            cell.nameLabel.text = "$30"
            cell.titleLabel.text = cellInfo.title
        }
        cell.backgroundColor = .clear
        cell.backgroundColor = .clear
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}
