//
//  SlotsCollectionCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 31/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SlotsCollectionCell: UICollectionViewCell, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titileLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.setGradientBackground(cornerRadius: 12)
    }

}
