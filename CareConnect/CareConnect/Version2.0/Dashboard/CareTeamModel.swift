//
//  CareTeamModel.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 16/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class CareTeamModel: Object, Codable {
    dynamic var doctorName: String?
    dynamic var doctorId: String?
    dynamic var imageUrl: String?
    dynamic var specialization: String?
    
    override static func primaryKey() -> String? {
        return "doctorId"
    }
    
    private enum CodingKeys: String, CodingKey {
        case doctorName
        case doctorId
        case imageUrl
        case specialization
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        doctorName = try container.decodeIfPresent(String.self, forKey: .doctorName)
        doctorId = try container.decodeIfPresent(String.self, forKey: .doctorId)
        specialization = try container.decodeIfPresent(String.self, forKey: .specialization)
        imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl)
    }
}

extension CareTeamModel {
    func encode(to encoder: Encoder) throws {
    }
}

extension CareTeamModel {
    class func storeInDataBase(teamData: [CareTeamModel]) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(teamData, update: .all)
        }
    }
    
    func storeTeamDataToDB() {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(self, update: .all)
        }
    }
}
