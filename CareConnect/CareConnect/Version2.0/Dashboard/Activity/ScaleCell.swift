//
//  ScaleCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 13/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

protocol ScaleDelegate: class {
    func didChangeScaleValue(value: Int)
}

class ScaleCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var rightValue: UILabel!
    @IBOutlet weak var leftValue: UILabel!
    @IBOutlet weak var lbsLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var kgsLabel: UILabel!
    
    var maxScaleRange = 120.0
    weak var scaleDelegate: ScaleDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lbsLabel.text = "130"
        kgsLabel.text = "59"
        layoutRuler()
    }

    fileprivate func layoutRuler() {
        guard let text = kgsLabel.text, let scale = Int(text) else {
            return
        }
        
        DTRuler.theme = Colorful()
        
        let ruler = DTRuler(scale: .integer(scale), minScale: .integer(0), maxScale: .integer(Int(maxScaleRange)), width: containerView.bounds.width)
        ruler.delegate = self
        ruler.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        ruler.transform = CGAffineTransform(rotationAngle: CGFloat( Double.pi))
        containerView.addSubview(ruler)
        
        ruler.translatesAutoresizingMaskIntoConstraints = false
        
        let bottom = NSLayoutConstraint(item: ruler, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: ruler, attribute: .leading, relatedBy: .equal, toItem: containerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: ruler, attribute: .trailing, relatedBy: .equal, toItem: containerView, attribute: .trailing, multiplier: 1, constant: 0)
        
        containerView.addConstraints([bottom, leading, trailing])
    }
    
    struct Colorful: DTRulerTheme {
        
        var backgroundColor: UIColor {
            return .clear
        }
        
        var pointerColor: UIColor {
            return Color.appPrimaryColor.uiColor
        }
        
        var majorScaleColor: UIColor {
            return Color.appPrimaryColor.uiColor
        }
        
        var minorScaleColor: UIColor {
            return Color.scaleMinorBarsColor.uiColor
        }
        
        var labelColor: UIColor {
            return Color.scaleMinorBarsColor.uiColor
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension ScaleCell: DTRulerDelegate {
    
    func didChange(on ruler: DTRuler, withScale scale: DTRuler.Scale) {
        guard let kgs = Double(scale.majorTextRepresentation())  else {
            return
        }
        let scaleWidth = Double(UIScreen.main.bounds.width / 100).rounded(.toNearestOrAwayFromZero)
        rightValue.text = "\(Int(Double(maxScaleRange * 2.20462) - Double(kgs * 2.20462)) + Int(scaleWidth / 2 * 10))"
        let leftLabelValue = Int(Double(maxScaleRange * 2.20462) - Double(kgs * 2.20462)) - Int(scaleWidth / 2 * 10)
        leftValue.text = "\(leftLabelValue > 0 ? leftLabelValue : 0)"
        kgsLabel.text = "\(maxScaleRange - kgs)" + " kgs"
        let lbs = Int(Double(maxScaleRange * 2.20462) - Double(kgs * 2.20462))
        lbsLabel.text = "\(lbs)" + " lbs"
        scaleDelegate?.didChangeScaleValue(value: lbs)
    }
    
}
