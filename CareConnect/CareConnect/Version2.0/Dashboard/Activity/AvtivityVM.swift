//
//  AvtivityVM.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 05/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import Rudder
import RealmSwift

class ActivityVM {
    
    struct CareModel {
        var title: String = ""
        var numberOfRows: Int = 0
        var details: [CellDetails] = []
    }
    
    struct CellDetails {
        var title: String = ""
        var subtitle: String = ""
        var image: UIImage?
        var isCompleted: Bool = false
    }
    
    struct CareCardModel {
        var title: String = ""
        var duration: String = ""
        var model: [CareModel] = []
    }
    
    var careCardModel = CareCardModel()
    var surveyCountCompleted: Int = 0
    var totalCount: Int = 0
    var selectedSurvey: SurveysListModel?
    var selectedCareCard: ActivityModel?
    var selectedDate = Date()
    var completedDays = [Date]()
    var didReadDocument: Bool = false

    func fetchCompletedDates() -> [Date] {
        var dates = [Date]()
        let from = Date.getDateFromTimeInterval(selectedCareCard?.startDate ?? 0)
        let to = Date.getDateFromTimeInterval(selectedCareCard?.targetBy ?? 0)
        let datesRange = Date.dates(from: from, to: to)
        
        guard let selectedCard = selectedCareCard else { return [] }
        /// + 1 is for document count.
        let count = selectedCard.activitiesList.count + selectedCard.surveysList.count + 1
        totalCount = count * datesRange.count
        surveyCountCompleted = 0
        var isCompleted = true
        for each in datesRange {
            isCompleted = true
            for activity in selectedCard.activitiesList {
                let data = activity.valuesList.filter {
                  let date = Date.getDateFromTimeInterval($0.createdOn)
                    return date.isSameDate(each)
                }
                isCompleted = !data.isEmpty
                surveyCountCompleted += isCompleted ? 1 : 0
                if !isCompleted { break }
            }
            for survey in selectedCard.surveysList {
                let data = survey.valuesList.filter {
                    let date = Date.getDateFromTimeInterval($0.createdOn)
                    return date.isSameDate(each)
                }
                surveyCountCompleted += isCompleted ? 1 : 0
                isCompleted = !data.isEmpty
                if !isCompleted { break }
            }
            if isCompleted {
                dates.append(each)
            }
        }
        /// This addition is for reading the document for entire care card and not on daily basis
        surveyCountCompleted += didReadDocument ? 1 : 0
        return dates
    }
    
    func setUpModel() {
        careCardModel = CareCardModel()
        let date = Date.getDateStringFromTimeStamp(selectedCareCard?.targetBy ?? 0, format: slotDateFormat)
        guard let selectedCard = selectedCareCard else { return }
        let model = createModel(with: selectedCard)
        careCardModel = CareCardModel(title: selectedCareCard?.name ?? "", duration: "by \(date)", model: model)
        completedDays = fetchCompletedDates()
    }
    
    func postSurvey(id: Int, value: String, callback: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        let header = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let path = Apiconstants.baseUrl + "survey/\(id)"
        let params = ["value": value] as [String: Any]
        guard let request = NetworkManager.requestForURL(path, method: .Put, params: params, headers: header, encoding: .Raw) else {
            return
        }
        _ = NetworkManager.request(request) { response, error, _ in
            if error == nil {
                DispatchQueue.main.async {
                    let survey = self.selectedCareCard?.surveysList.filter {
                        $0.id == id
                        }.first
                    let valueModel = ActivityValuesModel()
                    valueModel.isCompleted = true
                    valueModel.createdOn = Date().timeIntervalSince1970 * 1000
                    valueModel.value = value
                    survey?.addValueList(value: valueModel)
                }
                callback(true, nil)
            } else {
                callback(false, error)
            }
        }
    }
    
    func postActivity(id: Int, value: String, callback: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        let header = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let path = Apiconstants.baseUrl + "activity/\(id)"
        
        guard let request = NetworkManager.requestForURL(path, method: .Put, params: [:], headers: header) else {
            return
        }
        _ = NetworkManager.request(request) { response, error, _ in
            if error == nil {
                DispatchQueue.main.async {
                    let activity = self.selectedCareCard?.activitiesList.filter {
                        $0.id == id
                        }.first
                    let value = ActivityValuesModel()
                    value.isCompleted = true
                    value.createdOn = Date().timeIntervalSince1970 * 1000
                    value.value = nil
                    activity?.addValueList(value: value)
                }
                callback(true, nil)
            } else {
                callback(false, error)
            }
        }
    }
    
    func createModel(with careCard: ActivityModel) -> [CareModel] {
        var model = [CareModel]()
        var cellDetails = [CellDetails]()
        for each in careCard.activitiesList {
            var isCompleted = false
            let data = each.valuesList.filter {
                let date = Date.getDateFromTimeInterval($0.createdOn)
                return date.isSameDate(self.selectedDate)
            }
            isCompleted = !data.isEmpty
            let detailModel = CellDetails(title: each.name ?? "", subtitle: each.duration ?? "", image: #imageLiteral(resourceName: "icnAppoinment"), isCompleted: isCompleted)
            cellDetails.append(detailModel)
        }
        model = [CareModel(title: careCard.activitySectionName ?? "", numberOfRows: cellDetails.count, details: cellDetails)]

        /// Mock data for this section
        cellDetails = [CellDetails(title: "Pre-Op Clearance", subtitle: "5 min read", image: #imageLiteral(resourceName: "Icon-Document"), isCompleted: didReadDocument)]
        model.append(CareModel(title: "Care Instructions", numberOfRows: cellDetails.count, details: cellDetails))
        
        cellDetails = []
        for each in careCard.surveysList {
            var isCompleted = false
            let data = each.valuesList.filter {
                let date = Date.getDateFromTimeInterval($0.createdOn)
                return date.isSameDate(self.selectedDate)
            }
            isCompleted = !data.isEmpty
            let valueMod = each.valuesList.filter { activityValueModel -> Bool in
                let date = Date.getDateFromTimeInterval(activityValueModel.createdOn)
                return self.selectedDate.isSameDate(date)
            }.first
            var text = valueMod?.value ?? ""
            if valueMod?.value == "true" {
                text = "Yes"
            } else if valueMod?.value == "false" {
                text = "No"
            }
            let detailModel = CellDetails(title: each.name ?? "", subtitle: text, image: #imageLiteral(resourceName: "icnAppoinment"), isCompleted: isCompleted)
            cellDetails.append(detailModel)
        }
        model.append(CareModel(title: careCard.surveySectionName ?? "", numberOfRows: cellDetails.count, details: cellDetails))

        return model
    }
}
