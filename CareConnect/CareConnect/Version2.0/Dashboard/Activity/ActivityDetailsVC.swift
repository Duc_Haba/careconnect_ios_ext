//
//  ActivityDetailsVC.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 05/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import VACalendar
import RealmSwift

class ActivityDetailsVC: UIViewController {

    @IBOutlet weak var loaderStackView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var daysLeftLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var weekDaysView: VAWeekDaysView! {
        didSet {
            let appereance = VAWeekDaysViewAppearance(symbolsType: .veryShort, weekDayTextColor: .black, separatorBackgroundColor: .clear, calendar: defaultCalendar)
            
            weekDaysView.appearance = appereance
        }
    }
    @IBOutlet weak var calenderView: UIView!

    var vacalendarView: VACalendarView?
    let defaultCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        if let zone = TimeZone(secondsFromGMT: 0) {
            calendar.timeZone = zone
        }
        return calendar
    }()
    var activityVM = ActivityVM()
    var isDiabetic: Bool = true
    var model: [ActivityVM.CareModel] = []
    var selectedIndex = IndexPath()
    var totalCount = 0
    var selectedID = 0
    var updateCount = 0
    var datesRange = [Date]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerCells()
        loaderStackView.isHidden = true
        activityVM.setUpModel()
        let date = Date.getDateStringFromTimeStamp(activityVM.selectedCareCard?.createdOn ?? 0, format: slotDateFormat)
        let fromDate = Date.getDateFromTimeInterval(activityVM.selectedCareCard?.startDate ?? 0)
        let toDate = Date.getDateFromTimeInterval(activityVM.selectedCareCard?.targetBy ?? 0)
        datesRange = Date.dates(from: fromDate, to: toDate)
        dateLabel.text = date
        model = activityVM.careCardModel.model
        daysLeftLabel.text = activityVM.careCardModel.duration
        titleLabel.text = activityVM.careCardModel.title
        totalCount = activityVM.totalCount
        updateCount = activityVM.surveyCountCompleted
        setupCalendar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if activityVM.didReadDocument {
            updateCount += 1
        }
        updateProgress()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if vacalendarView?.frame == .zero {
            vacalendarView?.frame = CGRect(
                x: 0,
                y: weekDaysView.frame.maxY,
                width: view.frame.width * 0.97,
                height: calenderView.frame.height * 3
            )
            vacalendarView?.setup()
        }
    }
    
    private func setupCalendar() {
        let calendar = VACalendar(selectedDate: Date(), calendar: defaultCalendar)
        
        vacalendarView = VACalendarView(frame: .zero, calendar: calendar)
        vacalendarView?.showDaysOut = true
        vacalendarView?.selectionStyle = .single
        vacalendarView?.dayViewAppearanceDelegate = self
        vacalendarView?.calendarDelegate = self
        vacalendarView?.scrollDirection = .horizontal
        vacalendarView?.changeViewType()
        let dates = setSuplimentariesForDates(from: Date.getDateFromTimeInterval(activityVM.selectedCareCard?.startDate ?? 0), to: Date().previousDay ?? Date())
        vacalendarView?.setSupplementaries(dates)
        let availableDays = DaysAvailability.some(datesRange)
        vacalendarView?.setAvailableDates(availableDays)
        calenderView.addSubview(vacalendarView ?? UIView())
    }
    
    private func setSuplimentariesForDates(from: Date, to: Date, type: [VADaySupplementary] = [VADaySupplementary.bottomDots([.red])]) -> [(Date, [VADaySupplementary])] {
        var supplimentaries: [(Date, [VADaySupplementary])] = []
        let datesRange = Date.dates(from: from, to: to.previousDay ?? Date())
        let completedDatesRange = activityVM.completedDays
        let greenType = [VADaySupplementary.bottomDots([.green])]
        var setType = type
        for each in datesRange {
            setType = type
            let isPresent = completedDatesRange.filter { $0.isSameDate(each) }
            if !isPresent.isEmpty {
                setType = greenType
            }
            print("\(each),\(setType)")
            supplimentaries.append((each, setType))
        }
        return supplimentaries
    }
    
    private func registerCells() {
        tableView.register(CareDetailTableCell.self)
        tableView.registerReusableHeaderFooterView(HomeTableSectionHeader.self)
        tableView.tableFooterView = UIView()
    }
    
    private func updateProgress() {
        DispatchQueue.main.async { [weak self] in
            guard let weakSelf = self else {
                return
            }
            let percentage = (100 * weakSelf.updateCount) / weakSelf.totalCount
            weakSelf.percentageLabel.text = "\(percentage)% completed"
            
            weakSelf.progressViewWidthConstraint.constant = (UIScreen.main.bounds.width - 48 ) / (CGFloat(weakSelf.totalCount) / CGFloat(weakSelf.updateCount))
            if weakSelf.updateCount == 0 {
                weakSelf.progressViewWidthConstraint.constant = 0
            }
        }
    }
    
    @IBAction private func didTapClose(_ sender: Any) {
        dismiss(animated: true)
    }
}

extension ActivityDetailsVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model[section].numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        cell = createActivityCell(indexPath: indexPath) ?? UITableViewCell()
        return cell
    }
    
    func createActivityCell(indexPath: IndexPath) -> CareDetailTableCell? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CareDetailTableCell.className, for: indexPath) as? CareDetailTableCell else {
            return nil
        }
        let currentModel = model[indexPath.section].details[indexPath.row]
        
        let date = Date.getDateStringFromTimeStamp(activityVM.selectedDate.timeIntervalSince1970 * 1000, format: dateFormat)
        let today = Date.getDateStringFromTimeStamp(Date().timeIntervalSince1970 * 1000, format: dateFormat)
        let textColor: UIColor = date == today ? .black : .lightGray
        cell.titileLabel.textColor = textColor
        cell.subTitileLabel.textColor = textColor
        cell.titileLabel.text = currentModel.title
        cell.subTitileLabel.text = currentModel.subtitle
        cell.accesoryImageView?.image = currentModel.image
        let image = currentModel.isCompleted ? #imageLiteral(resourceName: "Icon-Selected") : #imageLiteral(resourceName: "Icon-UnSelected")
        cell.checkButton.setImage(image, for: .normal)
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: HomeTableSectionHeader.className) as? HomeTableSectionHeader
        let currentModel = model[section]
        header?.titleLabel.text = currentModel.title
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath
        let date = Date.getDateStringFromTimeStamp(activityVM.selectedDate.timeIntervalSince1970 * 1000, format: dateFormat)
        let today = Date.getDateStringFromTimeStamp(Date().timeIntervalSince1970 * 1000, format: dateFormat)
        guard date == today else { return }
        guard indexPath.section != 1 else {
            let linkDetailVC = LinkDetailsVC(nibName: LinkDetailsVC.className, bundle: nil) 
            linkDetailVC.delegate = self
            linkDetailVC.shouldHideButton = model[indexPath.section].details[indexPath.row].isCompleted
            navigationController?.pushViewController(linkDetailVC, animated: true)
            return
        }
        
        guard !model[indexPath.section].details[indexPath.row].isCompleted else { return }
        
        if model[indexPath.section].title == activityVM.selectedCareCard?.activitySectionName ?? "" {
            loaderStackView.isHidden = false
            activityVM.postActivity(id: activityVM.selectedCareCard?.activitiesList[indexPath.row].id ?? 11, value: "") { [weak self] isSuccess, err in
                DispatchQueue.main.async {
                    self?.loaderStackView.isHidden = true
                }
                if isSuccess {
                    self?.updateModelStatus(indexPath: indexPath)
                }
            }
        } else {
            let painSurveyVC = PainSurveyViewController(nibName: PainSurveyViewController.className, bundle: nil)
            activityVM.selectedSurvey = activityVM.selectedCareCard?.surveysList[indexPath.row]
            painSurveyVC.activityVM = activityVM
            painSurveyVC.surveyDelegate = self
            self.navigationController?.pushViewController(painSurveyVC, animated: true)
        }
    }
    
    private func updateModelStatus(indexPath: IndexPath) {
        if indexPath.section != 1 && !activityVM.didReadDocument {
            updateCount += 1
            activityVM.surveyCountCompleted += 1
        }
        updateProgress()
        model[indexPath.section].details[indexPath.row].isCompleted = true
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }

}

extension ActivityDetailsVC: ReadDelegate {
    func didReadArticle() {
        activityVM.didReadDocument = true
        updateModelStatus(indexPath: selectedIndex)
    }
}

extension ActivityDetailsVC: VADayViewAppearanceDelegate {
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return UIColor(red: 199 / 255, green: 199 / 255, blue: 212 / 255, alpha: 1.0)
        case .selected:
            return .white
        case .unavailable:
            return .lightGray
        default:
            return .black
        }
    }
    
    func font(for state: VADayState) -> UIFont {
        return UIFont.systemFont(ofSize: 12)
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            return Color.appPrimaryColor.uiColor
        default:
            return .clear
        }
    }
    
    func shape() -> VADayShape {
        return .circle
    }
    
    func dotBottomVerticalOffset(for state: VADayState) -> CGFloat {
        return 2
    }
}

extension ActivityDetailsVC: VACalendarViewDelegate {
    
    func selectedDate(_ date: Date) {
        activityVM.selectedDate = date
        vacalendarView?.startDate = date
        activityVM.setUpModel()
        model = activityVM.careCardModel.model
        tableView.reloadData()
    }
    
}

extension ActivityDetailsVC: UpdateSurveyDelegate {
    func didUpdateSurvey() {
        updateModelStatus(indexPath: selectedIndex)
    }
}
