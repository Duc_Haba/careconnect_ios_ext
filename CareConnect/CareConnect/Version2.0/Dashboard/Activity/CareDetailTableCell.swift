//
//  CareDetailTableCell.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 05/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class CareDetailTableCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var titileLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var accesoryImageView: UIImageView!
    @IBOutlet weak var subTitileLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var isChecked: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.dropShadow(cornerRadius: 16)
    }
    
}
