//
//  LinkDetailsVC.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 05/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

protocol ReadDelegate: class {
    func didReadArticle()
}

class LinkDetailsVC: UIViewController {

    @IBOutlet weak var footerButton: UIButton!
    
    weak var delegate: ReadDelegate?
    var shouldHideButton: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        footerButton.isHidden = shouldHideButton
        footerButton.dropShadow(cornerRadius: 16)
        footerButton.setGradientBackground(cornerRadius: 12)
    }

    @IBAction private func didPressBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func didPressRead(_ sender: Any) {
        delegate?.didReadArticle()
        navigationController?.popViewController(animated: true)
    }
}
