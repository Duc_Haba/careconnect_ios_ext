//
//  SwitchCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 07/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SwitchCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var toggle: UISwitch!
    @IBOutlet weak var titleText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        toggle.isOn = false
    }
    
}
