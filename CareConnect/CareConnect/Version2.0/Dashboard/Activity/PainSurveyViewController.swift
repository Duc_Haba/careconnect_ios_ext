//
//  PainSurveyViewController.swift
//  CareConnect
//
//  Created by Venugopal S A on 07/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

protocol UpdateSurveyDelegate: class {
    func didUpdateSurvey()
}

class PainSurveyViewController: UIViewController {
    
    @IBOutlet weak var loaderStackView: UIView!
    @IBOutlet weak var screenTitleLabel: UILabel!
    @IBOutlet weak var surveyTableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var progressView: UIView!
    
    weak var surveyDelegate: UpdateSurveyDelegate?
    var surveyInputType = SurveyInputType.numeric
    weak var activityVM: ActivityVM?
    var value: String = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        surveyInputType = SurveyInputType(rawValue: activityVM?.selectedSurvey?.type ?? "NUMERICAL") ?? .numeric
        var title = "Pain Survey"
        switch surveyInputType {
        case .scale:
            title = "Weight"
        case .text:
            title = "Log"
        case .toggle:
            title = "Update"
        case .numeric:
            title = "Pain Survey"
        }
        screenTitleLabel.text = title
        nextButton.setGradientBackground(cornerRadius: 8)
        surveyTableView.register(PainSurveyTextTableCell.self)
        surveyTableView.register(SliderTableViewCell.self)
        surveyTableView.register(SwitchCell.self)
        surveyTableView.register(ScaleCell.self)
        surveyTableView.registerReusableHeaderFooterView(HomeTableSectionHeader.self)
        loaderStackView.isHidden = true
    }
    
    @IBAction private func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func nextTapped(_ sender: Any) {
        self.loaderStackView.isHidden = false
        activityVM?.postSurvey(id: activityVM?.selectedSurvey?.id ?? 8, value: value) { [weak self] isSuccess, err in
            DispatchQueue.main.async {
                self?.loaderStackView.isHidden = true
                self?.surveyDelegate?.didUpdateSurvey()
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
}

extension PainSurveyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch surveyInputType {
      
        case .text:
            let cell = tableView.dequeueReusableCell(for: indexPath) as PainSurveyTextTableCell
            cell.searchTextField.keyboardType = .numberPad
            cell.searchTextField.delegate = self
            cell.titleLabel.text = activityVM?.selectedSurvey?.name
            cell.searchTextField.text = ""
            cell.searchTextField.placeholder = "Enter Here"
            return cell
        case .scale:
             let cell = tableView.dequeueReusableCell(for: indexPath) as ScaleCell
             cell.scaleDelegate = self
             return cell
        case .numeric:
            let cell = tableView.dequeueReusableCell(for: indexPath) as SliderTableViewCell
            cell.slider.addTarget(self, action: #selector(valueChanged(_:)), for: .valueChanged)
            return cell
        case .toggle:
            let cell = tableView.dequeueReusableCell(for: indexPath) as SwitchCell
            cell.titleText.text = "Update Value"
            cell.toggle.addTarget(self, action: #selector(didToggle(_:)), for: .valueChanged)
            return cell
        }
        
    }
    
    @objc func valueChanged(_ sender: UISlider) {
        let changedValue = floor(sender.value)
        value = changedValue.cleanValue
    }
    
    @objc func didToggle(_ sender: UISwitch) {
        value = "\(sender.isOn)"
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: HomeTableSectionHeader.className) as? HomeTableSectionHeader
        var title = "Enter Value"
        switch surveyInputType {
        case .text:
            title = activityVM?.selectedSurvey?.name ?? ""
        case .numeric:
            title = activityVM?.selectedSurvey?.name ?? ""
        case .scale:
            title = "How much do you weight?"
        case .toggle:
            title = activityVM?.selectedSurvey?.name ?? ""
        }
        header?.titleLabel.text = title
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 300
        if surveyInputType == .toggle {
            height = 52
        } else if surveyInputType == .text {
            height = 80
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
}

extension PainSurveyViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        value = textField.text ?? "1"
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        value = textField.text ?? "1"
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        value = textField.text ?? "1"
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            value = text + string
        }
        return true
    }
}

extension PainSurveyViewController: ScaleDelegate {
    func didChangeScaleValue(value: Int) {
        self.value = "\(value)"
    }
}

extension Float {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
