//
//  SliderTableViewCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 07/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SliderTableViewCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var finalValue: UILabel!
    @IBOutlet weak var slider: CustomSlider!
    @IBOutlet weak var smileyImageView: UIImageView!
    
    let smileyDivider = 0.7
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let min: Float = 1
        finalValue.text = "\(Int(min))"
        slider.minimumValue = min
        slider.maximumValue = 10
        slider.isContinuous = true
        slider.sliderThumbImage = UIImage(named: "group2")
        slider.thickness = 10
        smileyImageView.frame = CGRect(x: 15, y: 0, width: 50, height: 50)
        smileyImageView.image = UIImage(named: "Smile_1")
        let trackRect = slider.trackRect(forBounds: slider.frame)
        let thumbRect = slider.thumbRect(forBounds: slider.bounds, trackRect: trackRect, value: slider.value)

        finalValue.center = CGPoint(x: thumbRect.midX, y: finalValue.center.y)
        }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction private func valueChanged(_ sender: UISlider) {
        
        let sliderValue = Double(sender.value)
        smileyImageView.image = UIImage(named: "Smile_" + "\(Double(sliderValue * 0.7).rounded(.toNearestOrAwayFromZero))")
        finalValue.text = "\(Int(sliderValue))"
        let trackRect = sender.trackRect(forBounds: sender.frame)
        let thumbRect = sender.thumbRect(forBounds: sender.bounds, trackRect: trackRect, value: sender.value)
        finalValue.center = CGPoint(x: thumbRect.midX, y: finalValue.center.y)
        smileyImageView.center = CGPoint(x: thumbRect.midX, y: smileyImageView.center.y)
    }
    
}
