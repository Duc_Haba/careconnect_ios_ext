//
//  DashboardVM.swift
//  CareConnect
//
//  Created by Venugopal S A on 01/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import Rudder

struct CareCard {
    var id: Int?
    var name: String?
    var activitySectionName: String?
    var activites: [CareActivity]?
    var surveySectionName: String?
    var surveys: [Survey]?
    var patientId: Int?
    var createdOn: Double?
    var startDate: Double?
    var targetBy: Double?
}

struct CareActivity {
    var createdOn: Double?
    var id: Int?
    var name: String?
    var values: [CareActivityValue]?
}

struct CareActivityValue {
    var completedOn: Double?
}

struct Survey {
    var id: Int?
    var name: String?
    var type: SurveyInputType?
    var frequency: String?
    var units: String?
    var values: [SurveyValue]?
}

struct SurveyValue {
    var value: String?
    var createdOn: Double?
}

enum SurveyInputType: String {
    case numeric = "NUMERICAL"
    case text = "TEXT"
    case scale = "SCALE"
    case toggle = "BOOLEAN"
}

enum SectionType {
    case dynamic
    case connectedDevices
    case careTeam
}

struct DashboardModel {
    var numOfRows: Int?
    var cellDetails: [CellData]?
    var title: String?
    var sectionType: SectionType?
    
    struct CellData {
        var cellType: CellType?
    }
}

class DashboardVM {
    
    var medications = [Medication]()
    var careCards = [CareCard]()
    var model: [DashboardModel] = []
    var careTeam: [CareTeamModel] = []
    var dashboardAppointments: [AppointmentsModel] = []
    var dashboardMedications: [Medication] = []
    var activites: [ActivityModel] = []
    var users = [Dependents]()
    
    func setUpDataSource() {
        model = []
        var cellDetails: [DashboardModel.CellData] = []
        if !dashboardAppointments.isEmpty {
            for _ in dashboardAppointments {
                cellDetails.append(DashboardModel.CellData(cellType: .appointment))
            }
        }
        model.append(DashboardModel(numOfRows: cellDetails.count, cellDetails: cellDetails, title: "", sectionType: .dynamic))

        cellDetails = []
        if !dashboardMedications.isEmpty {
            for _ in dashboardMedications {
                cellDetails.append(DashboardModel.CellData(cellType: .medication))
            }
        }
        model.append(DashboardModel(numOfRows: cellDetails.count, cellDetails: cellDetails, title: "", sectionType: .dynamic))

        cellDetails = []
        if !activites.isEmpty {
            for _ in activites {
                cellDetails.append(DashboardModel.CellData(cellType: .careCard))
            }
        }
        model.append(DashboardModel(numOfRows: cellDetails.count, cellDetails: cellDetails, title: "", sectionType: .dynamic))
        
        cellDetails = []
        cellDetails.append(DashboardModel.CellData(cellType: .link))
        model.append(DashboardModel(numOfRows: cellDetails.count, cellDetails: cellDetails, title: "", sectionType: .dynamic))

        if Defaults.shared.conncetedDeviceType != ShimType.none.rawValue {
            let shimCells = [DashboardModel.CellData(cellType: .shim)]
            model.append(DashboardModel(numOfRows: shimCells.count, cellDetails: shimCells, title: "Connected Devices", sectionType: .connectedDevices))
        }
        
        cellDetails = []
        if !careTeam.isEmpty {
            for _ in careTeam {
                cellDetails.append(DashboardModel.CellData(cellType: .careTeam))
            }
            model.append(DashboardModel(numOfRows: 1, cellDetails: cellDetails, title: "My Care Team", sectionType: .careTeam))
        }
    }
    
    func getUser(userID: Int) -> Dependents {
        guard let user = RealmManager.shared.getObjects(type: Dependents.self, with: userID)  else {
            return Dependents()
        }
        return user
    }
    
   func getUsersDataFromRealm() {
        users = []
        let userData = RealmManager.shared.getObjects(type: Dependents.self)
        let dependents = userData.filter({ $0.relationship != "self" })
        guard let currentUser = userData.filter({ $0.relationship == "self" }).first else {
            return
        }
        users.append(currentUser)
        users.append(contentsOf: dependents)
    }
    
    func logout(completionHandler: @escaping (Bool, String?) -> Void) {
        let headers = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let path = Apiconstants.baseUrl + CareConnectURLs.logout.rawValue
        
        guard let request = NetworkManager.requestForURL(path, method: .Delete, params: [:], headers: headers) else {
            return
        }
        
        _ = NetworkManager.request(request) { _, error, _ in
            if error == nil {
                completionHandler(true, nil)
                
            } else {
                completionHandler(false, error?.localizedDescription ?? "")
            }
        }
    }
    
    func fetchMedications(date: Int, patientId: Int, callback: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        medications = []
        let header = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let path = Apiconstants.baseUrl + CareConnectURLs.medications.rawValue + "?patientId=\(patientId)" + "&date=\(date)"
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: header) else {
            return
        }
        _ = NetworkManager.request(request) { response, error, _ in
            if error == nil {
                guard let data = response?.dictionaryObject else {
                    return
                }
                self.getMedications(data: data)
                callback(true, nil)
            } else {
                callback(false, error)
            }
        }
    }
    
    private func getMedications(data: [String: Any], isDashboard: Bool = false) {
        medications = []
        dashboardMedications = []
        guard let response = data["entry"] as? [[String: Any]] else { return }
        response.enumerated().forEach { _, each in
            let meds = Medication()
            meds.consumed = false
            guard let resource = each["resource"] as? [String: Any], let consumptionStatus = resource["consumptionStatus"] as? Int, let patientMedicationId = resource["patientMedicationId"] as? Int, let medicationRef = resource["medicationReference"] as? [String: Any] else { return }
            meds.consumptionStatus = consumptionStatus
            meds.patientMedicationId = patientMedicationId
            if let name = medicationRef["display"] as? String {
                var medName = name
                if let tabName = name.components(separatedBy: ")").first {
                    medName = tabName
                }
                meds.name = medName.capitalized
                
            }
            
            if let dosageInstruction = resource["dosageInstruction"] as? [[String: Any]] {
                meds.asNeeded = dosageInstruction.first?["asNeededBoolean"] as? Bool ?? true
                meds.dosage = dosageInstruction.first?["text"]  as? String ?? ""
            }
            if isDashboard {
                dashboardMedications.append(meds)
            } else {
                medications.append(meds)
            }
        }
    }
    
    func updateMedication(date: Int, statusCode: Int, patientMedicationID: Int, callback: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        let header = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let path = Apiconstants.baseUrl + CareConnectURLs.medication.rawValue + "?patientMedicationId=\(patientMedicationID)" + "&status=\(statusCode)" + "&date=\(date)"
        guard let request = NetworkManager.requestForURL(path, method: .Post, params: [:], headers: header) else {
            return
        }
        _ = NetworkManager.request(request) { response, error, _ in
            if error == nil {
                callback(true, nil)
            } else {
                callback(false, error)
            }
        }
    }
    
    func fetchDashboardDetails(callback: @escaping (_ success: Bool) -> Void) {
        let header = HeaderConfiguration.token(token: Defaults.shared.sessionToken).header
        let patientID = Defaults.shared.selectedProfileID
        let path = Apiconstants.baseUrl + "home?patientId=\(patientID)"
        
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: header) else {
            return
        }
        _ = NetworkManager.request(request) {[weak self] response, error, _ in
            if error == nil {
                if let json = response {
                    self?.parseDashboardDetails(json: json)
                    callback(true)
                } else {
                    callback(false)
                }
            } else {
                callback(false)
            }
        }
    }
    
    private func parseDashboardDetails(json: JSON) {
        careTeam = []
        dashboardAppointments = []
        activites = []
        if let careData = try? JSONDecoder().decode([CareTeamModel].self, from: json["data"]["careTeam"].rawData()) {
            DispatchQueue.main.async { [weak self] in
                self?.careTeam = careData
                CareTeamModel.storeInDataBase(teamData: careData)
            }

        }
        
        if let appointments = try? JSONDecoder().decode(AppointmentsModel.self, from: json["data"]["appointment"].rawData()) {
            dashboardAppointments = [appointments]
        }
        
        if let activityData = try? JSONDecoder().decode([ActivityModel].self, from: json["data"]["careCards"].rawData()) {
            DispatchQueue.main.async { [weak self] in
                self?.activites = activityData
                ActivityModel.storeInDataBase(teamData: activityData)
            }
        }
        
        guard let response = json.dictionaryObject, let data = response["data"] as? [String: Any], let meds = data["medications"] as? [String: Any] else {
            return
        }
        
        self.getMedications(data: meds, isDashboard: true)
        
        self.setUpDataSource()
    }
    
    private func clearData() {
        RealmManager.shared.deleteObjects(object: CareTeamModel.self)
        RealmManager.shared.deleteObjects(object: AppointmentsModel.self)
        RealmManager.shared.deleteObjects(object: ActivityModel.self)
        RealmManager.shared.deleteObjects(object: ActivitiesListModel.self)
        RealmManager.shared.deleteObjects(object: SurveysListModel.self)
        RealmManager.shared.deleteObjects(object: ActivityValuesModel.self)
        dashboardMedications = []
    }
    
}
