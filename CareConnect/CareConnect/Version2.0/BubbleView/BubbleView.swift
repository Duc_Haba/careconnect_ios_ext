//
//  CareAlertView.swift
//  Compass
//
//  Created by Arun on 2/22/18.
//  Copyright © 2018 Y Media Labs. All rights reserved.
//

import UIKit

protocol BubbleViewDelegate: class {
    func bubbleView(_ bubbleView: BubbleView, didSelectCloseAt index: Int)
}

protocol BubbleViewTapDelegate: class {
    func bubbleView(_ alertView: BubbleView, didSelectAt index: Int)
}

final class BubbleView: UIView {
    
    @IBOutlet weak var trailing: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var checkButton: UIButton!
    
    weak var delegate: BubbleViewDelegate?
    weak var tapDelegate: BubbleViewTapDelegate?
    var currentIndex: Int = 0
    var isSelected: Bool = false

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        loadFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        loadFromNib()
    }
    
    public func loadFromNib() {
        guard let view = viewFromNib() else {
            return
        }
        
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        
        let left = view.leftAnchor.constraint(equalTo: leftAnchor)
        let right = view.rightAnchor.constraint(equalTo: rightAnchor)
        let top = view.topAnchor.constraint(equalTo: topAnchor)
        let bottom = view.bottomAnchor.constraint(equalTo: bottomAnchor)
        NSLayoutConstraint.activate([ left, right, top, bottom ])
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
        view.addGestureRecognizer(recognizer)

    }
    
    private func viewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    // MARK: Event Handlers
    
    @IBAction private func didSelectRemove() {
        delegate?.bubbleView(self, didSelectCloseAt: currentIndex)
    }
    
    @objc dynamic func handleTap(recognizer: UITapGestureRecognizer) {
        tapDelegate?.bubbleView(self, didSelectAt: currentIndex)
    }

}
