//
//  TagView.swift
//  Compass
//
//  Created by Arun on 2/23/18.
//  Copyright © 2018 Y Media Labs. All rights reserved.
//

import UIKit

protocol TagViewDelegate: class {
    func tagView(_ tagView: TagView, didRemoveTagAt index: Int)
    func tagView(_ tagView: TagView, didUpdateTagHeight height: CGFloat)
}

protocol TagViewDataSource: class {
    func tagView(_ tagView: TagView, titleTextAt index: Int) -> String
}

protocol TagViewTapDelegate: class {
    func tagView(_ tagView: TagView, didSelectAlertAt index: Int)
}

final class TagView: UIView {
    
    weak var delegate: TagViewDelegate?
    weak var tagTapDelegate: TagViewTapDelegate?
    weak var dataSource: TagViewDataSource?
    var numberOfTagsToShow: Int = 0
    var isOpaqueState: Bool = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setuptags()
    }
    
    func setuptags() {
        // This is to remove previous views if present
        subviews.forEach { $0.removeFromSuperview() }
        
        var alerts: [BubbleView] = []
        var tagViewHeight: CGFloat = 0.0
        
        if numberOfTagsToShow > 0 {
            for index in 0...numberOfTagsToShow - 1 {
                // If its not first object, then add the alert relative to previous view.
                let frame = index > 0 ? alerts[index - 1].frame : .zero
                let title = dataSource?.tagView(self, titleTextAt: index) ?? ""
                let alert = BubbleView(frame: frame, title: title, parent: self, isOpaqueState: isOpaqueState)
                alert.currentIndex = index
                alert.delegate = self
                if isOpaqueState {
                    alert.tapDelegate = self
                }
                alerts.append(alert)
                addSubview(alert)
                
                tagViewHeight = alert.frame.maxY
            }
        }
        
        delegate?.tagView(self, didUpdateTagHeight: tagViewHeight)
    }
    
}

// MARK: - BubbleViewDelegate

extension TagView: BubbleViewDelegate {
    
    func bubbleView(_ alertView: BubbleView, didSelectCloseAt index: Int) {
        UIView.animate(withDuration: 0.3, animations: {
            alertView.alpha = 0
        }, completion: { _ in
            alertView.removeFromSuperview()
            self.numberOfTagsToShow -= 1
            self.delegate?.tagView(self, didRemoveTagAt: index)
        })
    }
    
}

// MARK: - TapDelegate

extension TagView: BubbleViewTapDelegate {
    
    func bubbleView(_ alertView: BubbleView, didSelectAt index: Int) {
        UIView.animate(withDuration: 0.3, animations: {
            alertView.isSelected = !alertView.isSelected
            let color: UIColor = Color.appPrimaryColor.uiColor
            if alertView.isSelected {
                alertView.backgroundColor = color
                alertView.titleLabel.textColor = .white
            } else {
                alertView.titleLabel.textColor = color
                alertView.backgroundColor = .white
            }
        }, completion: { _ in
            self.tagTapDelegate?.tagView(self, didSelectAlertAt: index)
        })
    }
    
}

// MARK: - BubbleView

extension BubbleView {
    
    convenience init(frame: CGRect, title: String, parent: UIView, isOpaqueState: Bool) {
        self.init(frame: frame)
        
        let font: UIFont = Font.maaxMedium.getFont(with: 14)
        let padding: CGFloat = 12
        let spacing: CGFloat = 16
        let alertHeight: CGFloat = 40
        var bgColor: UIColor = Color.appPrimaryColor.uiColor
        var textColor: UIColor = .white
        self.removeButton.isHidden = false
        trailing.constant = 30
        titleLabel.textAlignment = .left
        checkButton.isHidden = true

        if isOpaqueState {
            bgColor = .clear
            textColor = Color.appPrimaryColor.uiColor
            self.removeButton.isHidden = true
            trailing.constant = 16
            titleLabel.textAlignment = .center
        }
        self.backgroundColor = bgColor
        self.titleLabel.textColor = textColor

        // Get the size of the string based on font and its size.
        let size = title.size(withConstrainedWidth: parent.bounds.width - (padding * 2), font: font)
        // Padding width left without titleLabel.
        // This is the width that needs to be added for every tag view, along with the size that is calculated above.
        var remainingWidth: CGFloat = 0
        if let frameWidth = subviews.first?.frame.width {
            // Get the width that will be remaining in a tag view without UIlabel.
            remainingWidth = (frameWidth - titleLabel.frame.width) + padding
        }
        
        // Calculate the width for the alert view. if it exceeds the current views width, then reduce the width which will fit into the current screen.
        var alertViewWidth = min(parent.bounds.maxX, size.width + remainingWidth)
        if isOpaqueState {
            alertViewWidth = parent.bounds.width / 2 - (padding)
        }
        // Check if the item can fit in row
        let canNewAlertViewFitInCurrentRow = (alertViewWidth + frame.maxX + padding) <= parent.bounds.maxX
        if canNewAlertViewFitInCurrentRow {
            // If its the first object, then margin should not be considered while adding views in the same row
            let xPosition = (frame == .zero) ? 0 : frame.maxX + spacing
            self.frame = CGRect(x: xPosition, y: frame.origin.y, width: alertViewWidth, height: alertHeight)
        } else {
            self.frame = CGRect(x: 0, y: frame.maxY + spacing, width: alertViewWidth, height: alertHeight)
        }
        
        titleLabel.setTitle(title: title, forFont: font, forColor: textColor)
        self.setGradientBackground(cornerRadius: 20)
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.borderWidth = 1
        self.layer.borderColor = Color.appPrimaryColor.cgColor
    }
}

extension UILabel {
    func setTitle(title: String? = nil, forFont textFont: UIFont? = nil, forColor color: UIColor? = nil) {
        text = title
        font = textFont
        textColor = color
    }
}
