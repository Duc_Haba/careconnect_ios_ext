//
//  LoginSignUpViewController.swift
//  CareConnect
//
//  Created by Venugopal S A on 11/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction private func signUpTapped(_ sender: Any) {
        let storyBoardObj = self.storyboard?.instantiateViewController(identifier: "SignUpViewController") as? SignUpViewController
        self.navigationController?.pushViewController(storyBoardObj!, animated: true)
    }
    
    @IBAction private func loginTapped(_ sender: Any) {
        let storyBoardObj = self.storyboard?.instantiateViewController(identifier: "LoginVC") as? LoginVC
        self.navigationController?.pushViewController(storyBoardObj!, animated: true)
    }
    
    @IBAction private func termsAndConditionsTapped(_ sender: Any) {
        
    }
}
