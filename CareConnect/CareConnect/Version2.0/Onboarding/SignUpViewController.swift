//
//  SignUpViewController.swift
//  CareConnect
//
//  Created by Venugopal S A on 11/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    enum SignUpInformation: Int, CaseIterable {
        case firstName = 0
        case lastName
        case phoneNumber
        case emailID
        case patientID
        case password
        
        var title: String {
            switch self {
            case .firstName:
                return "First Name"
            case .lastName:
                return "Last Name"
            case .phoneNumber:
                return "Phone Number"
            case .emailID:
                return "Email ID"
            case .patientID:
                return "Patient ID"
            case .password:
                return "Password"
            }
        }
        
        var description: String {
            switch self {
            case .firstName:
                return "Enter FirstName"
            case .lastName:
                return "Enter LastName"
            case .phoneNumber:
                return "Enter PhoneNumber"
            case .emailID:
                return "Enter EmailID"
            case .patientID:
                return "Enter PatientID"
            case .password:
                return "Enter Password"
            }
        }
        
    }
    
    @IBOutlet weak var signUpTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerCells()
    }
    
    func registerCells() {
        signUpTableView.register(SignUpTextfieldCell.self)
        signUpTableView.register(PhoneNumberCell.self)
    }
    
    @IBAction private func signUpTapped(_ sender: Any) {
        
    }
    
    @IBAction private func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SignUpViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SignUpInformation.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let signUpInfo = SignUpInformation(rawValue: indexPath.row) else {
            return UITableViewCell()
        }
        
        if signUpInfo == .phoneNumber {
            let cell = tableView.dequeueReusableCell(for: indexPath) as PhoneNumberCell
            cell.selectionStyle = .none
            
            cell.countryCodeTextfield.text = "+ 91"
            cell.phoneTextfield.placeholder = signUpInfo.description
            cell.titleLabel.text = signUpInfo.title
            cell.phoneTextfield.placeHolderTextColor = Color.placeHolderColor.uiColor
            return cell
        }
        let cell = tableView.dequeueReusableCell(for: indexPath) as SignUpTextfieldCell
        cell.selectionStyle = .none
      
        cell.title.text = signUpInfo.title
        cell.celltextfield.tag = indexPath.row
        cell.celltextfield.placeholder = signUpInfo.description
        cell.celltextfield.placeHolderTextColor = Color.placeHolderColor.uiColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        return UIView()
    }
}
