//
//  LoginVC.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 17/06/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import Rudder

class LoginVC: UIViewController, KeyboardNotificationProvider {
    
    @IBOutlet weak var loaderStackView: UIView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var userNameTitleLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordTitleLabel: UILabel!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loginButtonBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewCenterContraints: NSLayoutConstraint!
    var  viewBottomConstraintValue: CGFloat = 0.0
    let loginVM = LoginVM()
    var reachability = Reachability()
    var showNotification: NSObjectProtocol?
    var hideNotification: NSObjectProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTextField.becomeFirstResponder()
        errorLabel.isHidden = true
        loaderStackView.isHidden = true
        setUpTextFields()
        registerKeyboardNotification()
        
    }
    
    func setUpTextFields() {
        userNameTextField.placeholder = "Enter Email ID"
        passwordTextField.placeholder = "Enter Password"
        userNameTextField.placeHolderTextColor = .gray
        passwordTextField.placeHolderTextColor = .gray
    }
    
    private func showError(_ error: Error) {
        errorLabel.isHidden = false
        errorLabel.textColor = .red
        self.passwordTitleLabel.textColor = .red
        self.userNameTitleLabel.textColor = .red
        errorLabel.text = error.localizedDescription
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    @IBAction private func forgotTapped(_ sender: Any) {
        let forgotVC = self.storyboard?.instantiateViewController(identifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
    
    @IBAction private func signInButtonAction(_ sender: UIButton) {
        view.endEditing(true)
        loginVM.userName = userNameTextField.text
        loginVM.password = passwordTextField.text
        if (validationCheck(textField: userNameTextField) && validationCheck(textField: passwordTextField)) {
            signIn()
        }
        
    }
    
    @IBAction private func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func validationCheck(textField: UITextField) -> Bool {
        
        let errors = loginVM.emptyFieldErrors
        
        switch textField {
        case userNameTextField:
            if errors.contains(.userNameMissing) { showError(LoginVM.ValidationError.userNameMissing)
            }
            userNameTitleLabel.textColor = errors.contains(.userNameMissing) ? .red : .white
        case passwordTextField:
            if errors.contains(.passwordMissing) { showError(LoginVM.ValidationError.passwordMissing) }
            passwordTitleLabel.textColor = errors.contains(.passwordMissing) ? .red : .white
        default:
            assertionFailure("Invalid Text Field in Sign in screen")
        }
        
        if errors.isEmpty {
            userNameTitleLabel.textColor = .white
            passwordTitleLabel.textColor = .white
            errorLabel.isHidden = true
            errorLabel.text = nil
            return true
        }
        
        return false
        
    }
    
    private func signIn() {
        loaderStackView.isHidden = false
        do {
            try loginVM.submit { [weak self] isValid, error  in
                DispatchQueue.main.async {

                    if isValid {
                        if self?.reachability?.isReachable ?? false {
                            appDelegate?.switchToDashboard()
                            self?.loaderStackView.isHidden = true
                        } else {
                            self?.errorLabel.isHidden = false
                            self?.errorLabel.textColor = .red
                            self?.errorLabel.text = "No internet"
                            self?.loaderStackView.isHidden = true

                        }
                        // Push to dashboard
                    } else {
                        self?.showError(error)
                        self?.loaderStackView.isHidden = true
                    }
                }
            }
        } catch {
            guard let error = error as? LoginVM.ValidationError else {
                assertionFailure("Invalid validation error received from Login View Model")
                self.loaderStackView.isHidden = true
                return
            }
            
            showError(error)
            self.loaderStackView.isHidden = true
            if (loginVM.userName ?? "" ).isEmpty { userNameTitleLabel.textColor = .red }
            if (loginVM.password ?? "" ).isEmpty { passwordTitleLabel.textColor = .red }
        }
    }
    
    deinit {
        deRegisterKeyboardNotification()
    }
    
}

// MARK: - UITextFieldDelegate

extension LoginVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case userNameTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            view.endEditing(true)
            signIn()
        default:
            assertionFailure("Invalid Text Field in Sign in screen")
        }
        
        return false
    }
    
    /*
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case userNameTextField:
            loginVM.userName = textField.text
        case passwordTextField:
            loginVM.password = textField.text
        default:
            assertionFailure("Invalid Text Field in Sign in screen")
        }
        
        let errors = loginVM.emptyFieldErrors
        
        switch textField {
        case userNameTextField:
            if errors.contains(.userNameMissing) { showError(LoginVM.ValidationError.userNameMissing)
            }
            userNameTitleLabel.textColor = errors.contains(.userNameMissing) ? .red : .white
        case passwordTextField:
            if errors.contains(.passwordMissing) { showError(LoginVM.ValidationError.passwordMissing) }
            passwordTitleLabel.textColor = errors.contains(.passwordMissing) ? .red : .white
        default:
            assertionFailure("Invalid Text Field in Sign in screen")
        }
        
        if errors.isEmpty {
            userNameTitleLabel.textColor = .white
            passwordTitleLabel.textColor = .white
            errorLabel.isHidden = true
            errorLabel.text = nil
        }
    }*/
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        _ = (textField.text ?? "").isEmpty
        switch textField {
        case userNameTextField:
            userNameTitleLabel.textColor = .white
        case passwordTextField:
            passwordTitleLabel.textColor =  .white
        default:
            assertionFailure("Invalid Text Field in Sign in screen")
        }
    }
    
    @IBAction private func textFieldEditingChange(_ textField: UITextField) {
        let isEmpty = (textField.text ?? "").isEmpty
        switch textField {
        case userNameTextField:
            userNameTitleLabel.textColor = isEmpty ? .red : .white
        case passwordTextField:
            passwordTitleLabel.textColor = isEmpty ? .red : .white
        default:
            assertionFailure("Invalid Text Field in Sign in screen")
        }
        
        guard !errorLabel.isHidden else { return }
        errorLabel.isHidden = !(userNameTextField.text ?? "").isEmpty && !(passwordTextField.text ?? "").isEmpty
    }
}


extension LoginVC {
    
    func keyboardWillShow(height: CGFloat, duration: TimeInterval, options: UIView.AnimationOptions) {
//        loginButtonBottomConstraint.constant = height
//        viewCenterContraints.constant = height - viewHeight.constant / 2 - 100
//        loginLabel.isHidden = true
    }

    func keyboardWillHide(duration: TimeInterval, options: UIView.AnimationOptions) {
//        loginButtonBottomConstraint.constant = 0
//        viewCenterContraints.constant = 0
//        loginLabel.isHidden = false
    }
    
}

