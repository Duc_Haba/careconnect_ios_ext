//
//  ForgotPasswordViewController.swift
//  CareConnect
//
//  Created by MAC on 06/08/21.
//  Copyright © 2021 Yml. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController, KeyboardNotificationProvider {

    @IBOutlet weak var loaderStackView: UIView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var userNameTitleLabel: UILabel!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    let loginVM = LoginVM()
    var reachability = Reachability()
    var showNotification: NSObjectProtocol?
    var hideNotification: NSObjectProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTextField.becomeFirstResponder()
        errorLabel.isHidden = true
        loaderStackView.isHidden = true
        setUpTextFields()
        registerKeyboardNotification()
        
    }
    
    func setUpTextFields() {
        userNameTextField.placeholder = "Enter Email ID"
        userNameTextField.placeHolderTextColor = .gray
    }
    
    private func showError(_ error: Error) {
        errorLabel.isHidden = false
        errorLabel.textColor = .red
        self.userNameTitleLabel.textColor = .red
        errorLabel.text = error.localizedDescription
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }

    
    @IBAction private func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func ForgotPassWordClicked(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ForgotPasswordViewController {
    
    func keyboardWillShow(height: CGFloat, duration: TimeInterval, options: UIView.AnimationOptions) {
        loginLabel.isHidden = true
    }
    
    func keyboardWillHide(duration: TimeInterval, options: UIView.AnimationOptions) {
        loginLabel.isHidden = false
    }
    
}

// MARK: - UITextFieldDelegate

extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case userNameTextField:
            loginVM.userName = textField.text
        default:
            assertionFailure("Invalid Text Field in Sign in screen")
        }
        
        let errors = loginVM.emptyFieldErrors
        
        switch textField {
        case userNameTextField:
            if errors.contains(.userNameMissing) { showError(LoginVM.ValidationError.userNameMissing)
            }
            userNameTitleLabel.textColor = errors.contains(.userNameMissing) ? .red : .white
        default:
            assertionFailure("Invalid Text Field in Sign in screen")
        }
        
        if errors.isEmpty {
            userNameTitleLabel.textColor = .white
            errorLabel.isHidden = true
            errorLabel.text = nil
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        _ = (textField.text ?? "").isEmpty
        switch textField {
        case userNameTextField:
            userNameTitleLabel.textColor = .white
        default:
            assertionFailure("Invalid Text Field in Sign in screen")
        }
    }
    
    @IBAction private func textFieldEditingChange(_ textField: UITextField) {
        let isEmpty = (textField.text ?? "").isEmpty
        switch textField {
        case userNameTextField:
            userNameTitleLabel.textColor = isEmpty ? .red : .white
        default:
            assertionFailure("Invalid Text Field in Sign in screen")
        }
        
        guard !errorLabel.isHidden else { return }
        errorLabel.isHidden = !(userNameTextField.text ?? "").isEmpty
    }
}
