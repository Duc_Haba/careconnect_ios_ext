//
//  LoginVM.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 18/06/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import Rudder

class LoginVM {
    
    enum ValidationError: Error, LocalizedError {
        case userNameMissing
        case passwordMissing
        
        var errorDescription: String? {
            return "Please fill in all fields".localized
        }
    }
    
    enum GenericError: Error, LocalizedError {
        case invalidCredentials
        case none
        
        var errorDescription: String? {
            return "Username or password is incorrect".localized
        }
    }
    
    var userName: String?
    var password: String?
    var userData = UserModel()
    var emptyFieldErrors: [LoginVM.ValidationError] {
        var errors: [LoginVM.ValidationError] = []
        if userName == nil || userName?.isEmpty == true {
            errors.append(.userNameMissing)
        }
        
        if password == nil || password?.isEmpty == true {
            errors.append(.passwordMissing)
        }
        
        return errors
    }
    
    func submit(completionHandler: @escaping (Bool, Error) -> Void) throws {
        guard let userName = userName, !userName.isEmpty else { throw ValidationError.userNameMissing }
        guard let password = password, !password.isEmpty else { throw ValidationError.passwordMissing }
        
        let headers = HeaderConfiguration.epicHeader.header
        let params = ["email": userName, "password": password]
        let path = Apiconstants.baseUrl + CareConnectURLs.login.rawValue
        guard let request = NetworkManager.requestForURL(path, method: .Post, params: params, headers: headers) else {
            return
        }
        
        _ = NetworkManager.request(request) { json, error, _ in
            
            if error == nil, let jsonData = json {
                if let response1 = try? JSONDecoder().decode(UserModel.self, from: (jsonData["data"].rawData())) {
                    RealmManager.shared.saveToRealm(objectData: response1)
                    self.userData = response1
                }
                guard let response = json?.dictionaryObject, let data = response["data"] as? [String: Any] else {
                    return
                }
                guard let token = data["token"] as? String, let twilioToken = data["twilioToken"] as? String, let virgileToken = data["virgilToken"] as? String, let email = data["email"] as? String, let id = data["id"] as? Int else {
                    return
                }
                
//                guard let token = data["token"] as? String, let twilioToken = data["twilioToken"] as? String, let virgileToken = data["virgilToken"] as? String, let email = data["email"] as? String else {
//                    return
//                }
                
                Defaults.shared.sessionToken = token
                Defaults.shared.twilioToken = twilioToken
                Defaults.shared.virgileToken = virgileToken
                Defaults.shared.isUserLoggedIn = true
                Defaults.shared.email = email
                Defaults.shared.selectedProfileID = id
                completionHandler(true, GenericError.none as Error)
                
            } else {
                completionHandler(false, GenericError.invalidCredentials as Error)
            }
        }
    }
}
