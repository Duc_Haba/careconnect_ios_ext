//
//  LoginDM.swift
//  CareConnect
//
//  Created by Venugopal S A on 20/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class UserModel: Object, Codable {
    dynamic var firstName: String?
    dynamic var id: Int = 0
    dynamic var lastName: String?
    dynamic var email: String?
    dynamic var imageUrl: String?
    dynamic var phoneNumber: String?
    dynamic var token: String?
    dynamic var twilioToken: String?
    dynamic var virgilToken: String?
    dynamic var dependents = List<Dependents>()

    override static func primaryKey() -> String? {
        return "id"
    }
    private enum CodingKeys: String, CodingKey {
        case firstName
        case lastName
        case email
        case imageUrl
        case phoneNumber
        case token
        case twilioToken
        case virgilToken
        case dependents
        case id
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        firstName = try container.decodeIfPresent(String.self, forKey: .firstName)
        lastName = try container.decodeIfPresent(String.self, forKey: .lastName)
        email = try container.decodeIfPresent(String.self, forKey: .email)
        imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl)
        phoneNumber = try container.decodeIfPresent(String.self, forKey: .phoneNumber)
        token = try container.decodeIfPresent(String.self, forKey: .token)
        twilioToken = try container.decodeIfPresent(String.self, forKey: .twilioToken)
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        virgilToken = try container.decodeIfPresent(String.self, forKey: .virgilToken)
        dependents = try container.decodeIfPresent(List<Dependents>.self, forKey: .dependents) ?? List<Dependents>()
    }
}

@objcMembers
class Dependents: Object, Codable {
    dynamic var id: Int = 0
    dynamic var name: String?
    dynamic var email: String?
    dynamic var race: String?
    dynamic var gender: String?
    dynamic var ethnicity: String?
    dynamic var address: String?
    dynamic var phoneNumber: String?
    dynamic var dob: String?
    dynamic var imageUrl: String?
    dynamic var relationship: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case email
        case imageUrl
        case race
        case gender
        case ethnicity
        case address
        case phoneNumber
        case dob
        case relationship
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try container.decodeIfPresent(String.self, forKey: .name)
        email = try container.decodeIfPresent(String.self, forKey: .email)
        imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl)
        phoneNumber = try container.decodeIfPresent(String.self, forKey: .phoneNumber)
        race = try container.decodeIfPresent(String.self, forKey: .race)
        gender = try container.decodeIfPresent(String.self, forKey: .gender)
        ethnicity = try container.decodeIfPresent(String.self, forKey: .ethnicity)
        address = try container.decodeIfPresent(String.self, forKey: .address)
        dob = try container.decodeIfPresent(String.self, forKey: .dob)
        relationship = try container.decodeIfPresent(String.self, forKey: .relationship)
    }
}

extension UserModel {
    class func storeInDataBase(userData: UserModel) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(userData, update: .all)
        }
    }
    
    func storeTeamDataToDB() {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(self, update: .all)
        }
    }
}
