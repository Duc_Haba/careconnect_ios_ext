//
//  phoneNumberCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 11/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class PhoneNumberCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var countryCodeTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    
    let countryPicker = CountryPicker()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        countryCodeTextfield.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 12, height: 6 ))
        let image = UIImage(named: "icnDropdownWhite")
        imageView.image = image
        countryCodeTextfield.rightView = imageView
        countryPicker.textField = countryCodeTextfield
        countryPicker.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension PhoneNumberCell: CountryPickerDelegate {
    
    func didSelectCountry(country: Country) {
       countryCodeTextfield.text = country.flagEmoji + country.phoneCode
        
    }
    
}
