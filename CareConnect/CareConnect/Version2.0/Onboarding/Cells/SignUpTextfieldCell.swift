//
//  SignUpTextfieldCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 11/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SignUpTextfieldCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var celltextfield: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        celltextfield.placeHolderTextColor = Color.placeHolderColor.uiColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
