//
//  Color.swift
//  CareConnect
//
//  Created by Y Media Labs on 06/09/18.
//  Copyright © 2018 Y Media Labs. All rights reserved.
//

import UIKit

enum Color {
    case appPrimaryColor
    case appointmetCancelledColor
    case appointmentCancelledTextColor
    case placeHolderColor
    case gradientStartColor
    case gradientEndcolor
    case grayBackgroundColor
    case appBlackTextColor
    case labTestValueNormalStartColor
    case labTestValueNormalEndColor
    case labTestValueLowColor
    case scaleMinorBarsColor
    case graphgradientStartColor
    case graphGradientEndColor
    case trackColor
    
    var uiColor: UIColor {
        var color: UIColor = .black
        switch self {
        case .appPrimaryColor:
            color = UIColor(red: 82 / 255, green: 74 / 255, blue: 237 / 255, alpha: 1.0)
        case .appointmetCancelledColor:
            color = UIColor(red: 234 / 255, green: 97 / 255, blue: 97 / 255, alpha: 1)
        case .appointmentCancelledTextColor:
            color = UIColor(red: 148 / 255, green: 147 / 255, blue: 149 / 255, alpha: 1)
        case .placeHolderColor:
            color = UIColor(red: 248 / 255, green: 248 / 255, blue: 248 / 255, alpha: 0.5)
        case .gradientStartColor:
            color = UIColor(red: 76 / 255, green: 103 / 255, blue: 255 / 255, alpha: 1)
        case .gradientEndcolor:
            color = UIColor(red: 86 / 255, green: 53 / 255, blue: 223 / 255, alpha: 1)
        case .grayBackgroundColor:
            color = UIColor(red: 242 / 255, green: 242 / 255, blue: 243 / 255, alpha: 1)
        case . appBlackTextColor:
            color = UIColor(red: 101 / 255, green: 101 / 255, blue: 101 / 255, alpha: 1)
        case .labTestValueNormalStartColor:
            color = UIColor(red: 0 / 255, green: 232 / 255, blue: 214 / 255, alpha: 1)
        case .labTestValueNormalEndColor:
            color = UIColor(red: 0 / 255, green: 207 / 255, blue: 191 / 255, alpha: 1)
        case .labTestValueLowColor:
            color = UIColor(red: 255 / 255, green: 75 / 255, blue: 156 / 255, alpha: 1)
        case .scaleMinorBarsColor:
            color = UIColor(red: 76 / 255, green: 75 / 255, blue: 95 / 255, alpha: 1)
        case .graphgradientStartColor:
            color = UIColor(red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 0.3)
        case .graphGradientEndColor:
            color = UIColor(red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 0)
        case .trackColor:
            color = UIColor(red: 241 / 255, green: 241 / 255, blue: 241 / 255, alpha: 1.0)
        }
        return color

    }

    var cgColor: CGColor { return uiColor.cgColor }
}
