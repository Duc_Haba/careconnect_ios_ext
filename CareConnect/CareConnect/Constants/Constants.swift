//
//  Constants.swift
//  CareConnect
//
//  Created by Y Media Labs on 05/09/18.
//  Copyright © 2018 Y Media Labs. All rights reserved.
//

import UIKit

let appDelegate = UIApplication.shared.delegate as? AppDelegate
let dateFormatter = DateFormatter()

extension CGFloat {
    var f: Double {
        return Double(self)
    }
}

struct NotificationConstants {
    
    static let launchNotification = "SampleBitLaunchNotification"
}

struct FitBitConstants {
    
    static let clientID = "22DRH4"
    static let defaultScope = "sleep+settings+nutrition+activity+social+heartrate+profile+weight+location"
    static let clientSecret = "b7833ef9fb2d4ef3fc21c9a3f00f470d"
    static let authoriseURL = " https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=22DRH4&redirect_uri=myapp%3A%2F%2Ffitbit&scope=activity%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight&expires_in=604800"
    static let tokenURL = "https://api.fitbit.com/oauth2/token"
    static let stepsAndCaloriesURL = "https://api.fitbit.com/1/user/-/activities/date/"
    static let sleepURL = "https://api.fitbit.com/1.2/user/-/sleep/date/"
    static let heartRateURL = "https://api.fitbit.com/1/user/-/activities/heart/date/today/1d.json"
    static let sleepGoalURL = "https://api.fitbit.com/1/user/-/sleep/goal.json"
}

enum EpicURLs: String {
    
    case conditions = "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Condition?patient=Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
    case profile = "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Patient/Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
    case allergies = "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/AllergyIntolerance?patient=Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
    case immunization = "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Immunization?patient=Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
    case resport = "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/DiagnosticReport?patient=Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB"
}

enum CareConnectURLs: String {
    
//    case getAppointments = "careconnect/appointments"
//    case getDoctors = "careconnect/doctors"
//    case getSymptoms = "careconnect/symptoms"
//    case getSlots = "careconnect/available-slots"
//    case createAppointment = "careconnect/appointment"
//    case login = "careconnect/login"
//    case logout = "careconnect/logout"
//    case getLabResults = "careconnect/labresults"
//    case getLabResultDetaiils = "careconnect/labresult-details"
//    case shimmerAuthorization = "authorize/"
//    case medications = "careconnect/medications"
//    case medication = "careconnect/medication"
//    case allergies = "careconnect/allergies"
//    case conditions = "careconnect/conditions"
//    case immunizations = "careconnect/immunizations"
    
    case getAppointments = "appointments"
    case getDoctors = "doctors"
    case getSymptoms = "symptoms"
    case getSlots = "available-slots"
    case createAppointment = "appointment"
    case login = "login"
    case logout = "logout"
    case getLabResults = "labresults"
    case getLabResultDetaiils = "labresult-details"
    case shimmerAuthorization = ""
    case medications = "medications"
    case medication = "medication"
    case allergies = "allergies"
    case conditions = "conditions"
    case immunizations = "immunizations"
}

struct Apiconstants {
//    https://careconnect-api.ymedia.in/
//    static let baseUrl = "http://198.251.72.91/"
    static let baseUrl = "http://198.251.72.91/cc-prod/"
    static let shimmerUrl = "http://careconnect-shimmer.ymedia.in/"
}

struct ApiKeys {
    static let uppcomingAppointments = "upcomingAppointments"
    static let pastAppoinments = "pastAppointments"
    static let name = "name"
    static let address = "address"
    static let specialisation = "specialization"
    static let doctorId = "doctorId"
    static let imageUrl = "imageUrl"
}
