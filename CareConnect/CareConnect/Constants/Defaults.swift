//
//  Defaults.swift
//  CareConnect
//
//  Created by Y Media Labs on 05/09/18.
//  Copyright © 2018 Y Media Labs. All rights reserved.
//

import UIKit

final class Defaults {
    private let userLoggedIn = "userLoggedIn"
    private let updateDashboard = "shouldUpdateDashboard"
    private let dependent = "dependent"
    private let fitbitLoggedIn = "fitBitLoggedIn"
    private let fitBitRefreshToken = "refreshToken"
    private let userToken = "token"
    private let userprofile = "selectedProfileID"
    private let userID = "id"
    private let userTwilioToken = "twiolioToken"
    private let userVirgileToken = "virgileToken"
    private let userEmail = "userToken"
    private let firstName = "firstName"
    private let step = "step"
    private let heartRate = "heartRate"
    private let sleep = "sleep"
    private let connectedDeviceType = "connectedDeviceType"
    
    static var shared: Defaults {
        return Defaults()
    }
    
    var isUserLoggedIn: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: userLoggedIn)
        }
        
        get {
            if UserDefaults.standard.value(forKey: userLoggedIn) == nil {
                UserDefaults.standard.set(false, forKey: userLoggedIn)
            }
            return UserDefaults.standard.bool(forKey: userLoggedIn)
        }
    }
    
    var shouldUpdateDashboard: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: updateDashboard)
        }
        
        get {
            if UserDefaults.standard.value(forKey: updateDashboard) == nil {
                UserDefaults.standard.set(true, forKey: updateDashboard)
            }
            return UserDefaults.standard.bool(forKey: updateDashboard)
        }
    }
    
    var twilioToken: String {
        set {
            UserDefaults.standard.set(newValue, forKey: userTwilioToken)
        }
        
        get {
            if UserDefaults.standard.value(forKey: userTwilioToken) == nil {
                UserDefaults.standard.set("", forKey: userTwilioToken)
            }
            return UserDefaults.standard.string(forKey: userTwilioToken) ?? ""
        }
    }
    
    var email: String {
        set {
            UserDefaults.standard.set(newValue, forKey: userEmail)
        }
        
        get {
            if UserDefaults.standard.value(forKey: userEmail) == nil {
                UserDefaults.standard.set("", forKey: userEmail)
            }
            return UserDefaults.standard.string(forKey: userEmail) ?? ""
        }
    }
    
    var userName: String {
        set {
            UserDefaults.standard.set(newValue, forKey: firstName)
        }
        
        get {
            if UserDefaults.standard.value(forKey: firstName) == nil {
                UserDefaults.standard.set("", forKey: firstName)
            }
            return UserDefaults.standard.string(forKey: firstName) ?? ""
        }
    }
    
    var isDepedentUser: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: dependent)
        }
        
        get {
            if UserDefaults.standard.value(forKey: dependent) == nil {
                UserDefaults.standard.set(false, forKey: dependent)
            }
            return UserDefaults.standard.bool(forKey: dependent)
        }
    }
    
    var virgileToken: String {
        set {
            UserDefaults.standard.set(newValue, forKey: userVirgileToken)
        }
        
        get {
            if UserDefaults.standard.value(forKey: userVirgileToken) == nil {
                UserDefaults.standard.set("", forKey: userVirgileToken)
            }
            return UserDefaults.standard.string(forKey: userVirgileToken) ?? ""
        }
    }
    
    var sessionToken: String {
        set {
            UserDefaults.standard.set(newValue, forKey: userToken)
        }
        
        get {
            if UserDefaults.standard.value(forKey: userToken) == nil {
                UserDefaults.standard.set("", forKey: userToken)
            }
            return UserDefaults.standard.string(forKey: userToken) ?? ""
        }
    }
    
    var isFitBitLoggedIn: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: fitbitLoggedIn)
        }
        get {
            if UserDefaults.standard.value(forKey: fitbitLoggedIn) == nil {
                UserDefaults.standard.set(false, forKey: fitbitLoggedIn)
            }
            return UserDefaults.standard.bool(forKey: fitbitLoggedIn)
        }
    }
    
    var refreshToken: String {
        set {
            UserDefaults.standard.set(newValue, forKey: fitBitRefreshToken)
        }
        get {
            if UserDefaults.standard.value(forKey: fitBitRefreshToken) == nil {
                UserDefaults.standard.set("", forKey: fitBitRefreshToken)
            }
            return UserDefaults.standard.string(forKey: fitBitRefreshToken) ?? ""
        }
    }
    
    var selectedProfileID: Int {
        set {
            UserDefaults.standard.set(newValue, forKey: userprofile)
        }
        get {
            if UserDefaults.standard.value(forKey: userprofile) == nil {
                UserDefaults.standard.set(0, forKey: userprofile)
            }
            return UserDefaults.standard.integer(forKey: userprofile)
        }
    }
    
    var id: Int {
        set {
            UserDefaults.standard.set(newValue, forKey: userID)
        }
        get {
            if UserDefaults.standard.value(forKey: userID) == nil {
                UserDefaults.standard.set(0, forKey: userID)
            }
            return UserDefaults.standard.integer(forKey: userID)
        }
    }
    
    var conncetedDeviceType: Int {
        set {
            UserDefaults.standard.set(newValue, forKey: connectedDeviceType)
        }
        get {
            if UserDefaults.standard.value(forKey: connectedDeviceType) == nil {
                UserDefaults.standard.set(3, forKey: connectedDeviceType)
            }
            return UserDefaults.standard.integer(forKey: connectedDeviceType)
        }
    }
    
}
