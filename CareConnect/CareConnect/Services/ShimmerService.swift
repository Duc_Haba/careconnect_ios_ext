//
//  ShimmerService.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 06/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Rudder
import RealmSwift

enum ShimType: Int {
    case fitbit = 0
    case healthKit
    case googleFit
    case none
    
    var title: String {
        var text = ""
        switch self {
        case .fitbit:
            text = "FitBit"
        case .healthKit:
            text = "Health Kit"
        case .googleFit:
            text = "Google Fit"
        default:
            text = ""
        }
        return text
    }
    
    var key: String {
        var text = ""
        switch self {
        case .fitbit:
            text = "fitbit"
        case .healthKit:
            text = "healthkit"
        case .googleFit:
            text = "googlefit"
        default:
            text = ""
        }
        return text
    }
}

enum ShimActivityType {
    case steps
    case heartRate
    case sleep
}

class ShimmerService {
    
    struct TableModel {
        var shimType: ShimType = .fitbit
        var isConnected: Bool = false
    }
    
    var authenticationUrl: String = ""
    var steps: [StepsModel] = []
    var heartRates: [HeartRateModel] = []
    var sleepData: [SleepModel] = []
    var model: [TableModel] = [TableModel]()
    var connectedShim: ShimType = .none
    var deviceIntegrated = false {
        didSet {
            if deviceIntegrated {
                Defaults.shared.conncetedDeviceType = connectedShim.rawValue
            } else {
                Defaults.shared.conncetedDeviceType = ShimType.none.rawValue
            }
        }
    }

    func updateModel() {
        connectedShim = ShimType(rawValue: Defaults.shared.conncetedDeviceType) ?? .none
        if connectedShim != .none {
            deviceIntegrated = true
        }
        model = []
        model.append(TableModel(shimType: .fitbit, isConnected: connectedShim == .fitbit))
        model.append(TableModel(shimType: .healthKit, isConnected: connectedShim == .healthKit))
        model.append(TableModel(shimType: .googleFit, isConnected: connectedShim == .googleFit))
    }
    
    func authorizeService(with shimType: ShimType, completion: @escaping CompletionBlock) {
        let headers = HeaderConfiguration.defaultHeader.header
        let baseUrl = Apiconstants.shimmerUrl + CareConnectURLs.shimmerAuthorization.rawValue
        let appendUrl = shimType.key + "?username=" + "\(Defaults.shared.id)"
        let path = baseUrl + appendUrl
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }

        _ = NetworkManager.request(request) {[weak self] json, error, _ in
            
            if error == nil {
                
                guard let response = json?.dictionaryObject else {
                    return
                }
                if let isAuthorized = response["isAuthorized"] as? Int {
                    if isAuthorized == 1 {
                        self?.model[shimType.rawValue].isConnected = true
                    } else {
                        if let authUrl = response["authorizationUrl"] as? String {
                            self?.model[shimType.rawValue].isConnected = false
                            self?.authenticationUrl = authUrl
                        }
                    }
                    completion(true)
                } else {
                    completion(false)
                }
            } else {
                completion(false)
            }
        }
    }
    
    func fetchSteps(with shimType: ShimType, fromDate: Date, toDate: Date, completion: @escaping CompletionBlock) {
        let headers = HeaderConfiguration.defaultHeader.header
        let path = formPath(with: shimType, fromDate: fromDate, toDate: toDate, apiKey: "step_count")
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        
        _ = NetworkManager.request(request) { json, error, _ in
            if error == nil, let json = json {
                if let response = try? JSONDecoder().decode([StepsModel].self, from: json["body"]["activities-steps"].rawData()) {
                    StepsModel.storeInDataBase(stepData: response)
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
    }
    
    func fetchSleepData(with shimType: ShimType, fromDate: Date, toDate: Date, completion: @escaping CompletionBlock) {
        let headers = HeaderConfiguration.defaultHeader.header
        let path = formPath(with: shimType, fromDate: fromDate, toDate: toDate, apiKey: "sleep_duration")
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        
        _ = NetworkManager.request(request) { json, error, _ in
            if error == nil, let json = json {
                    if let response = try? JSONDecoder().decode([SleepModel].self, from: json["body"]["sleep"].rawData()) {
                    SleepModel.storeInDataBase(sleepData: response)
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
    }
    
    func fetchHearRateData(with shimType: ShimType, fromDate: Date, toDate: Date, completion: @escaping CompletionBlock) {
        let headers = HeaderConfiguration.defaultHeader.header
        let path = formPath(with: shimType, fromDate: fromDate, toDate: toDate, apiKey: "heart_rate")
        guard let request = NetworkManager.requestForURL(path, method: .Get, params: [:], headers: headers) else {
            return
        }
        
        _ = NetworkManager.request(request) { json, error, _ in
            if error == nil, let json = json {
                if let response = try? JSONDecoder().decode([HeartRateModel].self, from: json["body"]["activities-heart"].rawData()) {
                    HeartRateModel.storeInDataBase(heartRateData: response)
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
    }
    
    private func formPath(with shimType: ShimType, fromDate: Date, toDate: Date, apiKey: String) -> String {
        let baseUrl = Apiconstants.shimmerUrl + "data/" + shimType.key
        let appendUrl = "/\(apiKey)?username=" + "\(Defaults.shared.id)"
        let from = fromDate.string(format: dateFormat)
        let to = toDate.string(format: dateFormat)
        let params = "&normalize=false&dateStart=" + from + "&dateEnd=" + to
        let path = baseUrl + appendUrl + params
        return path
    }
    
    func clearHealthData() {
        RealmManager.shared.deleteObjects(object: SleepModel.self)
        RealmManager.shared.deleteObjects(object: SleepMicroModel.self)
        RealmManager.shared.deleteObjects(object: HeartRateModel.self)
        RealmManager.shared.deleteObjects(object: HeartRateZone.self)
        RealmManager.shared.deleteObjects(object: StepsModel.self)
    }
}
