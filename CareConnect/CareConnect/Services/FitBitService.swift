//
//  AuthenticationController.swift
//  SampleBit
//
//  Created by Venugopal on 9/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import SafariServices
import Foundation
import Rudder

protocol AuthenticationProtocol: class {
    func authorizationDidFinish(_ success: Bool)
}

class FitBitService: NSObject, SFSafariViewControllerDelegate {
    
    enum TokenType {
        case refreshToken
        case authToken
    }

    static let clientID = "22DRH4"
    let clientSecret = "b7833ef9fb2d4ef3fc21c9a3f00f470d"
    let baseURL = URL(string: "https://www.fitbit.com/oauth2/authorize")
    static let redirectURI = "yemdia://fitbit/auth"
    let defaultScope = "sleep+settings+nutrition+activity+social+heartrate+profile+weight+location"
    
    var authorizationVC: SFSafariViewController?
    weak var delegate: AuthenticationProtocol?
    var authenticationToken: String?
    static var accessToken: String?
    
    init(delegate: AuthenticationProtocol?) {
        self.delegate = delegate
        super.init()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: NotificationConstants.launchNotification), object: nil, queue: nil, using: { [weak self] (notification: Notification) in
            // Parse and extract token
            if let token = FitBitService.extractToken(notification, key: "myapp://fitbit?code") {
                self?.authenticationToken = token
                NSLog("You have successfully authorized")
            } else {
                print("There was an error extracting the access token from the authentication response.")
            }
            
            self?.authorizationVC?.dismiss(animated: true, completion: {
                guard let token = self?.authenticationToken else {
                    return
                }
                self?.getAccessToken(tokenType: .authToken, token: token, callback: { _ in
                    //self?.delegate?.authorizationDidFinish(success)
                })
            })
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Public API
    
    public func login(fromParentViewController viewController: UIViewController) {
        guard let url = URL(string: "https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=22DRH4&redirect_uri=myapp%3A%2F%2Ffitbit&scope=activity%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight&expires_in=604800") else {
            NSLog("Unable to create authentication URL")
            return
        }
        
        let authorizationViewController = SFSafariViewController(url: url)
        authorizationViewController.delegate = self
        authorizationVC = authorizationViewController
        viewController.present(authorizationViewController, animated: true, completion: nil)
    }
    
    public static func logout() {
        
    }
    
    private static func extractToken(_ notification: Notification, key: String) -> String? {
        guard let url = notification.userInfo?[UIApplication.LaunchOptionsKey.url] as? URL else {
            NSLog("notification did not contain launch options key with URL")
            return nil
        }
        
        // Extract the access token from the URL
        let strippedURL = url.absoluteString.replacingOccurrences(of: FitBitService.redirectURI, with: "")
        return self.parametersFromQueryString(strippedURL)[key]
    }
    
    private static func parametersFromQueryString(_ queryString: String?) -> [String: String] {
        var parameters = [String: String]()
        if let url = queryString {
            let parameterScanner: Scanner = Scanner(string: url)
            var name: NSString? = nil
            var value: NSString? = nil
            while parameterScanner.isAtEnd != true {
                name = nil
                parameterScanner.scanUpTo("=", into: &name)
                parameterScanner.scanString("=", into: nil)
                value = nil
                parameterScanner.scanUpTo("&", into: &value)
                parameterScanner.scanString("&", into: nil)
                let val = value?.replacingOccurrences(of: "#_=_", with: "")
                
                if let nameKey = name, let value = val {
                    guard let key = nameKey.removingPercentEncoding else {
                        return parameters
                    }
                    parameters[key] = value.removingPercentEncoding
                }
            }
        }
        return parameters
    }
    
    // MARK: SFSafariViewControllerDelegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        delegate?.authorizationDidFinish(false)
    }
    
    func getAccessToken(tokenType: TokenType, token: String, callback: @escaping (_ success: Bool) -> Void) {
        
        let data: Data?
        switch tokenType {
        case .authToken:
            guard let urlData: Data = "clientId=\(FitBitConstants.clientID)&grant_type=authorization_code&code=\(token)&redirect_uri=myapp://fitbit".data(using: .utf8) else {
                return
            }
            data = urlData
        case .refreshToken:
            guard let urlData: Data = "grant_type=refresh_token&refresh_token=\(token)".data(using: .utf8) else {
                return
            }
            data = urlData
        }
       
        let headers = HeaderConfiguration.tempToken.header
        guard let request = NetworkManager.requestForURL(FitBitConstants.tokenURL, method: .Post, params: data, headers: headers) else {
            return
        }
        _ = NetworkManager.request(request) { [weak self] response, error, _ in
            if error == nil {
                guard let data = response else {
                    return
                }
                guard let response = data.dictionaryObject, let token = response["access_token"] as? String, let refreshToken = response["refresh_token"] as? String else {
                    return
                }
                Defaults.shared.refreshToken = refreshToken
                FitBitService.accessToken = token
                self?.delegate?.authorizationDidFinish(true)
                callback(true)
            } else {
                callback(false)
            }
        }
    }
}
