//
//  UserProfile.swift
//  HealthApp
//
//  Created by Arun on 01/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import HealthKit

//////////////////
//error enum
//////////////////

private enum HealthkitSetupError: Error {
    case notAvailableOnDevice
    case dataTypeNotAvailable
}

class HealthKitAssistant {
    
    ///////////////////////////
    //Shared Variable
    ///////////////////////////
    
    static let shared = HealthKitAssistant()
    
    ///////////////////////////
    //Healthkit store object
    ///////////////////////////
    
    var healthKitStore = HKHealthStore()

    ////////////////////////////////////
    // MARK: Permission block
    ////////////////////////////////////
    func getHealthKitPermission(completion: @escaping (Bool) -> Void) {
        
        guard HKHealthStore.isHealthDataAvailable(), let stepsCount = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount), let heartRate = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate), let sleepData = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis) else {
            return
        }
        
        self.healthKitStore.requestAuthorization(toShare: [stepsCount, heartRate, sleepData], read: [stepsCount, heartRate, sleepData]) { success, error in
            if success {
                completion(true)
            } else {
                if error != nil {
                    print(error ?? "")
                }
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    //////////////////////////////////////////////////////
    // MARK: - Get Recent step Data
    //////////////////////////////////////////////////////
    
    func getMostRecentStep(for sampleType: HKQuantityType, completion: @escaping (_ stepRetrieved: Int, _ stepAll: [[String: String]]) -> Void) {
        
        // Use HKQuery to load the most recent samples.
        let mostRecentPredicate = HKQuery.predicateForSamples(withStart: Date.distantPast, end: Date(), options: .strictStartDate)
        
        var interval = DateComponents()
        interval.day = 1
        
        let stepQuery = HKStatisticsCollectionQuery(quantityType: sampleType, quantitySamplePredicate: mostRecentPredicate, options: .cumulativeSum, anchorDate: Date.distantPast, intervalComponents: interval)
        
        stepQuery.initialResultsHandler = { query, results, error in
            
            if error != nil {
                //  Something went Wrong
                return
            }
            if let myResults = results {
                
                var stepsData: [[String: String]] = [[:]]
                var steps: Int = Int()
                stepsData.removeAll()
                
                myResults.enumerateStatistics(from: Date.distantPast, to: Date()) {
                    
                    statistics, stop in
                    
                    //Take Local Variable
                    
                    if let quantity = statistics.sumQuantity() {
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MMM d, yyyy"
                        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
                        dateFormatter.timeZone = NSTimeZone.local
                        
                        var tempDic: [String: String]?
                        let endDate: Date = statistics.endDate
                        
                        steps = Int(quantity.doubleValue(for: HKUnit.count()))
                        
                        print("DataStore Steps = \(steps)")
                        
                        tempDic = ["enddate": "\(dateFormatter.string(from: endDate))", "steps": "\(steps)"]
                        stepsData.append(tempDic ?? [:])
                    }
                }
                completion(steps, stepsData.reversed())
            }
        }
        healthKitStore.execute(stepQuery)
    }
    
    //////////////////////////////////////////////////////
    // MARK: - Get Recent Heart Rate
    //////////////////////////////////////////////////////
    
    func getRecentHeartRate(for sampleType: HKQuantityType, completion: @escaping (_ heartRate: Int, _ allHeartRates: [[String: String]]) -> Void) {
        
        // Creating the sample for the heart rate
        guard let sampleType: HKSampleType =
            HKObjectType.quantityType(forIdentifier: .heartRate) else {
                return
        }
        
        /// Creating an observer, so updates are received whenever HealthKit’s
        // heart rate data changes.
        _ = HKObserverQuery(sampleType: sampleType, predicate: nil) { [weak self] _, _, error in
                guard error == nil else {
                    return
                }
                
                /// When the completion is called, an other query is executed
                /// to fetch the latest heart rate
                self?.fetchLatestHeartRateSample(completion: { sample in
                    guard let sample = sample else {
                        return
                    }
                    
                    /// The completion in called on a background thread, but we
                    /// need to update the UI on the main.
                    DispatchQueue.main.async {
                        
                        /// Converting the heart rate to bpm
                        let heartRateUnit = HKUnit(from: "count/min")
                        let heartRate = sample
                            .quantity
                            .doubleValue(for: heartRateUnit)
                        
                        /// Updating the UI with the retrieved value
                        print("\(Int(heartRate))")
                    }
                })
        }
    }
    
    func fetchLatestHeartRateSample(
        completion: @escaping (_ sample: HKQuantitySample?) -> Void) {
        
        /// Create sample type for the heart rate
        guard let sampleType = HKObjectType
            .quantityType(forIdentifier: .heartRate) else {
                completion(nil)
                return
        }
        
        /// Predicate for specifiying start and end dates for the query
        let predicate = HKQuery
            .predicateForSamples(
                withStart: Date.distantPast,
                end: Date(),
                options: .strictEndDate)
        
        /// Set sorting by date.
        let sortDescriptor = NSSortDescriptor(
            key: HKSampleSortIdentifierStartDate,
            ascending: false)
        
        /// Create the query
        let query = HKSampleQuery(sampleType: sampleType, predicate: predicate, limit: Int(HKObjectQueryNoLimit), sortDescriptors: [sortDescriptor]) { _, results, error in
                
                guard error == nil else {
                    return
                }
                
                completion(results?[0] as? HKQuantitySample)
        }
        
        healthKitStore.execute(query)
    }
    
    func saveMockHeartData() {
        // 1. Create a heart rate BPM Sample
        if let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate) {
            let heartRateQuantity = HKQuantity(unit: HKUnit(from: "count/min"),
                                               doubleValue: Double(arc4random_uniform(80) + 100))
            let heartSample = HKQuantitySample(type: heartRateType,
                                               quantity: heartRateQuantity, start: Date(), end: Date())
            
            // 2. Save the sample in the store
            HKHealthStore().save(heartSample, withCompletion: { success, error -> Void in
                if let error = error {
                    print("Error saving heart sample: \(error.localizedDescription)")
                }
            })
        }
       
    }
   
}
