//
//  UIImageView+Helper.swift
//  Careconnect
//
//  Created by Y Media Labs on 26/09/18.
//  Copyright © 2018 Y Media Labs. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func animate(name: String, range: (Int, Int)) {
        let imagesArray = fetchImages(name: name, range: (range.0, range.1))
        backgroundColor = .clear
        tintColor = .clear
        contentMode = .center
        animationImages = imagesArray
        animationRepeatCount = 0
        startAnimating()
    }
    
    private func fetchImages(name: String, range: (start: Int, end: Int)) -> [UIImage] {
        var images: [UIImage] = []
        for i in range.start...range.end {
            guard let image = UIImage(named: "\(name)\(i - 1)") else { continue }
            images.append(image)
        }
        return images
    }
}
