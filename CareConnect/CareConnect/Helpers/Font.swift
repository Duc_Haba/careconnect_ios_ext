//
//  UIView+ReusableView.swift
//  CareConnect
//
//  Created by Y Media Labs on 12/11/16.
//  Copyright © 2017 Y Media Labs. All rights reserved.
//

import UIKit

enum Font {
    case maaxRegular
    case maaxBold
    case maaxMedium
    
    func getFont(with size: CGFloat) -> UIFont {
        var font: UIFont? = .systemFont(ofSize: size)
        switch self {
        case .maaxRegular:
            font = UIFont(name: "Sans-Regular", size: size)
        case .maaxBold:
            font = UIFont(name: "Sans-Bold", size: size)
        case .maaxMedium:
            font = UIFont(name: "Sans-Medium", size: size)
        }
        return font ?? .systemFont(ofSize: size)
    }
}

extension UIView {
    func setGradientBackground(cornerRadius: CGFloat, startColor: CGColor = Color.gradientStartColor.cgColor, endColor: CGColor = Color.gradientEndcolor.cgColor) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [startColor, endColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.frame = self.bounds
        gradientLayer.cornerRadius = cornerRadius
        
        if let layer = self.layer.sublayers?[0] as? CAGradientLayer {
            layer.removeFromSuperlayer()
        }
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
