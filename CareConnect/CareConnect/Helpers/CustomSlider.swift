//
//  CustomSlider.swift
//  CareConnect
//
//  Created by Venugopal S A on 13/08/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomSlider: UISlider {
    @IBInspectable var thickness: CGFloat = 20 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var sliderThumbImage: UIImage? {
        didSet {
            setup()
        }
    }
    
    func setup() {
        let minTrackStartColor = UIColor(red: 0.3, green: 0.4, blue: 1, alpha: 1)
        let minTrackEndColor = UIColor(red: 0.34, green: 0.21, blue: 0.87, alpha: 1)
        let maxTrackColor = UIColor(red: 0.78, green: 0.78, blue: 0.83, alpha: 1)
        do {
            self.setMinimumTrackImage(try self.gradientImage(
                size: self.trackRect(forBounds: self.bounds).size,
                colorSet: [minTrackStartColor.cgColor, minTrackEndColor.cgColor]),
                                      for: .normal)
            self.setMaximumTrackImage(try self.gradientImage(
                size: self.trackRect(forBounds: self.bounds).size,
                colorSet: [maxTrackColor.cgColor, maxTrackColor.cgColor]),
                                      for: .normal)
            self.setThumbImage(sliderThumbImage, for: .normal)
        } catch {
            self.minimumTrackTintColor = minTrackStartColor
            self.maximumTrackTintColor = maxTrackColor
        }
    }
    
    func gradientImage(size: CGSize, colorSet: [CGColor]) throws -> UIImage? {
        let tgl = CAGradientLayer()
        tgl.frame = CGRect(x: 0, y:0, width:size.width, height: size.height)
        tgl.cornerRadius = tgl.frame.height / 2
        tgl.masksToBounds = false
        tgl.colors = colorSet
        tgl.startPoint = CGPoint(x: 0.0, y: 0.5)
        tgl.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        UIGraphicsBeginImageContextWithOptions(size, tgl.isOpaque, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        tgl.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets:
                UIEdgeInsets(top: 0, left: size.height, bottom: 0, right: size.height))
        UIGraphicsEndImageContext()
        return image ?? UIImage()
    }
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(
            x: bounds.origin.x,
            y: bounds.origin.y,
            width: bounds.width,
            height: thickness
        )
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
}
