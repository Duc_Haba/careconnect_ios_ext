//
//  RealmManager.swift
//  CareConnect
//
//  Created by Y Media Labs on 31/08/18.
//  Copyright © 2018 Y Media Labs. All rights reserved.
//

import Foundation
import RealmSwift

final class RealmManager {
    
    // Shared instance
    public static var shared: RealmManager {
        return RealmManager()
    }
    
    static func getRealm() -> Realm? {
        do {
            let realm = try Realm()
            realm.refresh()
            return realm
        } catch {
            if let fileUrl = Realm.Configuration.defaultConfiguration.fileURL {
                do {
                    try FileManager.default.removeItem(at: fileUrl)
                } catch {
                    print(error.localizedDescription)
                }
            }
            print(error.localizedDescription)
            return nil
        }
    }
    
    let realm = getRealm()
    
    func refreshRealm() {
        realm?.refresh()
    }
    
    func saveToRealm<T: Object>(objectData: T) {
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(objectData, update: .all)
            }
        } catch let error {
            print(error)
        }
    }
    
    
    // Save realm objects to realm
    func save(objects: [Object], update: Bool = true) {
        do {
            try realm?.write {
                realm?.add(objects, update: .all)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    // Clear realm DB
    func resetRealmData() {
        do {
            try realm?.write {
                realm?.deleteAll()
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    // Delete realm objects from realm
    func deleteObject(object: Object) {
        do {
            try realm?.write {
                realm?.delete(object)
            }
        } catch {
            print(error.localizedDescription)
        }

    }
    
    // Delete realm objects from realm
    func deleteObjects<T>(object: T.Type) where T: Object {
        guard let realm = realm else { return }
        do {
            try realm.write {
                let objects = realm.objects(object)
                realm.delete(objects)
            }
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    //Returs an array as Results<object>?
    func getObjects<T>(type: T.Type) -> [T] where T: Object {
        guard let realm = realm else { return [] }
        realm.refresh()
        return Array(realm.objects(type))
    }
    
    func commitWrite() {
        try? realm?.commitWrite()
    }
    
    func getObjects<T>(type: T.Type, with primaryKey: Any) -> T? where T: Object {
        realm?.refresh()
        if let object = realm?.object(ofType: type, forPrimaryKey: primaryKey) {
            return object
        }
        return nil
    }
}
