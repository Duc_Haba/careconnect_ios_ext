//
//  TextfieldExtension.swift
//  CareConnect
//
//  Created by Venugopal S A on 11/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    @IBInspectable var placeHolderTextColor: UIColor? {
        set {
            
            let placeholderText = self.placeholder != nil ? self.placeholder! : ""
            attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.foregroundColor: newValue!])
        }
        get {
            return self.placeHolderTextColor
        }
    }
}
