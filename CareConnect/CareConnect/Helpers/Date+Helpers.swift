//
//  Date+Helpers.swift
//  CareConnect
//
//  Created by Y Media Labs on 19/09/18.
//  Copyright © 2018 Y Media Labs. All rights reserved.
//

import UIKit

let dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
let newDateTimeFormat = "yyyy-MM-ddTHH:mm:ss.000+0000"
let dateFormat = "yyyy-MM-dd"
let calendarDateFormat = "EEE, MMM d"
let chatDateFormat = "EEEE, MMM d"
let prescriptionDateFormat = "d MMM yyyy"
let labResultsDateFormate = "dd/MM/yyyy"
let weekdayFormat = "EEE, MMM d"
let genericTimeFormat = "hh:mm a"
let slotDateFormat = "MMM d, hh:mm a"
let graphDateFormate = "MMM dd"

extension Date {
    
    var previousDay: Date? {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon ?? Date())
    }
    var nextDay: Date? {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon ?? Date())
    }
    var noon: Date? {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)
    }
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    var isLastDayOfMonth: Bool {
        return nextDay?.month != month
    }
    
    func isSameDate(_ comparisonDate: Date) -> Bool {
        let order = Calendar.current.compare(self, to: comparisonDate, toGranularity: .day)
        return order == .orderedSame
    }
    
    func isLessThan(_ comparisonDate: Date) -> Bool {
        let order = Calendar.current.compare(self, to: comparisonDate, toGranularity: .minute)
        return order == .orderedAscending
    }
    
    //  This method converts the Time interval value to Date by using timeIntervalSince1970.
    public static func getDateFromTimeInterval(_ date: TimeInterval) -> Date {
        return (Date(timeIntervalSince1970: date / 1000))
    }
    
    func currentTimeInMiliseconds() -> Double {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return Double(since1970 * 1000 * 1000)
    }
    
   static func getCurrentDate(format: String = dateFormat) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    
    static func getDateStringFromTimeStamp (_ date: TimeInterval, format: String = dateFormat) -> String {
        let dateValue = self.getDateFromTimeInterval(date)
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: dateValue)
    }
    
    static func getStringFromDateString (_ dateString: String, format: String = dateFormat) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.dateFormat = dateTimeFormat
        let newFormatter = DateFormatter()
        newFormatter.dateFormat = format
        if let date = dateFormatter.date(from: dateString.replacingOccurrences(of: ".000+0000", with: "")) {
            return newFormatter.string(from: date)
        } else {
            return nil
        }
    }
    
    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        dates.append(date)
        return dates
    }
    
    func getTimeDiffrenceInMinutes(date: Date?) -> Int {
        let date1 = self
        let fitBitUpdatedDate = date
        guard let date2 = fitBitUpdatedDate else {
            return 0
        }
        let diff = Int(date1.timeIntervalSince1970 - date2.timeIntervalSince1970)
        
        let hours = diff / 3600
        var minutes = (diff - hours * 3600) / 60
        if diff > 0 && minutes == 0 {
            minutes = 1
        }
        return minutes
    }
    
    func minutesBetweenDates(_ newDate: Date) -> CGFloat {
        
        let newDateMinutes = newDate.timeIntervalSinceReferenceDate / 60
        let oldDateMinutes = self.timeIntervalSinceReferenceDate / 60
        return CGFloat(newDateMinutes - oldDateMinutes) / 1000000
    }
    
    public static func getTimeIntervalInMilliSeconds(timeInterval: Double) -> Double {
        return round(timeInterval * 1000)
    }
    
    // Return a ordinal suffix for days as string.
    func daySuffix() -> String {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: self)
        switch dayOfMonth {
        case 11, 12, 13:
            return "th"
        default:
            switch dayOfMonth % 10 {
            case 1:
                return "st"
            case 2:
                return "nd"
            case 3:
                return "rd"
            default:
                return "th"
            }
        }
    }
    
    // Retrun text based upon time of the day
    func getGreetingsText() -> String {
        var greetingText = String()
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        
        let morning = 5
        let afternoon = 12
        let evening = 16
        let night = 22
        
        switch hour {
        case morning..<afternoon:
            greetingText = "Good Morning".localized
        case afternoon..<evening:
            greetingText = "Good Afternoon".localized
        case evening..<night:
            greetingText = "Good Evening".localized
        default:
            greetingText = "Good Night".localized
        }
 
        return greetingText
    }
    
    // Return timestamp with date and without time
    func stripTime() -> Date? {
        let components = Calendar.current.dateComponents([.year, .month, .day, .minute], from: self)
        let date = Calendar.current.date(from: components)
        return date
    }
    
    static func convertTimeStringToDate(timeString: String) -> Date? {
        dateFormatter.dateFormat = genericTimeFormat
        let timeDate = dateFormatter.date(from: timeString)
        return timeDate
    }
    
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    static func combineDateWithTime(date: Date, time: Date) -> Date? {
        let calendar = NSCalendar.current
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: date)
        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: time)
        var mergedComponments = DateComponents()
        mergedComponments.year = dateComponents.year
        mergedComponments.month = dateComponents.month
        mergedComponments.day = dateComponents.day
        mergedComponments.hour = timeComponents.hour
        mergedComponments.minute = timeComponents.minute
        mergedComponments.second = timeComponents.second
        return calendar.date(from: mergedComponments)
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self))) ?? Date()
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth()) ?? Date()
    }
    
    static func minutesToHoursMinutes (minutes: Int) -> (hours: Int, leftMinutes: Int) {
        return (minutes / 60, (minutes % 60))
    }
    
}
