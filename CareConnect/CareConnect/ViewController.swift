//
//  ViewController.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 17/06/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class ViewController: UIViewController, AuthenticationProtocol {

    var test: Int?
    var fitBitService: FitBitService?

    override func viewDidLoad() {
        super.viewDidLoad()
        fitBitService = FitBitService(delegate: self)
        fitBitService?.login(fromParentViewController: self)
    }
    func authorizationDidFinish(_ success: Bool) {
        guard let authToken = FitBitService.authenticationToken else {
            return
        }
        fitBitService?.getAccessToken()
        //performSegue(withIdentifier: "Steps", sender: self)
    }

}
