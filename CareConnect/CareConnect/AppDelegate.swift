//
//  AppDelegate.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 17/06/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit
import CoreData
import VirgilSDK
import VirgilCrypto

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if UserDefaults.standard.string(forKey: "first_launch")?.isEmpty ?? true {
            let context = persistentContainer.viewContext
            let fetchRequest =
                NSFetchRequest<NSManagedObject>(entityName: CoreDataHelper.Entities.account.rawValue)
            if let result = try? context.fetch(fetchRequest) {
                for object in result {
                    context.delete(object)
                }
            }
            try? KeyStorage().reset()
            UserDefaults.standard.set("happened", forKey: "first_launch")
            UserDefaults.standard.synchronize()
        }
        
        CoreDataHelper.initialize()
        
//        window = UIWindow(frame: UIScreen.main.bounds)
        if Defaults().isUserLoggedIn {
            appDelegate?.switchToDashboard()
        } else {
//            appDelegate?.switchToOnboarding()
        }
//        window?.makeKeyAndVisible()
        
        return true
    }
    
    func switchToDashboard() {

        guard let dashboardVC = UIStoryboard(storyboard: .dashboard).instantiateViewController(withIdentifier: String(describing: DashboardTabbarController.self)) as? DashboardTabbarController else {
            return
        }
        Defaults.shared.shouldUpdateDashboard = true
        dashboardVC.selectedIndex = 0
        let dashboardNC = UINavigationController(rootViewController: dashboardVC)
        dashboardNC.isNavigationBarHidden = true
        window?.setRootController(viewController: dashboardNC)
        
        let navController = window?.rootViewController as? UINavigationController
        if navController?.viewControllers.first as? UITabBarController != nil {
            let tabbarController = window?.rootViewController as? UITabBarController
            tabbarController?.selectedIndex = 0
        }
    }
    
    func switchToOnboarding() {
        resetDefaults()
        
//        let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
//        let initialViewController = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController")
//        self.window?.rootViewController = initialViewController

        /*
        guard let loginVC = UIStoryboard(storyboard: .onboarding).instantiateViewController(identifier: "OnboardingViewController") as? OnboardingViewController else {
            return
        }
        
        let loginNC = UINavigationController(rootViewController: loginVC)
        loginNC.isNavigationBarHidden = true
        window?.rootViewController = loginNC*/
    }
    
    private func resetDefaults() {
        Defaults.shared.conncetedDeviceType = ShimType.none.rawValue
        Defaults.shared.isDepedentUser = false
        Defaults.shared.shouldUpdateDashboard = true
        Defaults().isUserLoggedIn = false
        Defaults().isFitBitLoggedIn = false
        Defaults().refreshToken = ""
        Defaults().selectedProfileID = 0
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        self.saveContext()

    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        let notification = Notification(
            name: Notification.Name(rawValue: NotificationConstants.launchNotification),
            object: nil,
            userInfo: [UIApplication.LaunchOptionsKey.url: url])
        NotificationCenter.default.post(notification)
        return true
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "VirgilMessenger")
        container.loadPersistentStores(completionHandler: { storeDescription, error in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                Log.error("save context failed: \(error.localizedDescription)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        Log.debug("saving context")
        let context = persistentContainer.viewContext
        if context.hasChanges {
            Log.debug("context has changes")
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                Log.error("save context failed: \(nserror.localizedDescription)")
            }
        }
    }

}
