//
//  CoreDataHelper.swift
//  VirgilMessenger
//
//  Created by Eugen Pivovarov on 11/9/17.
//  Copyright © 2017 VirgilSecurity. All rights reserved.
//

import UIKit
import Foundation
import UIKit
import CoreData

class ColorPair {
    var first: CGColor
    var second: CGColor
    
    init(_ first: UIColor, _ second: UIColor) {
        self.first = first.cgColor
        self.second = second.cgColor
    }
}

class UIConstants {
    static let TransitionAnimationDuration: TimeInterval = 0.3
    
    static var colorPairs: [ColorPair] = [ColorPair(
        UIColor(rgb: 0x009DFF), UIColor(rgb: 0x6AC7FF)),
                                          ColorPair(UIColor(rgb: 0x541C12), UIColor(rgb: 0x9E3621)),
                                          ColorPair(UIColor(rgb: 0x156363), UIColor(rgb: 0x21999E)),
                                          ColorPair(UIColor(rgb: 0x54CB68), UIColor(rgb: 0x9ADD7D)),          //light green
        ColorPair(UIColor(rgb: 0x665FFF), UIColor(rgb: 0x81ADFF)),          //strange
        ColorPair(UIColor(rgb: 0xFFA95C), UIColor(rgb: 0xFFCB66))
        
    ]
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

class CoreDataHelper {
    let queue: DispatchQueue
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    let managedContext: NSManagedObjectContext

    static private(set) var sharedInstance: CoreDataHelper!
    private(set) var accounts: [Account] = []
    private(set) var currentChannel: Channel?
    private(set) var currentAccount: Account?

    enum Entities: String {
        case account = "Account"
        case channel = "Channel"
        case message = "Message"
    }

    enum Keys: String {
        case account = "account"
        case channel = "channel"
        case message = "message"
        case identity = "identity"
        case name = "name"
        case body = "body"
        case isIncoming = "isIncoming"
        case type = "type"
    }

    static func initialize() {
        sharedInstance = CoreDataHelper()
    }

    private init() {
        managedContext = (self.appDelegate?.persistentContainer.viewContext)!
        self.queue = DispatchQueue(label: "core-data-help-queue")
        guard let accounts = self.fetch() else {
            Log.error("Core Data: fetch error")
            return
        }
        self.accounts = accounts
        Log.debug("Core Data: accounts fetched. Count: \(self.accounts.count)")
        for account in self.accounts {
            let identity = account.identity ?? "not found"
            Log.debug(identity)
        }
    }

    func reloadData() {
        guard let accounts = self.fetch() else {
            Log.error("Core Data: fetch error")
            return
        }
        self.accounts = accounts
    }

    private func fetch() -> [Account]? {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: Entities.account.rawValue)

        do {
            let accounts = try managedContext.fetch(fetchRequest) as? [Account]
            return accounts
        } catch let error as NSError {
            Log.error("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }

    func setCurrent(account: Account) {
        self.currentAccount = account
    }

    func setCurrent(channel: Channel) {
        self.currentChannel = channel
    }

    func append(account: Account) {
        self.accounts.append(account)
    }
}
