//
//  TwilioHelper+Delegate.swift
//  VirgilMessenger
//
//  Created by Oleksandr Deundiak on 10/17/17.
//  Copyright © 2017 VirgilSecurity. All rights reserved.
//

import UIKit
import CoreData
import Foundation
import TwilioChatClient

extension TwilioHelper: TwilioChatClientDelegate {
    enum Notifications: String {
        case ConnectionStateUpdated = "TwilioHelper.Notifications.ConnectionStateUpdated"
        case MessageAdded = "TwilioHelper.Notifications.MessageAdded"
        case MessageAddedToSelectedChannel = "TwilioHelper.Notifications.MessageAddedToSelectedChannel"
        case ChannelAdded = "TwilioHelper.Notifications.ChannelAdded"
    }

    enum NotificationKeys: String {
        case NewState = "TwilioHelper.NotificationKeys.NewState"
        case Message = "TwilioHelper.NotificationKeys.Message"
        case Channel = "TwilioHelper.NotificationKeys.Channel"
    }

    enum ConnectionState: String {
        case unknown = "unknown"
        case disconnected = "disconnected"
        case connected = "connected"
        case connecting = "connecting"
        case denied = "denied"
        case error = "error"

        init(state: TCHClientConnectionState) {
            switch state {
            case .unknown: self = .unknown
            case .disconnected: self = .disconnected
            case .connected: self = .connected
            case .connecting: self = .connecting
            case .denied: self = .denied
            case .error: self = .error
            }
        }
    }

    func chatClient(_ client: TwilioChatClient, connectionStateUpdated state: TCHClientConnectionState) {
        let connectionState = ConnectionState(state: state)

        let stateStr = connectionState.rawValue
        Log.debug("\(stateStr)")

        NotificationCenter.default.post(
            name: Notification.Name(rawValue: TwilioHelper.Notifications.ConnectionStateUpdated.rawValue),
            object: self,
            userInfo: [
                TwilioHelper.NotificationKeys.NewState.rawValue: connectionState
            ])
    }

    func chatClient(_ client: TwilioChatClient, channelAdded channel: TCHChannel) {
        Log.debug("Channel added")
        self.join(channel: channel)
    }

    func chatClient(_ client: TwilioChatClient, channelDeleted channel: TCHChannel) {
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: TwilioHelper.Notifications.ChannelAdded.rawValue),
            object: self,
            userInfo: [:])
    }

    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, messageAdded message: TCHMessage) {
        Log.debug("Message added")
        if let currentChannel = self.currentChannel,
            self.getName(of: channel) == self.getName(of: currentChannel) {
                Log.debug("it's from selected channel")
                if message.author != self.username {
                    Log.debug("author is not me")
                    NotificationCenter.default.post(
                        name: Notification.Name(rawValue: TwilioHelper.Notifications.MessageAddedToSelectedChannel.rawValue),
                        object: self,
                        userInfo: [
                            TwilioHelper.NotificationKeys.Message.rawValue: message
                        ])
                }
        } else {
            self.processMessage(channel: channel, message: message)
        }
    }

    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, memberJoined member: TCHMember) {
        if self.getType(of: channel) == ChannelType.group,
            let name = self.getName(of: channel),
            let coreChannel = CoreDataHelper.sharedInstance.getChannel(withName: name) {
                Log.debug("New member joined")
                guard let identity = member.identity else {
                    Log.error("Member identity is unaccessable")
                    return
                }
                VirgilHelper.sharedInstance.getExportedCard(identity: identity) { exportedCard, error in
                    guard error == nil, let exportedCard = exportedCard else {
                        return
                    }
                    CoreDataHelper.sharedInstance.addMember(card: exportedCard, to: coreChannel)
                    guard let cards = CoreDataHelper.sharedInstance.currentChannel?.cards else {
                        Log.error("Fetching current channel cards failed")
                        return
                    }
                    VirgilHelper.sharedInstance.setChannelKeys(cards)
                }
        }
    }

    private func processMessage(channel: TCHChannel, message: TCHMessage) {
        guard let messageDate = message.dateUpdatedAsDate else {
            Log.error("Got corrupted message")
            return
        }
        guard let channelName = self.getName(of: channel) else {
            return
        }
        guard let coreDataChannel = CoreDataHelper.sharedInstance.getChannel(withName: channelName) else {
            Log.error("Can't get core data channel")
            return
        }

        if let messageBody = message.body {
            guard let decryptedMessageBody = VirgilHelper.sharedInstance.decrypt(messageBody) else {
                return
            }

            coreDataChannel.lastMessagesBody = decryptedMessageBody
            coreDataChannel.lastMessagesDate = messageDate

            if ((Int(truncating: message.index ?? 0) >= (coreDataChannel.message?.count ?? 0))) {
                CoreDataHelper.sharedInstance.createTextMessage(for: coreDataChannel, withBody: decryptedMessageBody,
                                                                isIncoming: true, date: messageDate)
            }
            Log.debug("Receiving " + decryptedMessageBody)
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: TwilioHelper.Notifications.MessageAdded.rawValue),
                object: self,
                userInfo: [
                    TwilioHelper.NotificationKeys.Message.rawValue: message
                ])
        }
    }
}
