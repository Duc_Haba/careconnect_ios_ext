//
//  ChatListUserCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 25/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class ChatListUserCell: UITableViewCell, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.dropShadow(cornerRadius: 20)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
