//
//  SenderTableviewCell.swift
//  ChattingApp
//
//  Created by PRAMOD S on 20/06/19.
//  Copyright © 2019 PRAMOD S. All rights reserved.
//

import UIKit

class SenderTableviewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var senderTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        senderTextView.layer.cornerRadius = 8
        containerView.dropShadow(cornerRadius: 10)
    }    
}
