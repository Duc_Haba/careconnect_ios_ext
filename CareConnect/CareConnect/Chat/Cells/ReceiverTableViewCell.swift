//
//  ReceiverTableViewCell.swift
//  ChattingApp
//
//  Created by PRAMOD S on 20/06/19.
//  Copyright © 2019 PRAMOD S. All rights reserved.
//

import UIKit

class ReceiverTableViewCell: UITableViewCell {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var receiverIcon: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        receiverIcon.layer.masksToBounds = true
        receiverIcon.layer.cornerRadius = 15
        textView.layer.cornerRadius = 8
        containerView.dropShadow(cornerRadius: 20)
    }    
}
