//
//  ChatVC.swift
//  ChattingApp
//
//  Created by PRAMOD S on 20/06/19.
//  Copyright © 2019 PRAMOD S. All rights reserved.
//

import UIKit
import TwilioChatClient

enum MessageType {
    case sender
    case receiver
}

class MessageModel {
    
    var body: String
    var type = MessageType.sender
    var date: Date
    
    init(body: String, type: MessageType, date: Date) {
        
        self.body = body
        self.type = type
        self.date = date
    }
}

class ChatVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var textFieldBtmConstraint: NSLayoutConstraint!
    @IBOutlet weak var navBarHeight: NSLayoutConstraint!
    
    var messages = [MessageModel]()
    var selectedUser: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 50
        registerNib()
        sendButton.layer.cornerRadius = 10
        addKeyboardNotificationObserver()
        setUpTextField()
        //addObserver()
        userName.text = selectedUser
        NotificationCenter.default.addObserver(self, selector: #selector(doThisWhenNotify(notification:)), name: NSNotification.Name(rawValue: TwilioHelper.Notifications.MessageAddedToSelectedChannel.rawValue), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavBar()
        tableView.transform = CGAffineTransform(rotationAngle: .pi)
        tableView.contentInset.bottom = 20
        getMessages()
//        if let title = CoreDataHelper.sharedInstance.currentChannel?.name {
//            TwilioHelper.sharedInstance.setChannel(withName: title)
//        }

    }
    
    @objc func doThisWhenNotify(notification: NSNotification) {
        guard  let userInfo = notification.userInfo,
            let message = userInfo[TwilioHelper.NotificationKeys.Message.rawValue] as? TCHMessage,
            let messageDate = message.dateUpdatedAsDate else {
                return
        }
        let isIncoming = message.author == TwilioHelper.sharedInstance.username ? false : true
        
        if let messageBody = message.body {
            guard let decryptedBody = VirgilHelper.sharedInstance.decrypt(messageBody) else {
                return
            }
            
            insertRowForTable(text: decryptedBody, for: isIncoming == true ? .receiver : .sender, date: messageDate)
            CoreDataHelper.sharedInstance.createTextMessage(withBody: decryptedBody, isIncoming: isIncoming, date: messageDate)
            
        }
    }
    
    func getMessages() {
        
        guard let channel = CoreDataHelper.sharedInstance.currentChannel,
            let chanelMessages = channel.message else {
                Log.error("Missing Core Data current channel")
                return
        }
       // let sortedMessages = chanelMessages.sorted(by: { $0.date?.compare($1.date ?? Date()) == .orderedAscending})

        for message in chanelMessages {
            
            guard let messageData = message as? Message, let body = messageData.body, let date = messageData.date else {
                return
            }
            
            if messageData.isIncoming == true {
                messages.append(MessageModel(body: body, type: .receiver, date: date))
            } else {
                messages.append(MessageModel(body: body, type: .sender, date: date))
            }
            
        }
        messages = messages.sorted(by: {$0.date.compare($1.date) == .orderedDescending})
        tableView.reloadData()
    }
    
//    func addObserver() {
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(ChatVC.processMessage(notification:)),
//                                               name: Notification.Name(rawValue: TwilioHelper.Notifications.MessageAddedToSelectedChannel.rawValue),
//                                               object: nil)
//    }
    
    private func messageStatus(ciphertext: String, message: MessageModel) {
        
        // let encrypted = VirgilHelper.sharedInstance.encrypt(message.body ?? "")
        //            self.messageStatus(ciphertext: ciphertext, message: message)
        if let messages = TwilioHelper.sharedInstance.currentChannel.messages {
            // DEBUG
            /*let options = TCHMessageOptions().withBody(ciphertext)
            Log.debug("sending \(ciphertext)")
            messages.sendMessage(with: options) { result, msg in
                if result.isSuccessful() {
                    //self.updateMessage(message, status: .success)
                    CoreDataHelper.sharedInstance.createTextMessage(withBody: message.body, isIncoming: false,
                                                                    date: message.date)
                } else {
                    Log.error("error sending: Twilio cause")
                    //self.updateMessage(message, status: .failed)
                }
            }*/
        } else {
            Log.error("can't get channel messages")
        }
    }
    
    @objc private func processMessage(notification: Notification) {
        guard  let userInfo = notification.userInfo,
            let message = userInfo[TwilioHelper.NotificationKeys.Message.rawValue] as? TCHMessage,
            let messageDate = message.dateUpdatedAsDate else {
                return
        }
        _ = message.author == TwilioHelper.sharedInstance.username ? false : true
        
        if let messageBody = message.body {
            guard let decryptedBody = VirgilHelper.sharedInstance.decrypt(messageBody) else {
                return
            }
            
            Log.debug("Receiving " + decryptedBody)
            
            //            let model = MessageFactory.createMessageModel("\(self.nextMessageId)", isIncoming: isIncoming, type: TextMessageModel<MessageModel>.chatItemType, status: .success, date: messageDate)
            //            let decryptedMessage = DemoTextMessageModel(messageModel: model, text: decryptedBody)
            
            CoreDataHelper.sharedInstance.createTextMessage(withBody: messageBody, isIncoming: true, date: messageDate)
            
            //            self.slidingWindow.insertItem(decryptedMessage, position: .bottom)
            //            self.nextMessageId += 1
        } else {
            Log.error("Empty Twilio message")
        }
        //self.delegate?.chatDataSourceDidUpdate(self)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction private func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func registerNib() {
        let senderNib = UINib(nibName: "SenderTableviewCell", bundle: nil)
        tableView.register(senderNib, forCellReuseIdentifier: "SenderTableviewCell")
        let receiverNib = UINib(nibName: "ReceiverTableViewCell", bundle: nil)
        tableView.register(receiverNib, forCellReuseIdentifier: "ReceiverTableViewCell")
    }
    
    func setUpNavBar() {
        if UIDevice.current.hasNotch {
            navBarHeight.constant = 88
        } else {
            navBarHeight.constant = 64
        }
    }
    
    func insertRowForTable(text: String, for messageType: MessageType, date: Date) {
        messages.insert(MessageModel(body: text, type: messageType, date: date), at: 0)
        let newIndexpath = IndexPath(row: 0, section: 0)
        tableView.insertRows(at: [newIndexpath], with: .none)
        tableView.scrollToRow(at: newIndexpath, at: .bottom, animated: true)
    }
    
    func setUpTextField() {
        textField.delegate = self
    }
    
    func addKeyboardNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func handleKeyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double
            let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            if UIDevice.current.hasNotch {
                self.textFieldBtmConstraint.constant = isKeyboardShowing ? keyboardSize.height - 30 : 0
            } else {
                self.textFieldBtmConstraint.constant = isKeyboardShowing ? keyboardSize.height : 0
            }
            
            if let animationDuration = duration {
                UIView.animate(withDuration: animationDuration, delay: 0, options: .curveEaseOut, animations: {
                    self.view.layoutIfNeeded()
                }) { completed in
                    if isKeyboardShowing {
                        if self.messages.isEmpty {
                            return
                        }
                        let indexpath = IndexPath(item: 0, section: 0)
                        self.tableView.scrollToRow(at: indexpath, at: .bottom, animated: true)
                    }
                }
            }
        }
    }
    
    @IBAction private func sendMessage() {
        if let text = textField.text, !text.isEmpty {
            self.insertRowForTable(text: text, for: .sender, date: Date())
            textField.text = nil
            let message = MessageModel(body: text, type: .sender, date: Date())
            guard let encrypted = VirgilHelper.sharedInstance.encrypt(message.body) as? String else {
                return
            }
            messageStatus(ciphertext: encrypted, message: message)
        }
    }
}

extension ChatVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        let messageType = messages[indexPath.item].type
            switch messageType {
            case .sender:
                guard  let senderCell = tableView.dequeueReusableCell(withIdentifier: "SenderTableviewCell", for: indexPath) as? SenderTableviewCell else {
                    return UITableViewCell()
                }
                
                senderCell.senderTextView.text = messages[indexPath.item].body
                
                senderCell.transform = CGAffineTransform(rotationAngle: -.pi)
                cell = senderCell
            case .receiver:
                guard  let receiverCell = tableView.dequeueReusableCell(withIdentifier: "ReceiverTableViewCell", for: indexPath) as? ReceiverTableViewCell else {
                    return UITableViewCell()
                }
                var message = messages[indexPath.item].body
                message = message.replacingOccurrences(of: "\\n", with: "\n")
                receiverCell.textView.text = message
                receiverCell.transform = CGAffineTransform(rotationAngle: -.pi)
                cell = receiverCell
            }
            return cell
        }
    }
    
    extension ChatVC: UITextFieldDelegate {
        public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.endEditing(true)
            sendButton.setImage(UIImage(named: "iconAttachment"), for: .normal)
            return true
        }
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
            sendButton.setImage(UIImage(named: "iconCheckmark"), for: .normal)
        }
}
