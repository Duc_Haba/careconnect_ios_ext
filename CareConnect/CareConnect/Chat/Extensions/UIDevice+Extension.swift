//
//  UIDevice+Extension.swift
//  ChattingApp
//
//  Created by PRAMOD S on 20/06/19.
//  Copyright © 2019 PRAMOD S. All rights reserved.
//

import Foundation
import UIKit

extension UIDevice {
    var hasNotch: Bool {
        if #available(iOS 11.0, *) {
            let bottom = UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0
            return bottom > 0
        }
        return false
    }
}
