//
//  UIWindow+Extension.swift
//  CareConnect
//
//  Created by Y Media Labs on 11/09/18.
//  Copyright © 2018 Y Media Labs. All rights reserved.
//

import UIKit

extension UIWindow {
    
    func setRootController(viewController: UIViewController) {
        let transition = CATransition()
        transition.type = CATransitionType.fade
        
        layer.add(transition, forKey: kCATransition)
        self.rootViewController = viewController
    }
}
