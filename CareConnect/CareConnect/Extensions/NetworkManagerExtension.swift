//
//  NetworkManagerExtension.swift
//  CareConnect
//
//  Created by Venugopal S A on 18/06/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import Foundation
import Rudder

enum HeaderConfiguration {
    case tempToken
    case apiToken
    case defaultHeader
    case epicHeader
    case token(token: String)
    
    var header: [String: String] {
        var header = ["Content-Type": "application/x-www-form-urlencoded"]
        switch  self {
        case .tempToken:
            header["Authorization"] = "Basic MjJEUkg0OmI3ODMzZWY5ZmIyZDRlZjNmYzIxYzlhM2YwMGY0NzBk"
        case .apiToken:
            header["Authorization"] = "Basic MjJEUkg0OmI3ODMzZWY5ZmIyZDRlZjNmYzIxYzlhM2YwMGY0NzBk"
        case .epicHeader:
            header["Content-Type"] = "application/json"
            header["accept"] = "application/json"
        case .token(let token):
            header["Content-Type"] = "application/json"
            header["x-auth-token"] = "\(token)"
        case .defaultHeader:
            header["Content-Type"] = "application/json"
        }
        
        return header
    }
}

extension NetworkManager {
    
    class public func requestForURL(_ urlString: String, method: HTTPMethod, params: Data?, headers: [String: String]?, encoding: NetworkManagerEncodingType = .URLEncoded) -> URLRequest? {
        
        guard let url = URL(string: urlString), let data = params else {
            return nil
        }
        
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        URLCache.shared.removeCachedResponse(for: request)
        request.httpMethod = method.rawValue
        if let headers = headers {
            for (field, value) in headers {
                request.setValue(value, forHTTPHeaderField: field)
            }
        }
        switch encoding {
        case .URLEncoded:
            request.setValue(NetworkManagerEncodingType.URLEncoded.rawValue, forHTTPHeaderField: "Content-Type")
            request.httpBody = data
        case .Raw:
            if let params = params, !params.isEmpty {
                request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            }
            
        case .URL:
          break
            
        }
        
        return request
    }
    
}
