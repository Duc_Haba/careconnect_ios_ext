//
//  AlertViewController.swift
//  CareConnect
//
//  Created by Arun Kulkarni on 12/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

protocol CustomAlertViewDelegate: class {
    func rescheduleButtonTapped()
    func cancelButtonTapped()
}

final class AlertViewController: UIViewController {
    
    @IBOutlet private weak var backgroundView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet private weak var leftButton: UIButton!
    @IBOutlet private weak var rightButton: UIButton!
    
    var message: String?
    weak var delegate: CustomAlertViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    // MARK: - Event Handlers
    @IBAction private func didSelectReschdule(_ sender: Any) {
            dismiss(animated: true) {
                self.delegate?.rescheduleButtonTapped()
            }
    }
    
    @IBAction private func didSelectCancel(sender: UIButton) {
        dismiss(animated: true) {
            self.delegate?.cancelButtonTapped()
        }
    }
    
    // MARK: - Private Methods
    
    private func setupView() {
        backgroundView.dropShadow(cornerRadius: 4)
        backgroundView.layer.cornerRadius = 20
        titleLabel.text = title
        messageLabel.text = message
    }
    
}
