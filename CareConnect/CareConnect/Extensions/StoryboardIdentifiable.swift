//
//  UIWindowStoryboardIdentifiableExtension.swift
//  CareConnect
//
//  Created by Y Media Labs on 11/09/18.
//  Copyright © 2018 Y Media Labs. All rights reserved.
//

import UIKit

protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}
