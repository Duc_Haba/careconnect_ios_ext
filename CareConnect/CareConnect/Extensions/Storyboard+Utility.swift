//
//  Storyboard+Utility.swift
//  CareConnect
//
//  Created by Y Media Labs on 11/09/18.
//  Copyright © 2018 Y Media Labs. All rights reserved.
//

import UIKit

/// The uniform place where we state all the storyboard we have in our application

enum Storyboard: String {
    case onboarding = "Onboarding"
    case dashboard = "Dashboard"
    case chat = "Chat"
    
}

/// Use this class to load storyboard with identifier.
/// Note: Class name identifier has to be same
/// Example
///     let storyboard = UIStoryboard(storyboard: .Post)
///     let viewController: PostViewController = storyboard.instantiateViewController()

extension UIStoryboard {
    /// Convenience Initializers
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
    /// Class Functions
    class func storyboard(_ storyboard: Storyboard, bundle: Bundle? = nil) -> UIStoryboard {
        return UIStoryboard(name: storyboard.rawValue, bundle: bundle)
    }
    /// View Controller Instantiation from Generics
    /// New Way
    func instantiateViewController<T: UIViewController>() -> T where T: StoryboardIdentifiable {
        guard let viewController = self.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.storyboardIdentifier) ")
        }
        return viewController
    }
}
