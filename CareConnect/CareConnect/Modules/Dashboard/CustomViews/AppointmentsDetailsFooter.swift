//
//  AppointmentsDetailsFooter.swift
//  CareConnect
//
//  Created by Venugopal S A on 02/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class AppointmentsDetailsFooter: UIView {
    
    class func instanceFromNib() -> UIView {
        guard let view = UINib(nibName: "AppointmentsDetailsFooter", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView else {
            return UIView()
        }
        return view
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    @IBAction private func confirmTapped(_ sender: Any) {
        
    }
}
