//
//  MedicationCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 08/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class MedicationCell: UITableViewCell {
    
    @IBOutlet weak var dropDown: UIButton!
    @IBOutlet weak var medicineName: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction private func dropDownTapped(_ sender: Any) {
        
    }
    
}
