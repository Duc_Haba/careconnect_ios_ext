//
//  SelectDoctorCell.swift
//  CareConnect
//
//  Created by Venugopal S A on 08/07/19.
//  Copyright © 2019 Yml. All rights reserved.
//

import UIKit

class SelectDoctorCell: UITableViewCell {

    @IBOutlet weak var selectDoctorLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.dropShadow(cornerRadius: 20)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction private func addTapped(_ sender: Any) {
    }
    
}
