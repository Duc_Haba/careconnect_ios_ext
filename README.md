### Code Setup - 13 Aug 2021 ###

* Open the terminal or Use any git tool for code clone
* Clone : https://nielfhir@bitbucket.org/Duc_Haba/careconnect_ios_ext.git
* Checkout Branch - dev
* Open terminal 
* Set project path in the terminal 
* Run the command : pod install
* Open the xcworkspace in the latest Xcode from the project folder 
* Clean the project before run on simulator : cmd + shift + k
* Run the project : cmd + r
